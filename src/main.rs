use log::error;
use mimalloc::MiMalloc;
use niceerr_api::{info::VERSION, spin_up_server};
use niceerr_config::Config;

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

fn print_banner() {
    println!(
        "
     ███╗   ██╗██╗ ██████╗███████╗
     ████╗  ██║██║██╔════╝██╔════╝
     ██╔██╗ ██║██║██║     █████╗  
     ██║╚██╗██║██║██║     ██╔══╝  
     ██║ ╚████║██║╚██████╗███████╗
     ╚═╝  ╚═══╝╚═╝ ╚═════╝╚══════╝
                                             
 ██╗   ███████╗ ██████╗  ██████╗    ██╗   
 ╚██╗  ██╔════╝ ██╔══██╗ ██╔══██╗  ██╔╝   
  ╚██╗ █████╗   ██████╔╝ ██████╔╝ ██╔╝      
  ██╔╝ ██╔══╝   ██╔══██╗ ██╔══██╗ ╚██╗    
 ██╔╝  ███████╗ ██║  ██║ ██║  ██║  ╚██╗   
 ╚═╝   ╚══════╝ ╚═╝  ╚═╝ ╚═╝  ╚═╝   ╚═╝

based music discovery server v{VERSION}"
    );
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    if std::env::var("RUST_LOG").is_err() {
        // Safety: set_var can be safely used in single-threaded programs, and this function
        // never runs in more than 1 thread/worker
        unsafe { std::env::set_var("RUST_LOG", "niceerr=info") };
    }
    print_banner();
    tracing_subscriber::fmt::init();
    Config::init().await?;
    spin_up_server()
        .await
        .inspect_err(|e| error!("fatal error: couldn't spin up server: {e:?}"))?;
    Ok(())
}
