use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub enum Iso669CountryCode {
    #[serde(rename = "en")]
    English,
    #[serde(rename = "es")]
    Spanish,
    #[serde(rename = "fr")]
    French,
    #[serde(rename = "pt")]
    Portuguese,
    #[serde(rename = "it")]
    Italian,
    #[serde(rename = "de")]
    German,
    #[serde(rename = "no")]
    Norwegian,
    #[serde(rename = "sv")]
    Swedish,
    #[serde(rename = "da")]
    Danish,
    #[serde(rename = "fi")]
    Finnish,
    #[serde(rename = "pl")]
    Polish,
    #[serde(rename = "ar")]
    Arabic,
    #[serde(rename = "uk")]
    Ukrainian,
    #[serde(rename = "ru")]
    Russian,
    #[serde(rename = "ja")]
    Japanese,
    #[serde(rename = "zh")]
    Chinese,
    #[serde(rename = "ko")]
    Korean,
    #[serde(rename = "eo")]
    Esperanto,
}

impl Iso669CountryCode {
    pub fn to_str(&self) -> &'static str {
        match self {
            Iso669CountryCode::English => "en",
            Iso669CountryCode::Spanish => "es",
            Iso669CountryCode::French => "fr",
            Iso669CountryCode::Portuguese => "pt",
            Iso669CountryCode::Italian => "it",
            Iso669CountryCode::German => "de",
            Iso669CountryCode::Norwegian => "no",
            Iso669CountryCode::Swedish => "sv",
            Iso669CountryCode::Danish => "da",
            Iso669CountryCode::Finnish => "fi",
            Iso669CountryCode::Polish => "pl",
            Iso669CountryCode::Arabic => "ar",
            Iso669CountryCode::Ukrainian => "uk",
            Iso669CountryCode::Russian => "ru",
            Iso669CountryCode::Japanese => "ja",
            Iso669CountryCode::Chinese => "zh",
            Iso669CountryCode::Korean => "ko",
            Iso669CountryCode::Esperanto => "eo",
        }
    }
}
