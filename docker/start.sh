#!/bin/sh

# stop nginx and Rust binary
function graceful_shutdown() {
    nginx -s quit
    kill -TERM "$RUST_PID"
    wait "$RUST_PID"
    exit
}

# Trap SIGTERM signal
trap 'graceful_shutdown' SIGTERM

# Start nginx
nginx &

# Default downloads directory (can be
# overriden in the .env config)
export downloads_folder=/downloads
# cd into env folder & start niceerr
cd /etc/niceerr && /usr/local/bin/niceerr &
RUST_PID=$!

# Wait for the Rust binary to exit
wait "$RUST_PID"
