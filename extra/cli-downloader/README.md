# CLI download utility for niceerr

This is a quite small and simple CLI utility that you can use to download music from any niceerr server straight to your computer.


## Coompile & run

This is just a regular rust program, you can run it with 

```bash
cargo run --release
# For convenience, copy the resulting binary somewhere else
cp target/release/cli-downloader .
./cli-downloader
```

### Usage

This CLI tool is very straightforward and simple to use.

Here are the defaults, unless you specify otherwise:

- Quality: FLAC
- Quality fallback: allowed (only needed if any given song isn't available in the desired quality)
- Exclude credited albums/songs (only works for discographies, though). This might download a lot of crap if enabled.
- Include live content

```bash
# Download something in FLAC quality, in the current working folder
./cli-downloader <URL>
# Download something in crap quality, and save everything to a folder "bruh" 
./cli-downloader <URL> --path bruh --quality mp3-128
# Download something exclusively in FLAC quality (this prevents niceerr from
# falling back to crappier foramts such as MP3), include credited albums and skip live content
./cli-downloader <URL> --no-fallback false --include-credited --skip-live

```