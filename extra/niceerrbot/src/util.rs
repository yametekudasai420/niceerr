use std::{env, path::PathBuf};

use rand::{distributions::Alphanumeric, thread_rng, Rng};

pub fn bytes_to_human_readable(size_in_bytes: u64) -> String {
    // Define the units and the corresponding thresholds
    let units = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
    let threshold = 1024.0;

    // Use mutable variables for the size and unit index
    let mut size = size_in_bytes as f64;
    let mut unit_index = 0;

    // Keep dividing the size until it is less than the threshold
    while size >= threshold && unit_index < units.len() - 1 {
        size /= threshold;
        unit_index += 1;
    }

    // Return the formatted string with two decimal places
    format!("{:.2} {}", size, units[unit_index])
}

pub fn random_string() -> String {
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect();
    rand_string
}

pub fn random_temp_path() -> PathBuf {
    env::temp_dir().join(random_string())
}
