use std::sync::{OnceLock, RwLock, RwLockReadGuard, RwLockWriteGuard};

use anyhow::{bail, Context};
use serde::{Deserialize, Serialize};

use crate::config::BotConfig;

#[derive(Deserialize, Serialize, Debug, Clone)]
struct User {
    id: i64,
    username: String,
    password: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct JsonDb {
    users: Vec<User>,
}

impl JsonDb {
    fn new() -> Self {
        Self { users: vec![] }
    }

    async fn load_or_init() -> anyhow::Result<Self> {
        let existing = Self::load_existing().await;
        let Ok(db) = existing else {
            let db = Self::new();
            db.save().await?;
            return Ok(db);
        };
        Ok(db)
    }

    fn get_user_by_username(&self, username: &str) -> Option<&User> {
        self.users.iter().find(|user| user.username == username)
    }

    fn remove_user(&mut self, username: &str) {
        self.users.retain(|user| user.username != username);
    }

    fn add_credentials(&mut self, id: i64, username: &str, password: &str) {
        self.users.push(User {
            id,
            username: username.to_string(),
            password: password.to_string(),
        })
    }

    async fn load_existing() -> anyhow::Result<Self> {
        let from = &BotConfig::get().db_location;
        let json = tokio::fs::read_to_string(from)
            .await
            .context("Can't read DB from FS")?;
        let db: JsonDb =
            serde_json::from_str(&json).context("Can't deserialize FS contents to JSON")?;
        Ok(db)
    }

    async fn save(&self) -> anyhow::Result<()> {
        let to = &BotConfig::get().db_location;
        let json = serde_json::to_string(&self).context("Can't serialize memory state to JSON")?;
        tokio::fs::write(to, json)
            .await
            .context("Can't write memory DB to FS")?;
        Ok(())
    }
}

type SharedRwDb = RwLock<JsonDb>;

#[derive(Deserialize, Serialize, Debug)]
pub struct AuthService {
    db: SharedRwDb,
}

static AUTH_SERVICE: OnceLock<AuthService> = OnceLock::new();

impl AuthService {
    pub async fn try_init() -> anyhow::Result<()> {
        let db = JsonDb::load_or_init()
            .await
            .context("Can't initialize user DB")?;
        let this = AuthService {
            db: RwLock::new(db),
        };
        AUTH_SERVICE.set(this).expect("Can't set auth service");
        Ok(())
    }

    fn read<'a>() -> RwLockReadGuard<'a, JsonDb> {
        let this = AUTH_SERVICE.get().expect("Auth service not initialized");
        this.db.read().expect("Can't get read lock")
    }

    fn write<'a>() -> RwLockWriteGuard<'a, JsonDb> {
        let this = AUTH_SERVICE.get().expect("Auth service not initialized");
        this.db.write().expect("Can't get write lock")
    }

    async fn save_cloned() -> anyhow::Result<()> {
        let db = Self::read().clone();
        db.save().await?;
        Ok(())
    }

    pub async fn save_credentials(id: i64, username: &str, password: &str) -> anyhow::Result<()> {
        if Self::read().get_user_by_username(username).is_some() {
            bail!("User {username} is already logged in.");
        }
        {
            Self::write().add_credentials(id, username, password);
        }
        Self::save_cloned().await?;
        Ok(())
    }

    pub fn is_user_authenticated(id: i64) -> bool {
        Self::read().users.iter().any(|user| user.id == id)
    }

    pub fn get_creds(id: i64) -> Option<(String, String)> {
        let binding = Self::read();
        let user = binding.users.iter().find(|user| user.id == id)?;
        Some((user.username.clone(), user.password.clone()))
    }

    pub async fn delete_credentials(id: i64) -> anyhow::Result<()> {
        let Some(user) = Self::read()
            .users
            .iter()
            .find(|user| user.id == id).cloned()
        else {
            bail!("User id={id} has never authenticated.");
        };
        {
            Self::write().remove_user(&user.username);
        }
        Self::save_cloned().await?;
        Ok(())
    }
}
