use log::info;
use std::io::SeekFrom;
use std::path::PathBuf;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::fs::{File, OpenOptions};
use tokio::io::{AsyncRead, AsyncSeek, AsyncWrite, ReadBuf};

pub struct SelfDeletingFile {
    inner: File,
    path: Option<PathBuf>, // None means deletion is disabled
}

impl SelfDeletingFile {
    pub async fn new(path: PathBuf) -> anyhow::Result<SelfDeletingFile> {
        let mut openopts = OpenOptions::new();
        openopts.create(true).read(true).write(true).truncate(true);
        info!("Path open: {path:?}");
        let file = openopts.open(&path).await?;
        Ok(Self {
            inner: file,
            path: Some(path),
        })
    }

    pub fn inner(&self) -> &File {
        &self.inner
    }
}

impl SelfDeletingFile {
    /// Disables the self-deletion functionality
    pub fn disarm(&mut self) {
        self.path = None;
    }
}

impl AsyncRead for SelfDeletingFile {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        Pin::new(&mut self.inner).poll_read(cx, buf)
    }
}

impl AsyncSeek for SelfDeletingFile {
    fn start_seek(mut self: Pin<&mut Self>, position: SeekFrom) -> std::io::Result<()> {
        Pin::new(&mut self.inner).start_seek(position)
    }

    fn poll_complete(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<u64>> {
        Pin::new(&mut self.inner).poll_complete(cx)
    }
}

impl AsyncWrite for SelfDeletingFile {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        Pin::new(&mut self.inner).poll_write(cx, buf)
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        Pin::new(&mut self.inner).poll_flush(cx)
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        Pin::new(&mut self.inner).poll_shutdown(cx)
    }
}

impl Drop for SelfDeletingFile {
    fn drop(&mut self) {
        if let Some(path) = self.path.clone() {
            tokio::spawn(async move {
                if let Err(e) = tokio::fs::remove_file(&path).await {
                    eprintln!("Failed to delete file {:?}: {}", path, e);
                }
            });
        }
    }
}
