use std::sync::OnceLock;

use anyhow::Context;
use log::info;
use serde::Deserialize;
use serde_inline_default::serde_inline_default;
use url::Url;

#[serde_inline_default]
#[derive(Debug, Deserialize)]
pub struct BotConfig {
    #[serde_inline_default(":dzbot".into())]
    pub trigger_keyword: String,
    #[serde_inline_default("db.json".into())]
    pub db_location: String,
    #[serde_inline_default("niceerrbot.session".into())]
    pub session_file_location: String,
    #[serde_inline_default(611335)]
    pub api_id: i32,
    #[serde_inline_default("d524b414d21f4d37f08684c1df41ac9c".into())]
    pub api_hash: String,
    pub api_url: Url,
    #[serde_inline_default(false)]
    pub ignore_unauthenticated: bool,
}

static CONFIG: OnceLock<BotConfig> = OnceLock::new();

impl BotConfig {
    pub fn try_init() -> anyhow::Result<()> {
        dotenvy::dotenv().ok();
        let env_conf = envy::from_env::<BotConfig>().context(
            "couldn't parse env conf: one or more settings are either invalid or missing",
        )?;
        info!("Loaded config: {env_conf:#?}");
        CONFIG.set(env_conf).expect("Can't set config");
        Ok(())
    }

    pub fn get() -> &'static BotConfig {
        CONFIG.get().expect("Config not initialized")
    }
}
