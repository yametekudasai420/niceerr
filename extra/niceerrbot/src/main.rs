use std::env::set_var;

use anyhow::Context;
use grammers_client::{InvocationError, Update};

use grammers_mtsender::ReadError;
use log::{error, info};
use niceerrbot::{
    auth::AuthService, backoff::LinearBackoffSleep, config::BotConfig, handler::entrypoint,
    init::ClientService,
};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    set_var("RUST_LOG", "niceerrbot=INFO");
    tracing_subscriber::fmt::init();
    BotConfig::try_init().context("Can't read configuration")?;
    ClientService::try_init().await?;
    AuthService::try_init()
        .await
        .context("Can't initialize auth service")?;

    info!("Ready to serve requests.");
    let mut backoff = LinearBackoffSleep::new();

    loop {
        let update = ClientService::get_client().next_update().await;
        match update {
            Ok(Update::NewMessage(msg)) => {
                backoff.reset();
                tokio::spawn(entrypoint(msg));
            }
            Err(InvocationError::Read(ReadError::Io(e))) => {
                error!("Connection lost, trying to refresh connection ({e:?})");
                match ClientService::try_refresh_client().await {
                    Ok(_) => {
                        info!("Connection refreshed");
                    }
                    Err(e) => {
                        error!(
                            "Can't refresh connection: {e:?}. Sleeping for {:?} before retrying.",
                            backoff.current_sleep_duration()
                        );
                        backoff.tick().await;
                    }
                }
            }
            Err(e) => {
                error!("Update error: {e:?}");
            }
            _ => {}
        }
    }
}
