use anyhow::{bail, Context};
use log::{info, warn};
use tracing::{info_span, Instrument};

use crate::{
    auth::AuthService,
    config::BotConfig,
    niceerr::{NiceerrClientService, TelegramStreamHandler},
};

static HELP_CMD: &str = "help";
static SERVER_ADD_CMD: &str = "sadd";
static STREAM_GET_CMD: &str = "get";
static LOGIN_CMD: &str = "login";
static LOGOUT_CMD: &str = "logout";

pub async fn entrypoint(msg: grammers_client::types::Message) {
    let trigger_kw = &BotConfig::get().trigger_keyword;

    if !msg.text().starts_with(&format!("{trigger_kw} ")) {
        return;
    }
    // no subcommand was passed
    let Some(cmd_args) = msg.text().split(trigger_kw).nth(1).map(|s| s.trim()) else {
        return;
    };

    let sender = msg.sender();
    let id = sender.as_ref().map(|it| it.id()).unwrap_or(0);
    let user = sender
        .and_then(|s| s.username().map(|s| s.to_owned()))
        .unwrap_or_default();
    let span = info_span!("niceerrbot", id=%id, user=%user);

    let dispatcher = HandlerDispatch::new(msg.clone(), cmd_args.to_string(), id);
    dispatcher.dispatch().instrument(span.clone()).await;
}

pub struct HandlerDispatch {
    msg: grammers_client::types::Message,
    args: String,
    sender_id: i64,
    is_authenticated: bool,
}

impl HandlerDispatch {
    pub fn new(msg: grammers_client::types::Message, args: String, sender_id: i64) -> Self {
        let is_authenticated = AuthService::is_user_authenticated(sender_id);
        Self {
            sender_id,
            msg,
            args,
            is_authenticated,
        }
    }

    fn extract_command(args: &str) -> Option<&str> {
        args.split(" ").next().map(|s| s.trim())
    }

    fn extract_command_args<'a>(prefix: &str, args: &'a str) -> Option<&'a str> {
        args.split(prefix).nth(1).map(|s| s.trim())
    }

    fn extract_parts(&self, prefix: &str, parts: usize) -> anyhow::Result<Vec<&str>> {
        let args = Self::extract_command_args(prefix, &self.args).ok_or_else(|| {
            anyhow::anyhow!(
                "{prefix}: can't use subcommand without any arguments. Use the help subcommand."
            )
        })?;
        let splitted = args.split(" ");
        let ret = splitted
            .map(|it| it.trim())
            .filter(|it| !it.is_empty())
            .collect::<Vec<&str>>();
        if ret.len() == parts {
            return Ok(ret);
        }
        bail!(
            "Expected {parts} arguments for this subcommand but got {}. Use the help subcommand.",
            ret.len()
        );
    }

    async fn help(&self) -> anyhow::Result<()> {
        let mut msg = "[niceerr] userbot help\nAvailable commands:\n\n".to_string();
        let trigger = &BotConfig::get().trigger_keyword;
        if self.is_authenticated {
            let ext = format!(
                r#"Server-side enqueue:
 {trigger} {SERVER_ADD_CMD} <URL>
Stream:
 {trigger} {STREAM_GET_CMD} <URL>
Log out and delete your saved credentials:
 {trigger} {LOGOUT_CMD}"#
            );
            msg.push_str(&ext);
        } else {
            let ext = format!(
                r#"Log in:
{trigger} {LOGIN_CMD} <USERNAME> <PASSWORD>"#
            );
            msg.push_str(&ext);
        }
        self.msg.reply(msg).await?;
        Ok(())
    }

    async fn handle_login(&self) -> anyhow::Result<()> {
        let parts = self.extract_parts(LOGIN_CMD, 2)?;
        let username = parts[0];
        let password = parts[1];
        info!("invoked login with username={username}");
        let svc = NiceerrClientService::get();
        svc.auth(username, password).await?;
        AuthService::save_credentials(self.sender_id, username, password)
            .await
            .context("Couldn't save credentials to cache")?;
        self.msg.reply("Logged in successfully.").await?;
        Ok(())
    }

    async fn handle_logout(&self) -> anyhow::Result<()> {
        info!("invoked logout");
        AuthService::delete_credentials(self.sender_id)
            .await
            .context("Couldn't delete credentials from cache")?;
        self.msg.reply("Logged out successfully.").await?;
        Ok(())
    }

    async fn handle_stream(&self) -> anyhow::Result<()> {
        let parts = self.extract_parts(STREAM_GET_CMD, 1)?;
        let url = parts[0];
        info!("invoked stream get: {url}");
        let reply = self.msg.reply("Preparing").await?;
        let upload_handler = TelegramStreamHandler::new(reply, self.sender_id);
        upload_handler.drive(url, &self.msg).await?;
        Ok(())
    }

    async fn handle_server_add(&self) -> anyhow::Result<()> {
        let parts = self.extract_parts(SERVER_ADD_CMD, 1)?;
        let url = parts[0];
        info!("invoked server add: {url}");
        let svc = NiceerrClientService::get();
        let dl_id = svc.server_add(self.sender_id, url).await?;
        self.msg.reply(format!("Enqueued: {dl_id}")).await?;
        Ok(())
    }

    async fn dispatch_inner(&self) -> anyhow::Result<()> {
        let cmd = Self::extract_command(&self.args)
            .ok_or_else(|| anyhow::anyhow!("No command passed. Use the help subcommand."))?;
        if cmd == HELP_CMD {
            return self.help().await;
        } else if !self.is_authenticated && cmd == LOGIN_CMD {
            return self.handle_login().await;
        }

        if !self.is_authenticated {
            bail!("You need to be authenticated to use this bot. Use the login subcommand.")
        }

        if cmd == LOGOUT_CMD {
            return self.handle_logout().await;
        } else if cmd == STREAM_GET_CMD {
            return self.handle_stream().await;
        } else if cmd == SERVER_ADD_CMD {
            return self.handle_server_add().await;
        }

        bail!("`{}` isn't a valid userbot subcommand.", self.args)
    }

    async fn dispatch(&self) {
        match self.dispatch_inner().await {
            Ok(_) => {
                info!("Handle command succeeded.");
            }
            Err(e) => {
                warn!("Handle message failed: {e:?}");
                if BotConfig::get().ignore_unauthenticated && !self.is_authenticated {
                    return;
                }
                self.msg.reply(format!("Command failed: {e:?}")).await.ok();
            }
        }
    }
}
