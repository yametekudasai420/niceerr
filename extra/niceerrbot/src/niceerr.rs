use std::{env, path::PathBuf, sync::LazyLock, time::Instant};

use anyhow::{anyhow, bail, Context};
use async_zip::tokio::read::seek::ZipFileReader;
use flume::Receiver;
use futures_util::TryStreamExt;
use grammers_client::{types::Message, InputMessage};
use log::info;
use serde_json::{json, Value};
use tokio::io::{AsyncSeekExt, AsyncWriteExt, BufReader, BufWriter};
use tokio_util::compat::FuturesAsyncReadCompatExt;

use crate::{
    auth::AuthService,
    callback::WithCallback,
    config::BotConfig,
    init::ClientService,
    temp::SelfDeletingFile,
    util::{bytes_to_human_readable, random_string, random_temp_path},
};

static SERVICE: LazyLock<NiceerrClientService> = LazyLock::new(|| NiceerrClientService {
    client: reqwest::Client::new(),
});

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Event {
    Text(&'static str),
    Progress(usize),
}

pub struct NiceerrClientService {
    client: reqwest::Client,
}

impl NiceerrClientService {
    pub fn get() -> &'static Self {
        &SERVICE
    }

    fn get_url(suffix: &str) -> String {
        let mut url = BotConfig::get().api_url.to_string();
        if url.ends_with("/") {
            url.pop();
        }
        url.push_str(suffix);
        url
    }

    async fn auth_by_id(&self, id: i64) -> anyhow::Result<String> {
        let (username, password) = AuthService::get_creds(id)
            .ok_or_else(|| anyhow!("Couldn't find creds for user {id}"))?;
        self.auth(&username, &password).await
    }

    fn build_request_url(url: &str, stream: bool) -> Value {
        json!({
            "link": url,
            "streamFormat": "Flac",
            "includeCredited": false,
            "allowFallback": true,
            "skipLive": true,
            "stream": stream
        })
    }

    pub async fn server_add(&self, id: i64, url: &str) -> anyhow::Result<String> {
        let token = self.auth_by_id(id).await?;
        let payload = Self::build_request_url(url, false);
        let url = Self::get_url("/api/download/link");
        let resp = self
            .client
            .post(url)
            .bearer_auth(token)
            .json(&payload)
            .send()
            .await?
            .error_for_status()?
            .json::<Value>()
            .await?;
        let Some(id) = resp["downloadSession"]["id"].as_str() else {
            bail!("Got unexpected response: {resp:?}");
        };
        Ok(id.into())
    }

    pub async fn get_streamable_file<C: Fn(Event) + Copy>(
        &self,
        id: i64,
        url: &str,
        callback: C,
    ) -> anyhow::Result<(SelfDeletingFile, String)> {
        callback(Event::Text("Authenticating"));
        let token = self.auth_by_id(id).await?;
        callback(Event::Text("Auth success, requesting stream now"));
        let payload = Self::build_request_url(url, true);
        let url = Self::get_url("/api/download/link");
        let filename = format!("{}.zip", random_string());
        let temp_file = SelfDeletingFile::new(env::temp_dir().join(&filename))
            .await
            .context("Couldn't create temporary file")?;
        let mut buffered = BufWriter::new(temp_file);
        let mut stream = self
            .client
            .post(url)
            .bearer_auth(token)
            .json(&payload)
            .send()
            .await?
            .error_for_status()?
            .bytes_stream();
        callback(Event::Text("Stream request accepted, writing to disk"));

        while let Some(chunk) = stream.try_next().await.transpose() {
            let chunk = chunk.context("Couldn't read chunk from stream")?;
            buffered
                .write_all(&chunk)
                .await
                .context("Couldn't write chunk to temp file")?;
            callback(Event::Progress(chunk.len()));
        }
        buffered.flush().await?;
        buffered.seek(std::io::SeekFrom::Start(0)).await?;

        let temp_file = buffered.into_inner();
        let mut bufreader = BufReader::new(temp_file);
        let mut zip = ZipFileReader::with_tokio(&mut bufreader).await?;

        // We check whether we should send the entire zip or not. If it's just a single song,
        // then can extract it and send it individually instead of sending a zip with just
        // one song.
        let mut file_count = 0;
        let mut file_index = None;

        if zip.file().entries().is_empty() {
            bail!("Empty zip file");
        }

        for idx in 0..zip.file().entries().len() {
            if file_count > 1 {
                file_index = None;
                break;
            }
            let entry = zip.file().entries().get(idx).expect("mem error");
            let path =
                PathBuf::from(String::from_utf8_lossy(entry.filename().as_bytes()).into_owned());

            if path.extension().is_none() {
                continue;
            } else {
                file_index = Some(idx);
                file_count += 1;
            }
        }

        let Some(idx) = file_index else {
            let mut original = bufreader.into_inner();
            original.seek(std::io::SeekFrom::Start(0)).await?;
            return Ok((original, filename));
        };

        callback(Event::Text("Detected single song, extracting"));
        // This is just a single song, extract it
        let name = zip
            .file()
            .entries()
            .get(idx)
            .expect("mem error")
            .filename()
            .as_bytes();
        let path = PathBuf::from(String::from_utf8_lossy(name).into_owned());
        let filename = path
            .file_name()
            .ok_or_else(|| anyhow!("Couldn't extract filename"))?
            .to_string_lossy()
            .to_string();
        let mut entry_reader = zip
            .reader_with_entry(idx)
            .await
            .context("Couldn't read zip entry")?
            .compat();

        let temp_song = SelfDeletingFile::new(random_temp_path())
            .await
            .context("Can't extract single song")?;
        let mut buf_song = BufWriter::new(temp_song);
        tokio::io::copy(&mut entry_reader, &mut buf_song)
            .await
            .context("Couldn't write single song to temp file")?;
        buf_song.flush().await.context("Couldn't flush temp file")?;
        buf_song.seek(std::io::SeekFrom::Start(0)).await?;
        Ok((buf_song.into_inner(), filename))
    }

    pub async fn auth(&self, username: &str, password: &str) -> anyhow::Result<String> {
        let payload = json!({
            "username": username,
            "password": password
        });
        let url = Self::get_url("/api/user/login");
        let resp = self
            .client
            .post(url)
            .json(&payload)
            .send()
            .await?
            .error_for_status()?
            .json::<Value>()
            .await
            .context("couldn't deserialize JSON response")?;

        let Some(token) = resp["token"].as_str() else {
            bail!("missing token in response");
        };
        Ok(token.into())
    }
}

pub struct TelegramStreamHandler {
    reply: Message,
    sender_id: i64,
}

impl TelegramStreamHandler {
    pub fn new(msg: Message, sender_id: i64) -> Self {
        Self {
            reply: msg,
            sender_id,
        }
    }

    async fn update_progress(msg: Message, rx: Receiver<Event>) {
        let mut buff = String::new();
        let now = Instant::now();
        let mut last_updated = Instant::now();
        let mut bytes = 0;
        while let Ok(event) = rx.recv_async().await {
            match event {
                Event::Text(text) => {
                    let elapsed = now.elapsed();
                    let text = format!("{text}: T+{elapsed:?}\n");
                    buff.push_str(&text);

                    if bytes > 0 {
                        let readable = bytes_to_human_readable(bytes as u64);
                        buff.push_str(&format!("Finished transfer: {readable}: T+{elapsed:?}\n"));
                        bytes = 0;
                    }
                    msg.edit(buff.clone()).await.ok();
                }
                Event::Progress(progress) => {
                    bytes += progress;
                }
            }

            if last_updated.elapsed().as_millis() > 1000 {
                if bytes > 0 {
                    let readable = bytes_to_human_readable(bytes as u64);
                    let text = format!("{buff}\nProgress: {readable}");
                    msg.edit(text).await.ok();
                } else {
                    msg.edit(buff.clone()).await.ok();
                }
                last_updated = Instant::now();
            }
        }
    }

    pub async fn drive(self, url: &str, to: &Message) -> anyhow::Result<()> {
        let svc = NiceerrClientService::get();
        let (tx, rx) = flume::unbounded();
        tokio::spawn(Self::update_progress(self.reply.clone(), rx));
        let callback = |bytes| {
            tx.send(bytes).ok();
        };
        let (mut stream, filename) = svc
            .get_streamable_file(self.sender_id, url, callback)
            .await?;
        info!("Finished downloading stream: {filename}");
        tx.send(Event::Text("Finished stream download, uploading now"))
            .ok();
        let size = stream.inner().metadata().await?.len();
        let client = ClientService::get_client();
        let callback = |_part, bytes| {
            tx.send(Event::Progress(bytes)).ok();
        };
        let uploaded = client
            .upload_stream_cbk(&mut stream, size as usize, filename, callback)
            .await?;
        tx.send(Event::Text("Finished telegram upload")).ok();
        let doc = InputMessage::default().document(uploaded);
        to.reply(doc).await?;
        Ok(())
    }
}

impl Drop for TelegramStreamHandler {
    fn drop(&mut self) {
        let clone = self.reply.clone();
        tokio::spawn(async move {
            clone.delete().await.ok();
        });
    }
}
