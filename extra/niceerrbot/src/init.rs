use std::sync::{OnceLock, RwLock};

use anyhow::{bail, Context};
use grammers_client::{session::Session, Client, Config, SignInError};
use log::info;

use crate::config::BotConfig;

static CLIENT: OnceLock<RwLock<Client>> = OnceLock::new();

pub struct ClientService;

impl ClientService {
    pub fn get_client() -> Client {
        CLIENT
            .get()
            .expect("Client is not initialized")
            .read()
            .expect("Can't unlock client mutex")
            .clone()
    }

    async fn try_build_client() -> anyhow::Result<Client> {
        let conf = BotConfig::get();

        let client = Client::connect(Config {
            session: Session::load_file_or_create(&conf.session_file_location)?,
            api_id: conf.api_id,
            api_hash: conf.api_hash.clone(),
            params: Default::default(),
        })
        .await
        .context("Can't initialize telegram client")?;

        info!("Successfully connected to telegram network. Authenticating...");

        if !client.is_authorized().await? {
            info!("Client isn't authorized, signing in...");
            let phone_p = inquire::Text::new("Enter phone number with country code");
            let phone = phone_p.prompt().context("Can't read phone number")?;

            let token = client
                .request_login_code(&phone)
                .await
                .context("Couldn't request login code - maybe phone number is invalid?")?;

            let code_p =
                inquire::Password::new("Enter login code you just received (input will be hidden)")
                    .without_confirmation();
            let code = code_p.prompt().context("Can't read login code")?;

            let signed_in = client.sign_in(&token, &code).await;
            match signed_in {
                Err(SignInError::PasswordRequired(password_token)) => {
                    let mfa_p = inquire::Password::new("Enter MFA password (input will be hidden)")
                        .without_confirmation();
                    let mfa = mfa_p.prompt().context("Can't read MFA password")?;
                    client
                        .check_password(password_token, mfa.trim())
                        .await
                        .context("MFA Password is incorrect")?;
                }
                Ok(_) => (),
                Err(e) => {
                    bail!("Sign in error: {e:?}");
                }
            };
            client
                .session()
                .save_to_file(&conf.session_file_location)
                .context("Can't save session to file")?;
            info!("Session saved to {}", conf.session_file_location);
        } else {
            info!("Reusing existing session");
        }
        Ok(client)
    }

    pub async fn try_refresh_client() -> anyhow::Result<()> {
        let client = Self::try_build_client().await?;

        let mut guard = CLIENT
            .get()
            .expect("Client is not initialized")
            .write()
            .expect("Can't unlock client mutex");
        *guard = client;

        Ok(())
    }

    pub async fn try_init() -> anyhow::Result<()> {
        if CLIENT.get().is_some() {
            return Ok(());
        }

        let client = Self::try_build_client().await?;

        CLIENT
            .set(RwLock::new(client))
            .expect("Client is already initialized");

        Ok(())
    }
}
