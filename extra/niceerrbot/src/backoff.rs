use std::time::Duration;
use tokio::time::sleep;

pub struct LinearBackoffSleep {
    current_step: u32,
}

impl Default for LinearBackoffSleep {
    fn default() -> Self {
        Self::new()
    }
}

impl LinearBackoffSleep {
    pub fn new() -> Self {
        Self { current_step: 0 }
    }

    pub fn reset(&mut self) {
        self.current_step = 0;
    }

    pub fn current_sleep_duration(&self) -> Duration {
        Duration::from_secs((1 + self.current_step).min(30) as u64)
    }

    pub async fn tick(&mut self) {
        let duration = self.current_sleep_duration();
        sleep(duration).await;
        self.current_step = self.current_step.saturating_add(1);
    }
}
