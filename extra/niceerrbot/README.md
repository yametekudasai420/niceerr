# Telegram userbot for niceerr

This folder contains a Telegram userbot that you can use to consume Niceerr using telegram.

# **IMPORTANT**

This userbot will ask you for your phone number and 2FA password if configured.

## Features

- It's small and cool
- Lightning fast
- Leverages Niceerr's auth system
- Automatically cleans up temporary files with dark magic
- 1:1 Telegram user to Niceerr user assignments 
- You can customize the trigger keyword

## Pre-Setup

If you already compiled and configured Niceerr, you don't need anything else.

## Configuration options

This is a quite simple bot, and it doesn't need anything at all other than your phone number, **which will be prompted when you first use this program.**


|      Env variable       |   Type/default    | Required? | Description |
|:-----------------------:|:-----------------:|:---------:|:------------|
|        `api_url`        | String/No default |  **Yes**  | Niceerr instance URL. Must not end with a trailing slash (`/`). |
|    `trigger_keyword`    |  String/`:dzbot`  |    No     | Keyword to listen to on all chats. **Make sure you use a special, uncommon word!** |
| `session_file_location` | String/`niceerrbot.session` |    No     | Telegram Session storage cache |
|      `db_location`      | String/`db.json`  |    No     | User database location |
|`ignore_unauthenticated` | bool/`false`      |    No     | Don't reply with error messages to users that aren't authenticated.*  |

*Note: the `login` command will still work.


You can save all these variables in a `.env` file and the program will automatically pick them up.

## Setup

This folder contains a regular rust program; just run it with `cargo run --release`.

Example output:

```
[2024-12-20T03:13:34Z INFO  niceerrbot::config] Loaded config: BotConfig {
        trigger_keyword: ":dzbot",
        db_location: "db.json",
        session_file_location: "niceerrbot.session",
        api_id: 611335,
        api_hash: "d524b414d21f4d37f08684c1df41ac9c",
        api_url: Url {
            scheme: "https",
            cannot_be_a_base: false,
            username: "",
            password: None,
            host: Some(
                Domain(
                    "example.com",
                ),
            ),
            port: None,
            path: "/",
            query: None,
            fragment: None,
        },
    }
[2024-12-20T03:13:37Z INFO  niceerrbot::init] Successfully connected to telegram network. Authenticating...
[2024-12-20T03:13:37Z INFO  niceerrbot::init] Reusing existing session
[2024-12-20T03:13:37Z INFO  niceerrbot] Ready to serve requests.
```

Once the bot is running, you can use `:dzbot help` for more info. **This assumes you didn't change the trigger keyword and that `ignore_unauthenticated` isn't set.**

If `ignore_unauthenticated` is set, then you can use `:dzbot login USERNAME PASSWORD` to login using your Niceerr credentials.