use axum::{
    body::Body,
    extract::Path,
    http::StatusCode,
    response::{sse::KeepAlive, IntoResponse, Sse},
    routing::{delete, get, post},
    Extension, Router,
};
use axum_extra::extract::Query;
use log::info;
use niceerr_config::Config;
use niceerr_dz::error::FallibleAPIResponse;
use niceerr_entity::{
    assigned_permission::Permission,
    download_session::{
        DownloadSessionWSongInfo, Model as DownloadSession, OutcomeKind, SearchFilterDlSession,
    },
    pagination::PaginationParams,
    song::{Model as SongModel, SearchFilterSong},
    user::Model as UserModel,
};
use reqwest::header;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{collections::HashMap, time::Duration};
use uuid::Uuid;

use crate::{
    arl_service::ArlService,
    download::ty::{DownloadOptions, DownloadService, SerializedState},
    error::ApiError,
    limit_service::WrappedUser,
    ongoing_sse::StateSender,
    pagination::PaginatedResponse,
    server_relay_service::get_relayed_song,
    storage_zip::ZipDownload,
    ty::{ApiResult, BodylessResult, Json, JsonResult},
    url_parse::{DzLink, DzLinkKind},
};

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct DownloadRequestPayload {
    pub link: String,
    #[serde(flatten)]
    options: DownloadOptions,
}

async fn link_download(
    Extension(caller): Extension<UserModel>,
    Json(params): Json<DownloadRequestPayload>,
) -> ApiResult<impl IntoResponse> {
    let conf = Config::get();
    // Check whether the requested features are enabled (global check)
    match (
        params.options.stream,
        conf.enable_server_downloads,
        conf.enable_stream_downloads,
    ) {
        (true, _, true) | (false, true, _) => {}
        _ => {
            info!("can't continue: feature is globally disabled");
            return Err(ApiError::DisabledFeature);
        }
    };
    // Check if user has permission to perform this download
    match (
        params.options.stream,
        caller.has_permission(&Permission::PerformServerSideDownload),
        caller.has_permission(&Permission::PerformStreamDownload),
    ) {
        (true, _, true) | (false, true, _) => {}
        _ => {
            info!("can't continue: feature is disabled for user");
            return Err(ApiError::DisabledFeature);
        }
    };
    let user = WrappedUser::new(caller)?;

    info!("requested download {:?}", params);
    let dzlink = DzLink::from_url(&params.link).await?;
    let response = match dzlink.kind {
        DzLinkKind::Artist(id) => {
            DownloadService::download_artist(user, id, params.options).await?
        }
        DzLinkKind::Album(id) => DownloadService::download_album(user, id, params.options).await?,
        DzLinkKind::Song(id) => DownloadService::download_song(user, id, params.options).await?,
        DzLinkKind::Playlist(id) => {
            DownloadService::download_playlist(user, id, params.options).await?
        }
    };
    Ok(response.into_response())
}

async fn list_ongoing_downloads(
    Extension(caller): Extension<UserModel>,
) -> JsonResult<Vec<SerializedState>> {
    caller.verify_has_permission(&Permission::ReadOngoingDownload)?;
    let state = DownloadService::get_serializable_instance(&caller);
    Ok((StatusCode::OK, axum::Json(state)))
}

async fn list_ongoing_downloads_sse(
    Extension(caller): Extension<UserModel>,
) -> ApiResult<impl IntoResponse> {
    caller.verify_has_permission(&Permission::ReadOngoingDownload)?;
    let sse =
        Sse::new(StateSender::new(caller)).keep_alive(KeepAlive::new().text("ping").interval(
            Duration::from_secs(Config::get().sse_keep_alive_interval_sec as u64),
        ));
    let headers = [(
        "x-keepalive-interval-sec",
        Config::get().sse_keep_alive_interval_sec.to_string(),
    )];

    Ok((headers, sse).into_response())
}

async fn list_download_sessions(
    Extension(caller): Extension<UserModel>,
    Query(params): Query<PaginationParams>,
    Query(filter): Query<SearchFilterDlSession>,
) -> ApiResult<PaginatedResponse<DownloadSessionWSongInfo>> {
    caller.verify_has_permission(&Permission::ReadDownloadSession)?;
    let result = DownloadSession::list_paginated(&caller, &params, filter).await?;
    Ok(PaginatedResponse::from(result))
}

async fn list_downloaded_songs(
    Extension(caller): Extension<UserModel>,
    Query(params): Query<PaginationParams>,
    Query(filter): Query<SearchFilterSong>,
) -> ApiResult<PaginatedResponse<SongModel>> {
    caller.verify_has_permission(&Permission::ReadDownloadSession)?;
    let result = SongModel::list_paginated(&caller, &params, filter).await?;
    Ok(PaginatedResponse::from(result))
}

#[derive(Deserialize, Debug, Default)]
#[serde(rename_all = "camelCase")]
pub struct ZipDownloadIgnoreError {
    ignore_errors: Option<bool>,
}

async fn zipped_download(
    Extension(caller): Extension<UserModel>,
    Path((user_id, id)): Path<(Uuid, Uuid)>,
    Query(params): Query<ZipDownloadIgnoreError>,
) -> ApiResult<impl IntoResponse> {
    if !Config::get().enable_stream_downloads {
        return Err(ApiError::DisabledFeature);
    }
    let other = caller
        .find_scoped(user_id)
        .await?
        .ok_or(ApiError::NotFound)?;
    caller.verify_has_authority_over(&other, &Permission::DownloadServerSideDownloadedContent)?;
    let download = DownloadSession::find_on_storage(id)
        .await?
        .ok_or(ApiError::NotFound)?;
    if download.outcome == OutcomeKind::InProgress {
        return Err(ApiError::InvalidRequest);
    }

    let stream = ZipDownload::new(download, params.ignore_errors.unwrap_or_default())?
        .stream_from_storage(&caller)
        .await?;

    let headers = [
        (header::CONTENT_TYPE, "application/x-zip"),
        (
            header::CONTENT_DISPOSITION,
            &format!("attachment; filename=\"{}\"", "zipped-music.zip"),
        ),
    ];
    Ok((headers, Body::from_stream(stream)).into_response())
}

async fn stop_download(
    Extension(caller): Extension<UserModel>,
    Path((user, id)): Path<(Uuid, Uuid)>,
) -> BodylessResult {
    caller.verify_has_permission(&Permission::StopOngoingDownload)?;
    let user = UserModel::find_by_id(user)
        .await?
        .ok_or(ApiError::NotFound)?;
    let request = DownloadService::find_ongoing(&user, &id)?;
    caller.verify_has_authority_over(&request.user, &Permission::StopOngoingDownload)?;
    info!(
        "user '{}' stopped request '{}' by user '{}'",
        caller.username, id, request.user.username
    );
    request.stop();
    Ok(StatusCode::NO_CONTENT)
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DeezerApiPayload {
    method: String,
    payload: HashMap<String, Value>,
}

async fn deezer_api_debug(Json(params): Json<DeezerApiPayload>) -> JsonResult<FallibleAPIResponse> {
    let client = ArlService::get().get_primary_client().await?;
    let result = client
        .call_method_proxy(&params.method, params.payload)
        .await?;
    Ok((StatusCode::OK, axum::Json(result)))
}

pub fn media_router() -> Router {
    let mut r = Router::new()
        .route("/download/{user}/{id}", delete(stop_download))
        .route("/download/link", post(link_download))
        .route("/download/zip/{user}/{id}", get(zipped_download))
        .route("/download/ongoing", get(list_ongoing_downloads))
        .route("/download/ongoingsse", get(list_ongoing_downloads_sse))
        .route("/download/history", get(list_download_sessions))
        .route("/download/history/songs", get(list_downloaded_songs));
    if Config::get().server_enable_relay {
        r = r.route("/download/relayed", post(get_relayed_song));
    }
    if cfg!(debug_assertions) {
        r = r.route("/deezer-api", post(deezer_api_debug));
    }

    r
}
