use axum::{
    extract::Path,
    http::StatusCode,
    routing::{delete, get, post, put},
    Extension, Router,
};
use axum_extra::extract::Query;
use futures_util::StreamExt;
use log::{error, info};
use niceerr_entity::{
    assigned_permission::Permission,
    pagination::PaginationParams,
    user::{Model as UserModel, SearchFilterUser, UpdatableUser, UserWExtendedInfo},
};
use serde::{Deserialize, Serialize};
use tracing::Span;
use uuid::Uuid;

use crate::{
    error::ApiError,
    jwt::{TokenResponse, UserClaim},
    limit_service::LimitService,
    pagination::PaginatedResponse,
    ty::{ApiResult, BodylessResult, Json, JsonResult},
};

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct UserCredentials {
    pub username: String,
    pub password: String,
}

// Creates an user.
async fn create_user(
    Extension(caller): Extension<UserModel>,
    Json(mut user): Json<UserModel>,
) -> JsonResult<UserModel> {
    if !caller.superuser {
        info!("can't create another user without superuser permissions");
        return Err(ApiError::InsufficientPermissions);
    }
    user.level = caller.level + 1;
    Ok((
        StatusCode::CREATED,
        axum::Json(user.create(Some(&caller)).await?),
    ))
}

// Login endpoint
pub async fn login(Json(credentials): Json<UserCredentials>) -> JsonResult<TokenResponse> {
    let user = UserModel::find_by_username(&credentials.username)
        .await
        .inspect_err(|e| info!("cannot find user: '{}': {e}", credentials.username))?
        .ok_or(ApiError::Unauthorized)?;

    Span::current().record("username", &*user.username);
    if !user.enabled {
        info!("user disabled");
        return Err(ApiError::Unauthorized);
    }
    user.verify_password(&credentials.password).await?;
    info!("successful auth for user {} ('{}')", user.id, user.username);
    Ok((StatusCode::OK, axum::Json(user.new_jwt()?)))
}

/// This endpoint only requires the user to send an empty request with a valid token, and we'll grant
/// a brand new token with a refreshed lifetime.
pub async fn refresh_token(Extension(user): Extension<UserModel>) -> JsonResult<TokenResponse> {
    Ok((StatusCode::OK, axum::Json(user.new_jwt()?)))
}

async fn list_users(
    Extension(caller): Extension<UserModel>,
    Query(params): Query<PaginationParams>,
    Query(filter): Query<SearchFilterUser>,
) -> ApiResult<PaginatedResponse<UserWExtendedInfo>> {
    Ok(PaginatedResponse::from(
        UserModel::list_paginated(&caller, &params, filter).await?,
    ))
}

// Deletes an user by ID.
// DELETE http://server/user/<UUID>
async fn delete_user(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
) -> BodylessResult {
    caller.verify_has_permission(&Permission::DeleteUser)?;
    let user = UserModel::find_by_id(id).await?.ok_or(ApiError::NotFound)?;
    caller.verify_has_authority_over(&user, &Permission::DeleteUser)?;
    if LimitService::get().has_permit(user.id) {
        return Err(ApiError::LockedResource);
    }

    user.delete_p().await?;
    Ok(StatusCode::NO_CONTENT)
}

async fn update_user(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
    Json(other): Json<UpdatableUser>,
) -> JsonResult<UserModel> {
    if !(caller.has_permission(&Permission::UpdateUserBasicInfo)
        || caller.has_permission(&Permission::UpdateUserExtendedInfo))
    {
        info!("caller doesn't have update user perms");
        return Err(ApiError::InsufficientPermissions);
    }
    let user = UserModel::find_by_id(id).await?.ok_or(ApiError::NotFound)?;
    let updated = user.update(&caller, other).await?;
    Ok((StatusCode::OK, axum::Json(updated)))
}

#[derive(Serialize, Deserialize, Default, Debug)]
#[serde(rename_all = "camelCase")]
struct PruneOptions {
    force_prune: Option<bool>,
    all_sessions: Option<bool>,
}

#[derive(Serialize, Deserialize, Default, Debug)]
#[serde(rename_all = "camelCase")]
struct PruneResponse {
    success: u32,
    error: u32,
    sessions_pruned: u32,
}

async fn prune_download_sessions(
    caller: UserModel,
    id: Uuid,
    dl_sess_id: Option<Uuid>,
    opts: PruneOptions,
) -> JsonResult<PruneResponse> {
    caller.verify_has_permission(&Permission::PruneDownloadSession)?;
    let other = UserModel::find_by_id(id).await?.ok_or(ApiError::NotFound)?;
    caller.verify_has_authority_over(&other, &Permission::PruneDownloadSession)?;
    // If this user doesn't have a persistence time set, then it doesn't make any sense
    // to prune their downloads. We don't want to prune all sessions for a user unless
    // we're specifically asked to do so.
    if dl_sess_id.is_none()
        && other.download_session_persist_minutes == 0
        && !opts.all_sessions.unwrap_or_default()
    {
        info!(
            "User {} ({}) has no persistence time set and no session ID was specified.",
            other.id, other.username
        );
        return Err(ApiError::InvalidRequest);
    }
    info!(
        "{} ({}) is prunning sessions for {}: session={dl_sess_id:?}, {opts:#?}",
        caller.id, caller.username, other.id
    );
    let mut prune_response = PruneResponse::default();

    let mut sessions = other
        .pruneable_download_sessions(
            dl_sess_id,
            opts.force_prune.unwrap_or_default(),
            opts.all_sessions.unwrap_or_default(),
        )
        .await?;
    while let Some(sess) = sessions.next().await.transpose()? {
        let mut songs = sess.songs_as_stream().await?;
        while let Some(song) = songs.next().await.transpose()? {
            match song.prune().await {
                Ok(_) => prune_response.success += 1,
                Err(e) => {
                    prune_response.error += 1;
                    error!("Error pruning {} @ {:?}: {e}", song.id, song.download_path);
                }
            }
        }
        prune_response.sessions_pruned += 1;
        sess.mark_pruned().await?;
    }
    info!(
        "Finished pruning (user: {}, session: {:?}): {prune_response:#?}",
        id, dl_sess_id
    );

    Ok((StatusCode::OK, axum::Json(prune_response)))
}

async fn prune_all_user_downloads(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
    Query(opts): Query<PruneOptions>,
) -> JsonResult<PruneResponse> {
    prune_download_sessions(caller, id, None, opts).await
}

async fn prune_user_download_id(
    Extension(caller): Extension<UserModel>,
    Path((id, dl_sess_id)): Path<(Uuid, Uuid)>,
    Query(opts): Query<PruneOptions>,
) -> JsonResult<PruneResponse> {
    prune_download_sessions(caller, id, Some(dl_sess_id), opts).await
}

pub fn user_router() -> Router {
    Router::new()
        .route("/user", post(create_user))
        .route("/user", get(list_users))
        .route("/user/{id}", delete(delete_user))
        .route("/user/{id}", put(update_user))
        .route("/user/prune/{id}", delete(prune_all_user_downloads))
        .route(
            "/user/prune/{id}/{dl_sess_id}",
            delete(prune_user_download_id),
        )
        .route("/user/refresh", post(refresh_token))
}
