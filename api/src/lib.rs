use std::time::Duration;

use admin_console_endpoints::admin_console_router;
use anyhow::Context;
use axum::body::Body;
use axum::{
    extract::MatchedPath, http::Request, middleware as ax_middleware, routing::post, Router,
};
use client_relay_service::RelayClientService;
use info::{init_uptime_tracker, prometheus_router};
use log::{info, warn};
use migration::{Migrator, MigratorTrait};
use niceerr_config::Config;
use niceerr_db::DBConfig;
use server_listener::spawn_servers;
use tower::ServiceBuilder;
use tower_http::cors::{Any, CorsLayer};
use tower_http::trace::TraceLayer;
use tracing::{field::Empty, info_span};
use url_parse::init_url_parser_regex;
use uuid::Uuid;

use crate::info::info_router;
use crate::prune_service::PruneService;
use crate::webhook_service::WebhookService;
use crate::{
    arl::arl_router, arl_service::ArlService, download::ty::DownloadService,
    limit_service::LimitService, media::media_router, startup::exec_db_startup_tasks,
    user::user_router,
};

mod admin_console;
pub mod admin_console_endpoints;
mod arl;
mod arl_service;
mod backoff;
mod cancellation;
mod clean_tmp;
mod client_relay_service;
mod download;
mod dz;
mod error;
pub mod info;
mod jwt;
mod limit_service;
mod media;
mod middleware;
mod ongoing_sse;
mod pagination;
mod prune_service;
mod scheduled_tracker;
mod server_listener;
mod server_relay_service;
pub mod shutdown;
mod slow_stream;
mod startup;
mod storage_zip;
pub mod tmpfile;
mod ty;
mod url_parse;
mod user;
mod webhook_service;

// Spins up an axum server.
pub async fn spin_up_server() -> anyhow::Result<()> {
    let config = Config::get();
    // Verify CORS origins are properly set up before we even start
    // any services.
    let mut cors = CorsLayer::new()
        .allow_methods(Any)
        .allow_headers(Any)
        .expose_headers(Any);
    if config.dangerous_cors_origin_allow_any {
        warn!("Security risk: 'dangerous_cors_origin_allow_any' is set to true.");
        cors = cors.allow_origin(Any);
    } else {
        // Parse cors origins,
        let origin = config
            .cors_origins
            .as_ref()
            .ok_or_else(|| anyhow::anyhow!("cors_origins is not set"))?
            .iter()
            .map(|it| it.to_string().parse())
            .collect::<Result<Vec<_>, _>>()
            .context("cors_origins: got malformed URL")?;
        cors = cors.allow_origin(origin);
    }

    DBConfig::init_db_connection().await?;
    let db = DBConfig::get_connection();
    info!("Running pending migrations...");
    Migrator::up(db, None).await?;
    tokio::spawn(exec_db_startup_tasks());
    ArlService::init_tasks();
    DownloadService::init();
    LimitService::init();
    PruneService::init();
    WebhookService::init();
    RelayClientService::init();
    shutdown::init();
    init_uptime_tracker();
    init_url_parser_regex();

    let unauthenticated_routes = Router::new().route("/user/login", post(user::login));

    let authenticated_api_routes = Router::new()
        .merge(user_router())
        .merge(media_router())
        .merge(arl_router())
        .merge(info_router())
        .merge(admin_console_router());

    let sb = ServiceBuilder::new().layer(
        TraceLayer::new_for_http()
            .make_span_with(|request: &Request<_>| {
                let path = request
                    .extensions()
                    .get::<MatchedPath>()
                    .map(MatchedPath::as_str);
                let uuid = Uuid::new_v4();

                info_span!(
                    "niceerr",
                    uuid = %uuid,
                    user_id = Empty,
                    username = Empty,
                    v = ?request.version(),
                    path,
                    method = ?request.method(),
                )
            })
            .on_response(
                |response: &axum::http::Response<Body>,
                 latency: Duration,
                 _span: &tracing::Span| {
                    tracing::info!("ret status={}, latency={:?}", response.status(), latency);
                },
            ),
    );

    let mut app = Router::new()
        .nest("/api", authenticated_api_routes)
        .route_layer(ax_middleware::from_fn(middleware::api_auth))
        .nest("/api", unauthenticated_routes)
        .route_layer(sb);

    if Config::get().prometheus_basic_auth_credentials.is_some() {
        info!("Enabling prometheus endpoint at /api/metrics");
        app = app.merge(prometheus_router());
    }
    app = app.layer(cors);
    spawn_servers(app).await?;
    shutdown::wait_for_download_completion().await;
    Ok(())
}
