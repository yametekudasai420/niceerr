use axum::{extract::Request, http::HeaderMap, middleware::Next, response::Response};
use base64::{engine::general_purpose, Engine};
use log::{error, info};
use niceerr_config::Config;
use niceerr_entity::{assigned_permission::Permission, user::Model as UserModel};
use tracing::Span;

use crate::{error::ApiError, jwt::UserClaim, ty::ApiResult};

// Auth middleware
pub async fn api_auth(headers: HeaderMap, mut request: Request, next: Next) -> ApiResult<Response> {
    let token = headers
        .get("Authorization")
        .ok_or(ApiError::Unauthorized)
        .and_then(|bearer| bearer.to_str().map_err(|_| ApiError::Unauthorized))
        .and_then(|token| token.strip_prefix("Bearer ").ok_or(ApiError::Unauthorized))?;
    if token.is_empty() {
        return Err(ApiError::Unauthorized);
    }

    let user = UserModel::parse_jwt(token).await?;
    let current_span = Span::current();
    current_span.record("username", &*user.username);
    current_span.record("user_id", user.id.to_string());
    if !user.enabled {
        info!("auth/jwt: user is disabled");
        return Err(ApiError::Unauthorized);
    }
    request.extensions_mut().insert(user);
    Ok(next.run(request).await)
}

pub async fn prometheus_basic_auth(
    headers: HeaderMap,
    request: Request,
    next: Next,
) -> ApiResult<Response> {
    let token = headers
        .get("Authorization")
        .ok_or(ApiError::Unauthorized)
        .and_then(|bearer| bearer.to_str().map_err(|_| ApiError::Unauthorized))
        .and_then(|token| token.strip_prefix("Basic ").ok_or(ApiError::Unauthorized))?;
    if token.is_empty() {
        return Err(ApiError::Unauthorized);
    }
    let Some(expected) = &Config::get().prometheus_basic_auth_credentials else {
        return Err(ApiError::DisabledFeature);
    };
    let expected = general_purpose::STANDARD.encode(expected);

    if expected != token {
        info!("auth/basic: invalid credentials");
        return Err(ApiError::Unauthorized);
    }
    Ok(next.run(request).await)
}

// super-superuser middleware
pub async fn require_admin_console_perm(request: Request, next: Next) -> ApiResult<Response> {
    let user = request.extensions().get::<UserModel>().ok_or_else(|| {
        error!("[BUG] middleware expected user in extensions");
        ApiError::InternalError
    })?;
    user.verify_has_permission(&Permission::AdminConsoleAccess)?;
    Ok(next.run(request).await)
}
