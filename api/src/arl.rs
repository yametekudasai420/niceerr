use axum::{
    extract::Path,
    http::StatusCode,
    routing::{delete, get, post, put},
    Extension, Router,
};
use axum_extra::extract::Query;
use chrono::DateTime;
use log::info;
use niceerr_entity::{
    arl::{ArlPayload, Model as ArlModel, SearchFilterArl, UpdatableArl},
    assigned_permission::Permission,
    pagination::PaginationParams,
    user::Model as UserModel,
};
use serde::Serialize;
use uuid::Uuid;

use crate::{
    arl_service::{ArlService, IntoRefreshedClient},
    error::ApiError,
    pagination::PaginatedResponse,
    scheduled_tracker::ScheduledTaskTracker,
    ty::{ApiResult, BodylessResult, Json, JsonResult},
};

async fn create(
    Extension(caller): Extension<UserModel>,
    Json(payload): Json<ArlPayload>,
) -> JsonResult<ArlModel> {
    caller.verify_has_permission(&Permission::CreateArl)?;
    ScheduledTaskTracker::check_arl_running()?;
    let dz_client = ArlService::new_arl(&payload.token).await?;
    let expiration =
        DateTime::from_timestamp(dz_client.expiration(), 0).ok_or(ApiError::InvalidRequest)?;
    let arl = ArlModel::new(&caller, payload, &expiration, dz_client.country().into()).await?;
    ArlService::get().on_change_trigger_refresh(arl.id);
    Ok((StatusCode::CREATED, axum::Json(arl)))
}

async fn list_all(
    Extension(caller): Extension<UserModel>,
    Query(params): Query<PaginationParams>,
    Query(filter): Query<SearchFilterArl>,
) -> ApiResult<PaginatedResponse<ArlModel>> {
    Ok(PaginatedResponse::from(
        ArlModel::list_paginated(&caller, &params, filter).await?,
    ))
}

async fn update(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
    Json(other): Json<UpdatableArl>,
) -> JsonResult<ArlModel> {
    caller.verify_has_permission(&Permission::UpdateArl)?;
    let (arl, user) = ArlModel::find_with_user(&id).await?;
    caller.verify_has_authority_over(&user, &Permission::UpdateArl)?;
    ScheduledTaskTracker::check_arl_running()?;
    let mut expires_at = None;
    let mut country = None;
    let refresh_prio_index = other.priority.is_some();
    if let Some(ref token) = other.token {
        let dz_client = ArlService::new_arl(&token).await?;
        expires_at = Some(
            DateTime::from_timestamp(dz_client.expiration(), 0).ok_or(ApiError::InvalidRequest)?,
        );
        country = Some(dz_client.country().into());
    }
    let arl = arl.update(other, expires_at, country).await?;
    if refresh_prio_index {
        ArlModel::reset_priority_index().await?;
    }
    ArlService::get().on_change_trigger_refresh(arl.id);
    Ok((StatusCode::CREATED, axum::Json(arl)))
}

#[derive(Serialize)]
pub struct OptionalArl {
    pub arl: Option<ArlModel>,
}

/// Verify whether any given ARL is working or not.
///
/// This endpoint needs admin permissions and it also requires the caller to have
/// visibility over the user who created the ARL.
async fn check_item(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
) -> JsonResult<ArlModel> {
    caller.verify_has_permission(&Permission::ReadArl)?;
    let (arl, user) = ArlModel::find_with_user(&id).await?;
    info!("Checking whether ARL {}/{} is ok", arl.id, arl.alias);
    caller.verify_has_authority_over(&user, &Permission::ReadArl)?;
    let (_client, updated_arl) = arl.into_refreshed_client().await?;
    Ok((StatusCode::OK, axum::Json(updated_arl)))
}

async fn delete_item(
    Extension(caller): Extension<UserModel>,
    Path(id): Path<Uuid>,
) -> BodylessResult {
    caller.verify_has_permission(&Permission::DeleteArl)?;
    let (arl, user) = ArlModel::find_with_user(&id).await?;
    caller.verify_has_authority_over(&user, &Permission::DeleteArl)?;
    ScheduledTaskTracker::check_arl_running()?;
    let uuid = arl.id;
    arl.delete_p().await?;
    // If we're deleting the currently cached token, we'll need to refresh
    // the cache again
    ArlService::get().on_change_trigger_refresh(uuid);
    Ok(StatusCode::NO_CONTENT)
}

async fn trigger_refresh(Extension(caller): Extension<UserModel>) -> BodylessResult {
    caller.verify_has_permission(&Permission::TriggerArlRefresh)?;
    ScheduledTaskTracker::check_arl_running()?;
    // Just get the primary client to refresh the hot cache
    ArlService::get().on_change_trigger_forced_refresh();
    ArlService::get().get_primary_client().await?;
    Ok(StatusCode::NO_CONTENT)
}

pub fn arl_router() -> Router {
    Router::new()
        .route("/arl", get(list_all))
        .route("/arl", post(create))
        .route("/arl/{id}", put(update))
        .route("/arl/{id}", delete(delete_item))
        .route("/arl/{id}/check", get(check_item))
        .route("/arl/refresh", post(trigger_refresh))
    // .route("/arl/status", get(system_has_valid_arl))
}
