use std::{sync::LazyLock, time::Duration};

use chrono::Utc;
use jsonwebtoken::{encode, Algorithm, Header, Validation};
use log::{error, info};
use niceerr_config::Config;
use niceerr_entity::user::Model as UserModel;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{error::ApiError, ty::ApiResult};

static JWT_HEADER: LazyLock<Header> = LazyLock::new(|| Header::new(jsonwebtoken::Algorithm::HS256));
static VALIDATION: LazyLock<Validation> = LazyLock::new(|| Validation::new(Algorithm::HS256));

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct TokenResponse {
    pub token: String,
    pub user: UserModel,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Claim {
    pub sub: Uuid,  // subject
    pub exp: usize, // expiration
}

pub trait UserClaim {
    fn new_jwt(self) -> ApiResult<TokenResponse>;
    async fn parse_jwt(token: &str) -> ApiResult<UserModel>;
}

impl UserClaim for UserModel {
    fn new_jwt(self) -> ApiResult<TokenResponse> {
        let now = Utc::now();
        let expires_in = Duration::from_secs(Config::get().jwt_expiration_sec.into());
        let exp = (now + expires_in).timestamp() as usize;
        let claim = Claim { sub: self.id, exp };
        let encoding_key = &Config::get().encoding_key;
        let jwt = encode(&JWT_HEADER, &claim, encoding_key).map_err(|e| {
            error!("auth/jwt: couldn't encode jwt: {e:?}");
            ApiError::InternalError
        })?;
        Ok(TokenResponse {
            token: jwt,
            user: self,
        })
    }

    // Parse jwt from token and return an existing user, if it exists.
    async fn parse_jwt(token: &str) -> ApiResult<UserModel> {
        let decoding_key = &Config::get().decoding_key;
        let decoded =
            jsonwebtoken::decode::<Claim>(token, decoding_key, &VALIDATION).map_err(|e| {
                info!("auth/jwt: couldn't decode jwt: {e:?}");
                ApiError::Unauthorized
            })?;
        let user = UserModel::find_by_id(decoded.claims.sub)
            .await?
            .ok_or_else(|| {
                info!(
                    "auth/jwt: couldn't find user with id {}",
                    decoded.claims.sub
                );
                ApiError::Unauthorized
            })?;
        Ok(user)
    }
}
