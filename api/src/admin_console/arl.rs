// Utility functions to keep the system in optimal state
use futures_util::StreamExt;
use log::{info, warn};
use niceerr_entity::arl::{Model as Arl, UpdatableArl};
use serde::Serialize;

use crate::{arl_service::ArlService, ty::ApiResult};

#[derive(Serialize, Default, Debug)]
pub struct ArlPriorityUpdate {
    updated: u32,
    untouched: u32,
}

/// Reset the ARL priorities
///
/// This will reset the database priority auto-increment thingy
/// and set each ARL's priority to a coherent, sequential number.
pub(crate) async fn reset_arl_priorities() -> ApiResult<ArlPriorityUpdate> {
    let mut arl_stream = Arl::get_all_stream().await?;
    let mut start_prio: u32 = 0;
    let mut response = ArlPriorityUpdate::default();
    while let Some(arl) = arl_stream.next().await.transpose()? {
        if start_prio == arl.priority as u32 {
            start_prio += 1;
            response.untouched += 1;
            continue;
        }
        let update = UpdatableArl {
            priority: Some(start_prio),
            ..Default::default()
        };
        info!("Reset ARL priority: {} -> {start_prio}", arl.priority);
        arl.update(update, None, None).await?;
        start_prio += 1;
        response.updated += 1;
    }
    info!("Reset ARL priorities: {response:?}");

    if response.updated > 0 {
        Arl::reset_priority_index().await?;
        ArlService::get().on_change_trigger_forced_refresh();
    }

    Ok(response)
}

pub async fn reprioritize_by_countries(countries: Vec<String>) -> ApiResult<ArlPriorityUpdate> {
    if countries.is_empty() {
        return Err(crate::error::ApiError::InvalidRequest);
    }

    {
        let existing_countries = Arl::unique_countries().await?;
        if !countries
            .iter()
            .all(|country| existing_countries.contains(country))
        {
            warn!("Invalid countries: {countries:?}");
            return Err(crate::error::ApiError::InvalidRequest);
        }
    }
    let mut response = ArlPriorityUpdate::default();
    let mut arls = Arl::sorted_by_countries(countries).await?;
    let mut new_prio = 0;
    let mut last_prio = Arl::lowest_priority()
        .await?
        .map(|m| m.priority + 1)
        .unwrap_or(0);

    while let Some(arl) = arls.next().await.transpose()? {
        if new_prio == arl.priority {
            new_prio += 1;
            response.untouched += 1;
            continue;
        }

        /*
         * We gotta check if another ARL already has the same priority. There are two cases:
         *
         * - There's another arl with the same priority: we need to swap their priorities, e.g.
         * the current arl being iterated will gain the former's priority, and the former
         * will gain the current arl old's priority. However, the swap doesn't work with ARLs
         * whose priority used to be higher (closer to 0) because we'll end up with the 'other'
         * ARL having a lower priority that has already been assigned in a previous iteration.
         *
         * In that case, we just don't revert back the 'other' ARL's priority, and we instead
         * leave it with a lower priority (farther to 0).
         *
         *
         * - There's no arl with the same priority: this is straightforward and we just assign the
         * current ARL the new priority.
         */
        if let Some(other) = Arl::find_by_priority(new_prio).await? {
            if other.id == arl.id {
                new_prio += 1;
                response.untouched += 1;
                continue;
            }
            let update_old_prio = UpdatableArl::new_priority_change(arl.priority);
            let keep_old_prio = arl.priority > new_prio;
            let other = other
                .update(UpdatableArl::new_priority_change(last_prio), None, None)
                .await?;
            arl.update(UpdatableArl::new_priority_change(new_prio), None, None)
                .await?;
            if keep_old_prio {
                other.update(update_old_prio, None, None).await?;
            } else {
                last_prio += 1;
            }
        } else {
            let update = UpdatableArl::new_priority_change(new_prio);
            arl.update(update, None, None).await?;
        }
        new_prio += 1;
        response.updated += 1;
    }
    if response.updated > 0 {
        Arl::reset_priority_index().await?;
        ArlService::get().on_change_trigger_forced_refresh();
    }

    info!("Reprioritized by countries: {response:?}");

    Ok(response)
}
