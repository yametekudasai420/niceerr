use std::ops::Deref;

use log::error;
use niceerr_entity::song::Model as Song;

use crate::download::ty::OngoingDownload;

/// We use this to self-delete streamed songs that get saved to the OS' temp folder.
pub struct SelfDeleteSong {
    song: Song,
    armed: bool,
}

impl SelfDeleteSong {
    pub fn new(song: Song, ongoing_download: &OngoingDownload) -> SelfDeleteSong {
        let armed = ongoing_download.stream_data.is_some();
        SelfDeleteSong { song, armed }
    }

    pub fn new_unarmed(song: Song) -> SelfDeleteSong {
        SelfDeleteSong { song, armed: false }
    }
}

impl Drop for SelfDeleteSong {
    fn drop(&mut self) {
        if !self.armed || !self.song.is_completed() {
            return;
        }
        let path = self.song.download_path.clone();
        let id = self.song.id;
        tokio::spawn(async move {
            // delete
            match tokio::fs::remove_file(&path).await {
                Ok(_) => {}
                Err(e) => error!("[{id}] self-delete failed at {path:?}: {e:?}"),
            }
        });
    }
}

impl Deref for SelfDeleteSong {
    type Target = Song;

    fn deref(&self) -> &Self::Target {
        &self.song
    }
}
