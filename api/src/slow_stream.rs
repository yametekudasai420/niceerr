use futures_util::Stream;
use log::warn;
use niceerr_config::Config;
use pin_project_lite::pin_project;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use std::time::{Duration, Instant};
use tokio::time::{sleep, Sleep};

use crate::error::ApiError;
use crate::ty::ApiResult;

pin_project! {
    /// A stream wrapper that detects slow consumers. Consuming this stream
    /// in a deliberately slow way will cause axum to call poll() less often,
    /// and that will, in turn, not reset this Stream's timeout.
    pub struct SlowConsumerStreamWrapper<S> {
        #[pin]
        inner: S,
        #[pin]
        timeout: Option<Sleep>,
    }
}

impl<S> SlowConsumerStreamWrapper<S> {
    pub fn new(inner: S) -> Self {
        Self {
            inner,
            timeout: None,
        }
    }
}

impl<S> Stream for SlowConsumerStreamWrapper<S>
where
    S: Stream<Item = ApiResult<Vec<u8>>>,
{
    type Item = ApiResult<Vec<u8>>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mut this = self.project();

        if let Some(timeout) = this.timeout.as_mut().as_pin_mut() {
            if timeout.poll(cx).is_ready() {
                warn!(
                    "Killing stream due to slow consumer after {}s",
                    Config::get().streaming_remote_timeout_sec
                );
                return Poll::Ready(Some(Err(ApiError::InvalidRequest)));
            }
        }

        match this.inner.as_mut().poll_next(cx) {
            Poll::Ready(Some(item)) => {
                match this.timeout.as_mut().as_pin_mut() {
                    Some(t) => {
                        let deadline = Instant::now()
                            + Duration::from_secs(
                                Config::get().streaming_remote_timeout_sec as u64,
                            );
                        t.reset(deadline.into());
                    }
                    // We can't time out right away, because there's a chance the server
                    // is slow (as opposed to the client). We first need to wait for the
                    // server to start sending data to initialize the timeout.
                    None => this.timeout.as_mut().set(Some(sleep(Duration::from_secs(
                        Config::get().streaming_remote_timeout_sec as u64,
                    )))),
                }

                Poll::Ready(Some(item))
            }
            Poll::Ready(None) => Poll::Ready(None),
            Poll::Pending => {
                if let Some(timeout) = this.timeout.as_pin_mut() {
                    let _ = timeout.poll(cx);
                }
                Poll::Pending
            }
        }
    }
}
