use std::{
    hash::Hash,
    ops::Deref,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, LazyLock, RwLock, RwLockReadGuard,
    },
    time::Duration,
};

use chrono::DateTime;
use dashmap::DashMap;
use futures_util::{StreamExt, TryFutureExt};
use log::{error, info, warn};
use niceerr_config::Config;
use niceerr_dz::{
    error::{DzError, DzResult},
    types::Song,
    DzClient,
};
use niceerr_entity::arl::Model as Arl;
use reqwest::{Client, ClientBuilder};
use tokio::time::interval;
use tokio_util::sync::CancellationToken;
use uuid::Uuid;

use crate::{
    backoff::EBCancellableRunner,
    error::{ApiError, TokenErrorReason},
    scheduled_tracker::ScheduledTaskTracker,
    ty::ApiResult,
};

type SharedRw<T> = Arc<RwLock<T>>;

#[derive(Debug, Clone)]
struct CountryArlKey {
    country: String,
    arl_id: Uuid,
}

#[derive(Debug)]
pub struct ArlService {
    primary: SharedRw<Option<CountryArlKey>>,
    arls: DashMap<CountryArlKey, DzClient>,
    refresh_rx: flume::Receiver<bool>,
    refresh_tx: flume::Sender<bool>,
    refresh_task_running: AtomicBool,
}

static NEW_ARL_SERVICE: LazyLock<ArlService> = LazyLock::new(ArlService::init);

struct ArlRefreshGuard;

impl ArlRefreshGuard {
    fn new() -> ArlRefreshGuard {
        NEW_ARL_SERVICE
            .refresh_task_running
            .store(true, Ordering::Relaxed);
        ArlRefreshGuard
    }
}

impl Drop for ArlRefreshGuard {
    fn drop(&mut self) {
        NEW_ARL_SERVICE
            .refresh_task_running
            .store(false, Ordering::Relaxed);
    }
}

impl ArlService {
    fn init() -> Self {
        let (tx, rx) = flume::bounded(0);
        ArlService {
            primary: Arc::new(RwLock::new(None)),
            arls: DashMap::new(),
            refresh_tx: tx,
            refresh_rx: rx,
            refresh_task_running: AtomicBool::new(false),
        }
    }

    pub fn get() -> &'static ArlService {
        &NEW_ARL_SERVICE
    }

    /// Proxy builder for the dzlib. This will disable ARL verification so that useless/expired ARLs work with Niceerr
    /// (we'll be using a relay anyway, so having an expired arl is no big deal)
    pub async fn new_arl<S: AsRef<str>>(arl: S) -> DzResult<DzClient> {
        let builder = Self::builder();
        match Config::get().relay_url.is_some() {
            true => DzClient::new_non_lossless(arl.as_ref(), builder).await,
            false => DzClient::new(arl.as_ref(), builder).await,
        }
    }

    pub(crate) fn builder() -> ClientBuilder {
        let mut tcp_keepalive = None;
        if Config::get().tcp_keepalive_sec > 0 {
            tcp_keepalive = Some(Duration::from_secs(Config::get().tcp_keepalive_sec.into()));
        }
        let mut headers = reqwest::header::HeaderMap::new();
        headers.append(
            reqwest::header::ACCEPT_LANGUAGE,
            reqwest::header::HeaderValue::from_static(Config::get().http_accept_language.to_str()),
        );

        Client::builder()
            .user_agent(Config::get().user_agent.clone())
            .connect_timeout(Duration::from_secs(
                Config::get().connect_timeout_sec.into(),
            ))
            .tcp_keepalive(tcp_keepalive)
            .default_headers(headers)
            .read_timeout(Duration::from_secs(Config::get().read_timeout_sec.into()))
            .use_rustls_tls()
            .hickory_dns(true)
    }

    async fn find_any_client() -> ApiResult<(DzClient, Arl)> {
        let mut arl_stream = Arl::get_unexpired_tokens_stream().await?;

        while let Some(arl) = arl_stream.next().await.transpose()? {
            let id = arl.id;
            let client_and_updated_arl = arl.into_refreshed_client().await;
            match client_and_updated_arl {
                Ok((client, arl)) => {
                    return Ok((client, arl));
                }
                Err(e) => {
                    warn!("[{id}'] Can't create client from arl: {e:?}");
                }
            }
        }
        Err(ApiError::TokenError(TokenErrorReason::NoValidArlAvailable))
    }

    async fn find_country_client(song: &Song) -> ApiResult<(DzClient, Arl)> {
        let countries = song.available_countries_owned();
        let mut arl_stream =
            Arl::get_country_unexpired_tokens_stream(countries.into_iter()).await?;
        while let Some(arl) = arl_stream.next().await.transpose()? {
            match arl.build_client().await {
                Ok(client) => {
                    return Ok((client, arl));
                }
                Err(e) => warn!("Can't create client from arl {}: {e:?}", arl.id),
            }
        }
        Err(ApiError::TokenError(TokenErrorReason::NoValidArlAvailable))
    }

    fn read_primary(&self) -> RwLockReadGuard<'_, Option<CountryArlKey>> {
        self.primary.read().expect("Can't unlock primary")
    }

    fn write_primary(&self, key: Option<CountryArlKey>) {
        let mut write = self.primary.write().expect("Can't unlock primary");
        *write = key
    }

    fn get_primary_client_inner(&self) -> Option<DzClient> {
        match self.read_primary().as_ref() {
            Some(key) => self.arls.get(key).map(|c| c.clone()),
            None => None,
        }
    }

    fn get_country_client_inner(&self, song: &Song) -> Option<DzClient> {
        self.arls
            .iter()
            .find(|clt| clt.can_stream(song).is_ok())
            .map(|c| c.clone())
    }

    /// Delete a client from the hot cache. If the primary client was removed,
    /// return true, false otherwise.
    fn delete_client(&self, key: &CountryArlKey) -> bool {
        self.arls.remove(key);
        let is_primary_removed = self.read_primary().as_ref().is_some_and(|p| p == key);
        if is_primary_removed {
            self.write_primary(None);
        }
        is_primary_removed
    }

    fn delete_primary(&self) -> bool {
        if let Some(primary) = self.read_primary().as_ref() {
            self.delete_client(primary)
        } else {
            false
        }
    }

    fn set_country_client(&self, arl: Arl, client: DzClient) {
        let key = CountryArlKey {
            country: arl.country,
            arl_id: arl.id,
        };
        self.arls.insert(key, client);
    }

    fn set_primary_client(&self, client: DzClient, arl: Arl) {
        let key = CountryArlKey {
            country: arl.country,
            arl_id: arl.id,
        };
        self.arls.insert(key.clone(), client);
        self.write_primary(Some(key));
    }

    /// Get or set the "primary" client. This one is always used above any other, as long as it
    /// has the rights to stream any given track. This function can also be forced to always
    /// get a brand new primary ARL.
    async fn get_or_set_primary_client(&self, force_fresh: bool) -> ApiResult<DzClient> {
        if !force_fresh {
            if let Some(client) = self.get_primary_client_inner() {
                return Ok(client);
            } else {
                self.delete_primary();
            }
        }
        info!("COLD: searching for a primary client");
        let (client, arl) = Self::find_any_client().await?;
        self.set_primary_client(client.clone(), arl);
        Ok(client)
    }

    async fn get_or_set_country_client(&self, song: &Song) -> ApiResult<DzClient> {
        if let Some(client) = self.get_country_client_inner(song) {
            return Ok(client);
        }
        info!("COLD: searching for a client to stream id={}", song.id);
        let (client, arl) = Self::find_country_client(song).await?;
        self.set_country_client(arl, client.clone());
        Ok(client)
    }

    /// Needed to prevent deadlocks with dashmap
    fn arl_keys(&self) -> Vec<CountryArlKey> {
        self.arls.iter().map(|c| c.key().clone()).collect()
    }

    fn is_refresh_task_running(&self) -> bool {
        self.refresh_task_running.load(Ordering::Relaxed)
    }

    fn trigger_spawn_refresh_task(&self) {
        if self.is_refresh_task_running() {
            self.trigger_refresh_task(false);
        } else {
            tokio::spawn(Self::refresh_task());
        }
    }

    fn is_arl_in_use(&self, uuid: Uuid) -> bool {
        self.arls.iter().any(|c| c.key().arl_id == uuid)
    }

    pub fn on_change_trigger_refresh(&self, uuid: Uuid) {
        if self.is_refresh_task_running() && self.is_arl_in_use(uuid) {
            self.trigger_refresh_task(true);
        } else {
            info!("ARL cache is empty or ARL isn't in the cache; not forcing a refresh.");
        }
    }

    pub fn on_change_trigger_forced_refresh(&self) {
        if self.is_refresh_task_running() {
            self.trigger_refresh_task(true);
        } else {
            info!("ARL cache is empty; not forcing a refresh.");
        }
    }

    fn trigger_refresh_task(&self, force_immediately: bool) {
        self.refresh_tx.try_send(force_immediately).ok();
    }

    fn purge_cache(&self) {
        self.arls.clear();
        self.arls.shrink_to_fit();
        self.write_primary(None);
    }

    async fn refresh_task() {
        let _guard = ArlRefreshGuard::new();
        let mut max_client_age = tokio::time::interval(Duration::from_secs(
            Config::get().arl_refresh_max_age_sec as u64,
        ));
        let exit_timeout_dur =
            Duration::from_secs(Config::get().arl_refresher_inactivity_timeout_sec as u64);
        let mut exit_timeout = interval(exit_timeout_dur);
        let service = Self::get();

        exit_timeout.tick().await;
        max_client_age.tick().await;
        info!("ARL refresh task started.");

        loop {
            tokio::select! {
                // Something acquired a client: we need to keep the refresh service active.
                Ok(force_refresh) = service.refresh_rx.recv_async() => {
                    exit_timeout.reset_after(exit_timeout_dur);
                    // Huh, someone triggered a hard refresh
                    if force_refresh{
                        if let Err(e) = service.try_refresh_cache().await {
                            error!("ARL (forced) refresh error: {e:?}");
                        }
                    }
                },
                // Cache is old: trigger refresh
                _ = max_client_age.tick() => {
                    if let Err(e) = service.try_refresh_cache().await {
                        error!("ARL refresh error: {e:?}");
                    }
                },
                _ = exit_timeout.tick() => {
                    service.purge_cache();
                    info!("ARL refresh task hasn't received update pings for a while. Exiting.");
                    return;
                }
            }
        }
    }

    async fn try_refresh_cache(&self) -> ApiResult<()> {
        let _tracker_guard = ScheduledTaskTracker::try_guard(
            crate::scheduled_tracker::ScheduledTaskName::PrimaryArlRefresh,
        )?;
        self.refresh_cache().await;
        Ok(())
    }

    async fn refresh_cache(&self) {
        // Primary client: it needs special care
        for key in self.arl_keys() {
            let arl = match Arl::find_by_id(&key.arl_id).await {
                Ok(arl) => {
                    info!("Found ARL: {arl:?}");
                    arl
                }
                Err(e) => {
                    warn!("Can't find arl with id {key:?}: {e:?}");
                    self.delete_client(&key);
                    continue;
                }
            };
            match arl.into_refreshed_client().await {
                Ok((client, arl)) => {
                    self.set_country_client(arl, client);
                }
                Err(e) => {
                    warn!("Can't build client from arl with id {key:?}: {e:?}");
                    self.delete_client(&key);
                }
            };
        }

        info!("Finished updating ARLs; now refreshing primary");
        match self.get_or_set_primary_client(true).await {
            Ok(_) => {}
            Err(e) => {
                warn!("Can't refresh primary client: {e:?}");
            }
        };
        self.arls.shrink_to_fit();
        info!("Updated {} client(s) in the hot cache", self.arls.len());
    }

    pub fn get_cache_size(&self) -> usize {
        self.arls.len()
    }

    #[inline]
    pub async fn get_primary_client(&self) -> ApiResult<DzClient> {
        self.trigger_spawn_refresh_task();
        self.get_or_set_primary_client(false).await
    }

    /// Get a client that is capable of streaming this song, or find one that can by traversing
    /// the different available stores (primary arl, arl fallback cache, and database)
    pub async fn get_country_client(&self, song: &Song) -> ApiResult<CachedClient<DzClient>> {
        let primary = self.get_primary_client().await?;
        if primary.can_stream(song).is_ok() || song.is_unavailable() {
            return Ok(CachedClient::Primary(primary));
        }
        if let Ok(client) = self.get_or_set_country_client(song).await {
            return Ok(CachedClient::Fallback(client));
        }
        warn!(
            "Couldn't find a valid client capable of streaming song with id {}",
            song.id
        );
        Ok(CachedClient::Primary(primary))
    }

    /// Find and return the song and the client required to download it.
    ///
    /// This will make sure a matching client is returned to the caller
    /// (because we can't use a different client than the one used to
    /// fetch the song)
    pub async fn fetch_song_with_client_by_id(&self, id: u64) -> ApiResult<(Song, DzClient)> {
        let client = self.get_primary_client().await?;
        let song = client.song(id).await?;
        let country_client = self.get_country_client(&song).await?;
        match country_client {
            CachedClient::Primary(client) => Ok((song, client)),
            CachedClient::Fallback(client) => {
                let song = client.song(id).await?;
                Ok((song, client))
            }
        }
    }

    pub async fn fetch_song_with_client(&self, song: Song) -> ApiResult<(Song, DzClient)> {
        let country_client = self.get_country_client(&song).await?;
        match country_client {
            CachedClient::Primary(client) => Ok((song, client)),
            CachedClient::Fallback(client) => {
                let song = client.song(song.id).await?;
                Ok((song, client))
            }
        }
    }

    pub fn init_tasks() {
        tokio::spawn(Self::prune_expired_arls());
    }

    async fn prune_expired_arls() {
        let mut sleeper = interval(Duration::from_secs(
            Config::get().expired_token_prune_sec.into(),
        ));
        loop {
            sleeper.tick().await;
            Self::prune_expired_arls_inner()
                .await
                .inspect_err(|e| error!("Expired ARL pruner: {e:?}"))
                .ok();
        }
    }

    pub async fn prune_expired_arls_inner() -> ApiResult<()> {
        let _tracker_guard = ScheduledTaskTracker::try_guard(
            crate::scheduled_tracker::ScheduledTaskName::PruneExpiredArl,
        )?;
        let mut arl_stream = Arl::get_unexpired_tokens_stream().await?;
        let (mut total, mut valid, mut pruned) = (0, 0, 0);
        while let Some(arl) = arl_stream.next().await.transpose()? {
            total += 1;
            match arl.build_client().await {
                Ok(client) => {
                    arl.update_info_with_client(&client).await?;
                    valid += 1;
                }
                Err(DzError::SubscriptionRequired) => {
                    info!("Pruning expired arl: {} ('{}')", arl.id, arl.alias);
                    arl.delete_p().await?;
                    pruned += 1;
                }
                Err(e) => {
                    warn!("Can't create client from arl: {e:?}");
                }
            }
        }
        info!(
            "Expired ARL pruner: {} total, {} still valid, {} pruned",
            total, valid, pruned
        );
        Ok(())
    }
}

impl Hash for CountryArlKey {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.country.hash(state);
    }
}

impl PartialEq for CountryArlKey {
    fn eq(&self, other: &Self) -> bool {
        self.country == other.country
    }
}

impl Eq for CountryArlKey {}

pub enum CachedClient<T> {
    Primary(T),
    Fallback(T),
}

impl Deref for CachedClient<DzClient> {
    type Target = DzClient;

    fn deref(&self) -> &Self::Target {
        match self {
            CachedClient::Primary(c) | CachedClient::Fallback(c) => c,
        }
    }
}

pub trait IntoRefreshedClient {
    async fn update_info_with_client(self, client: &DzClient) -> ApiResult<Arl>;

    async fn into_refreshed_client(self) -> ApiResult<(DzClient, Arl)>;

    async fn build_client(&self) -> DzResult<DzClient>;
}

impl IntoRefreshedClient for Arl {
    async fn update_info_with_client(self, client: &DzClient) -> ApiResult<Arl> {
        let expires_at = DateTime::from_timestamp(client.expiration(), 0).ok_or_else(|| {
            error!(
                "Couldn't convert expiration to datetime: {}",
                client.expiration()
            );
            ApiError::InvalidRequest
        })?;
        let updated = self.update_info(&expires_at, client.country()).await?;
        Ok(updated)
    }

    async fn into_refreshed_client(self) -> ApiResult<(DzClient, Arl)> {
        let _cancel_token = CancellationToken::new();
        let factory = || self.build_client().map_err(Into::into);
        let mut runner = EBCancellableRunner::new(factory, &_cancel_token);
        let client = runner.call().await?;
        let arl = self.update_info_with_client(&client).await?;

        Ok((client, arl))
    }

    async fn build_client(&self) -> DzResult<DzClient> {
        let client = ArlService::new_arl(&self.token).await?;
        Ok(client)
    }
}
