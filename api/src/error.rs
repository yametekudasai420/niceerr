use std::backtrace;

use async_zip::error::ZipError;
use axum::{extract::rejection::JsonRejection, http::StatusCode, response::IntoResponse, Json};
use log::{error, info, warn};
use niceerr_dz::error::DzError;
use niceerr_entity::error::EntityError;
use serde::{Deserialize, Serialize};
use serde_json::json;
use thiserror::Error;
use tokio::task::JoinError;

#[derive(Debug, Serialize, Deserialize)]
pub struct ApiErrorResponse {
    pub error: String,
    pub status: u16,
}

#[derive(Debug, PartialEq)]
pub enum TokenErrorReason {
    InvalidRelayCredentials,
    NoValidArlAvailable,
}

impl std::fmt::Display for TokenErrorReason {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidRelayCredentials => f.write_str("invalid relay credentials"),
            Self::NoValidArlAvailable => f.write_str("no valid arl client could be built"),
        }
    }
}

#[derive(Error, Debug, PartialEq)]
pub enum ApiError {
    #[error("Invalid request")]
    InvalidRequest,
    #[error("Internal server error")]
    InternalError,
    #[error("Server can't stream contents because of a token error: {0}")]
    TokenError(TokenErrorReason),
    #[error("A similar entry already exists")]
    UniqueViolation,
    #[error("Insufficient permissions")]
    InsufficientPermissions,
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Not found")]
    NotFound,
    #[error("{0} (you may choose to ignore errors)")]
    BypassableError(String),
    #[error("Deezer client error: {1}")]
    DeezerClientError(StatusCode, String),
    #[error("Max resource limit exceeded")]
    MaxResourceLimitExceeded,
    #[error("Feature is disabled")]
    DisabledFeature,
    #[error("Resource is currently locked by another task")]
    LockedResource,
}

#[derive(Debug, thiserror::Error)]
pub enum WrappedDownloadError {
    #[error("I/O error: {0}")]
    Io(#[from] std::io::Error),
    #[error("Relay error: {0}")]
    RetriableRelay(String),
    #[error("Unretriable relay error: {0}")]
    UnretriableRelay(String),
    #[error(transparent)]
    Dz(#[from] DzError),
}

impl IntoResponse for ApiError {
    fn into_response(self) -> axum::response::Response {
        let (status, error_message) = match self {
            Self::BypassableError(_) => (StatusCode::BAD_REQUEST, self.to_string()),
            Self::InvalidRequest => (StatusCode::BAD_REQUEST, self.to_string()),
            Self::InternalError => (StatusCode::INTERNAL_SERVER_ERROR, self.to_string()),
            Self::UniqueViolation => (StatusCode::CONFLICT, self.to_string()),
            Self::InsufficientPermissions => (StatusCode::FORBIDDEN, self.to_string()),
            Self::Unauthorized => (StatusCode::UNAUTHORIZED, self.to_string()),
            Self::NotFound => (StatusCode::NOT_FOUND, self.to_string()),
            Self::DeezerClientError(status_code, message) => (status_code, message.to_string()),
            Self::MaxResourceLimitExceeded => (StatusCode::TOO_MANY_REQUESTS, self.to_string()),
            Self::DisabledFeature => (StatusCode::FORBIDDEN, self.to_string()),
            Self::LockedResource => (StatusCode::LOCKED, self.to_string()),
            Self::TokenError(ref _msg) => (StatusCode::SERVICE_UNAVAILABLE, self.to_string()),
        };
        let json = json!({ "error": error_message, "status": status.as_u16() });
        (status, Json(json)).into_response()
    }
}

impl From<EntityError> for ApiError {
    fn from(value: EntityError) -> Self {
        match value {
            EntityError::InsufficientPermissions => Self::InsufficientPermissions,
            EntityError::InvalidData => Self::InvalidRequest,
            EntityError::NotFound => Self::NotFound,
            EntityError::PasswordMismatch => Self::Unauthorized,
            EntityError::UniqueViolation => Self::UniqueViolation,
            EntityError::Unhandled(_) => Self::InternalError,
        }
    }
}

impl From<DzError> for ApiError {
    fn from(value: DzError) -> Self {
        match value {
            DzError::ReqwestError(err) => {
                ApiError::DeezerClientError(StatusCode::SERVICE_UNAVAILABLE, err.to_string())
            }
            DzError::UpstreamError(msg) => {
                warn!("Unhandled upstream error: {msg}");
                ApiError::DeezerClientError(StatusCode::SERVICE_UNAVAILABLE, msg)
            }
            DzError::SubscriptionRequired => {
                ApiError::DeezerClientError(StatusCode::BAD_REQUEST, "Subscription required".into())
            }
            DzError::DecryptionError => ApiError::InternalError,
            DzError::NotFound => ApiError::NotFound,
            DzError::Unavailable(e) => {
                ApiError::DeezerClientError(StatusCode::BAD_REQUEST, format!("Unavailable: {e}"))
            }
            DzError::TaggingError(msg) => {
                ApiError::DeezerClientError(StatusCode::INTERNAL_SERVER_ERROR, msg)
            }
        }
    }
}

impl From<JoinError> for ApiError {
    fn from(value: JoinError) -> Self {
        let trace = backtrace::Backtrace::force_capture();
        error!("Join error: {}, backtrace: {trace:#?}", value);
        Self::InternalError
    }
}

impl From<ZipError> for ApiError {
    fn from(_value: ZipError) -> Self {
        Self::InternalError
    }
}

impl From<std::io::Error> for ApiError {
    fn from(value: std::io::Error) -> Self {
        error!("IO error: {}", value);
        Self::InternalError
    }
}

impl<T> From<flume::SendError<T>> for ApiError {
    fn from(value: flume::SendError<T>) -> Self {
        error!("channel send error: {}", value);
        Self::InternalError
    }
}

impl From<JsonRejection> for ApiError {
    fn from(value: JsonRejection) -> Self {
        info!("JSON rejection: {}", value.body_text());
        Self::InvalidRequest
    }
}
