use std::{collections::VecDeque, path::Path};

use camino::Utf8PathBuf;
use log::{error, info, warn};
use niceerr_config::Config;

use crate::ty::ApiResult;

pub struct TmpFolder {}

impl TmpFolder {
    pub async fn run_partial_cleanup() {
        let config = Utf8PathBuf::from(&Config::get().temp_folder).join("niceerr");
        match Self::delete_empty(config.as_str()).await{
            Ok(true) => info!("tmp folder: successfully cleaned up (partial)"),
            Ok(false) => warn!("tmp folder: not cleaning up existing files (maybe there's an active stream download?)"),
            Err(e) => error!("tmp folder: couldn't clean up: {e:?}")
        }
    }

    pub async fn run_full_cleanup() {
        let tmp_folder = Utf8PathBuf::from(&Config::get().temp_folder).join("niceerr");
        if let Err(e) = tokio::fs::remove_dir_all(tmp_folder).await {
            if e.kind() != std::io::ErrorKind::NotFound {
                error!("tmp folder: couldn't delete: {e:?}");
            }
        }
        info!("tmp folder: successfully cleaned up (full)");
    }

    /// Calculate folder size using BFS
    async fn get_folder_size<S: AsRef<Path>>(path: S) -> ApiResult<u64> {
        let path = path.as_ref().to_owned();
        let mut queue = VecDeque::new();

        let mut size = 0;
        queue.push_back(path);

        while let Some(path) = queue.pop_front() {
            let mut entries = match tokio::fs::read_dir(path).await {
                Ok(e) => e,
                Err(e) => {
                    if e.kind() != std::io::ErrorKind::NotFound {
                        warn!("Couldn't read dir entry: {e:?}");
                        return Err(e.into());
                    } else {
                        continue;
                    }
                }
            };

            while let Some(entry) = entries.next_entry().await? {
                let metadata = tokio::fs::metadata(entry.path()).await?;
                match (metadata.is_file(), metadata.is_dir()) {
                    (true, false) => size += metadata.len(),
                    (false, true) => {
                        queue.push_back(entry.path());
                    }
                    _ => {}
                };
            }
        }

        Ok(size)
    }

    pub async fn get_size() -> ApiResult<u64> {
        let tmp_folder = Utf8PathBuf::from(&Config::get().temp_folder).join("niceerr");
        Self::get_folder_size(tmp_folder.as_str())
            .await
            .inspect_err(|e| warn!("tmp folder: couldn't get size: {e:?}"))
    }

    /// Recursively traverse niceerr's tmp folder using BFS.
    ///
    /// Note that we intentionally don't delete existing files because there could be
    /// a potential race condition with the periodic cleaner and active streaming downloads.
    async fn delete_empty<S: AsRef<Path>>(path: S) -> ApiResult<bool> {
        let root_path = path.as_ref().to_owned();

        if let Err(e) = tokio::fs::metadata(&root_path).await {
            if e.kind() == std::io::ErrorKind::NotFound {
                return Ok(true);
            }
            return Err(e.into());
        }

        let mut queue = VecDeque::new();
        let mut second_pass = VecDeque::new();

        queue.push_back(root_path.clone());

        while let Some(path) = queue.pop_front() {
            let mut entries = tokio::fs::read_dir(&path).await?;

            while let Some(entry) = entries.next_entry().await? {
                let metadata = entry.metadata().await?;
                if metadata.is_dir() {
                    queue.push_back(entry.path());
                    second_pass.push_back(entry.path());
                }
            }
        }

        second_pass.push_front(root_path);
        let mut is_empty = true;

        #[allow(clippy::never_loop)]
        'outer: while let Some(path) = second_pass.pop_back() {
            let mut entries = tokio::fs::read_dir(&path).await?;

            // if the deepest directory contains something, then it must be a file
            // so we can just keep going on to the next directory.
            while let Some(_entry) = entries.next_entry().await? {
                is_empty = false;
                continue 'outer;
            }
            tokio::fs::remove_dir(path).await?;
        }

        Ok(is_empty)
    }
}
