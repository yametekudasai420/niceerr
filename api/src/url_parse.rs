use std::sync::LazyLock;

use log::info;
use regex::Regex;
use serde::Serialize;
use url::Url;

use crate::{error::ApiError, ty::ApiResult};

static ARTIST_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"/artist/(\d+)").expect("cannot initialize artist regex"));
static ALBUM_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"/album/(\d+)").expect("cannot initialize album regex"));
static SONG_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"/track/(\d+)").expect("cannot initialize song regex"));
static PLAYLIST_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"/playlist/(\d+)").expect("cannot initialize playlist regex"));

#[derive(Serialize, Debug)]
pub enum DzLinkKind {
    Artist(u64),
    Album(u64),
    Song(u64),
    Playlist(u64),
}

#[derive(Serialize, Debug)]
pub struct DzLink {
    pub kind: DzLinkKind,
}

pub fn init_url_parser_regex() {
    let _a = &ARTIST_REGEX.captures("test");
    let _b = &ALBUM_REGEX.captures("test");
    let _c = &SONG_REGEX.captures("test");
    let _d = &PLAYLIST_REGEX.captures("test");
}

impl DzLink {
    fn validate_url(link: &str) -> ApiResult<Url> {
        let url = Url::parse(link).map_err(|_| ApiError::InvalidRequest)?;
        let Some(domain) = url.domain() else {
            return Err(ApiError::InvalidRequest);
        };
        if !matches!(domain, "www.deezer.com" | "deezer.page.link") {
            info!("invalid download domain: {domain:?}");
            return Err(ApiError::InvalidRequest);
        }
        Ok(url)
    }

    fn try_extract_artist(path: &str) -> Option<u64> {
        ARTIST_REGEX
            .captures(path)
            .and_then(|c| c.get(1).map(|m| m.as_str()))
            .and_then(|s| s.parse::<u64>().ok())
    }

    fn try_extract_album(path: &str) -> Option<u64> {
        ALBUM_REGEX
            .captures(path)
            .and_then(|c| c.get(1).map(|m| m.as_str()))
            .and_then(|s| s.parse::<u64>().ok())
    }

    fn try_extract_track(path: &str) -> Option<u64> {
        SONG_REGEX
            .captures(path)
            .and_then(|c| c.get(1).map(|m| m.as_str()))
            .and_then(|s| s.parse::<u64>().ok())
    }

    fn try_extract_playlist(path: &str) -> Option<u64> {
        PLAYLIST_REGEX
            .captures(path)
            .and_then(|c| c.get(1).map(|m| m.as_str()))
            .and_then(|s| s.parse::<u64>().ok())
    }

    pub async fn from_url(link: &str) -> ApiResult<Self> {
        let mut url = Self::validate_url(link)?;
        // Parse deezer.page.link URLs
        if url.domain() == Some("deezer.page.link") {
            // We intentionally don't error out if this redirects us to a 404
            // page because some songs don't have a "song" page.
            let response = reqwest::get(link).await.map_err(|e| {
                info!("page.link fetch error: {e:?}");
                ApiError::InvalidRequest
            })?;
            response.url().clone_into(&mut url);
        }

        let path = url.path();
        if path.contains("artist") {
            let id = Self::try_extract_artist(path).ok_or(ApiError::InvalidRequest)?;
            Ok(Self {
                kind: DzLinkKind::Artist(id),
            })
        } else if path.contains("album") {
            let id = Self::try_extract_album(path).ok_or(ApiError::InvalidRequest)?;
            Ok(Self {
                kind: DzLinkKind::Album(id),
            })
        } else if path.contains("track") {
            let id = Self::try_extract_track(path).ok_or(ApiError::InvalidRequest)?;
            Ok(Self {
                kind: DzLinkKind::Song(id),
            })
        } else if path.contains("playlist") {
            let id = Self::try_extract_playlist(path).ok_or(ApiError::InvalidRequest)?;
            Ok(Self {
                kind: DzLinkKind::Playlist(id),
            })
        } else {
            info!("Unsupported link: {url:?}");
            Err(ApiError::InvalidRequest)
        }
    }
}
