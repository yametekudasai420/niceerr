use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

use camino::Utf8PathBuf;
use dashmap::DashMap;
use futures_util::TryFutureExt;
use log::{error, info, warn};
use niceerr_config::Config;
use niceerr_dz::{
    pagination::Paginator,
    playlist::Playlist,
    types::{Album, AlbumWCover, DiscographyIds, ExtraTagInfo, IncompleteSong, StreamFormat},
    DzClient,
};
use sanitize_filename::sanitize;
use tokio_util::sync::CancellationToken;

use crate::{
    arl_service::ArlService,
    backoff::{EBCancellableRunner, ExecutionResult, ExponentialBackoffCancellableMut},
    download::ty::OngoingDownload,
    error::ApiError,
    ty::ApiResult,
};

use super::song::NameSchema;

// Pseudo-album container used to speed up processing when downloading albums.
// Clients may request individual albums or songs. In these cases, we first determine
// whether the album exists, and if it does, we don't need to fetch it again.
//
// That isn't the case with discographies: artist objects only contain a list of album
// ids, and it's a bad practice to fetch all the albums at once. Instead, we fetch
// them one by one.
#[derive(Debug)]
pub enum MediaCollection {
    Unfetched(DiscographyIds),
    PrefetchedAlbum(Box<Album>),
    PrefetchedPlaylist(Paginator<Playlist>),
}

// Song filepath container. We use this to potentially avoid writing songs
// with the same filename to storage.
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct UniqueSongFilepath {
    parent: Utf8PathBuf,
    name: String,
}

/// Just a regular album but with slightly more cached information. This helps us
/// speed up the whole thing because we can cache and store some good stuff.
pub struct WrappedAlbum {
    pub(crate) album: AlbumWCover,
    pub(crate) disk_count: u64,
    pub(crate) extra_tags: ExtraTagInfo,
    pub(crate) suffix: Option<u32>,
    unique_song_paths: DashMap<UniqueSongFilepath, usize>,
}

impl WrappedAlbum {
    fn new(album: AlbumWCover) -> Self {
        Self {
            extra_tags: ExtraTagInfo {
                is_compilation: album.is_compilation(),
                ..Default::default()
            },
            disk_count: album.disk_count(),
            album,
            unique_song_paths: DashMap::new(),
            suffix: None,
        }
    }

    pub fn inner_album(&self) -> Album {
        self.album.album.clone()
    }

    /// Generate a unique filename for this song.
    ///
    /// This is more of a failsafe mechanism to prevent weird crap from happening:
    /// if the configured song name schema isn't good enough and leads to repeated
    /// song names, we'll append an increasing number to the 'repeated' songs.
    fn unique_song_fname(&self, path: Utf8PathBuf, name: String) -> Utf8PathBuf {
        let key = UniqueSongFilepath {
            parent: path,
            name: name.clone(),
        };

        let repeat_count = self
            .unique_song_paths
            .entry(key.clone())
            .and_modify(|count| *count += 1)
            .or_insert(0);

        if *repeat_count > 0 {
            warn!(
                "Found repeated song name: '{}' (alb id={}), appending suffix (_{})",
                name, self.album.id, *repeat_count
            );
            key.parent.join(format!("{}_{}", name, *repeat_count))
        } else {
            key.parent.join(name)
        }
    }

    fn inner_path_suffixed(&self) -> Utf8PathBuf {
        match self.suffix {
            Some(s) => Utf8PathBuf::from(format!("{}_{s}", self.album.name())),
            None => Utf8PathBuf::from(&self.album.name()),
        }
    }

    pub fn set_prefix(&mut self, suffix: u32) {
        self.suffix = Some(suffix);
    }

    fn album_path(&self) -> Utf8PathBuf {
        Utf8PathBuf::from(self.artist_name()).join(self.inner_path_suffixed())
    }

    fn album_path_for_song(&self, song: &IncompleteSong) -> Utf8PathBuf {
        Utf8PathBuf::from(song.artist_name()).join(self.inner_path_suffixed())
    }

    /// Build the path for this song.
    pub fn song_path(&self, song: &IncompleteSong, stream_format: StreamFormat) -> Utf8PathBuf {
        let mut path = self.album_path_for_song(song);
        if self.disk_count > 1 && Config::get().create_folder_for_each_disk {
            path = path.join(song.disk_number.to_string());
        }
        self.unique_song_fname(
            path,
            format!("{}.{}", song.name(), stream_format.extension()),
        )
    }
}

impl Deref for WrappedAlbum {
    type Target = AlbumWCover;
    fn deref(&self) -> &Self::Target {
        &self.album
    }
}

impl DerefMut for WrappedAlbum {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.album
    }
}

impl MediaCollection {
    async fn get_album_backoff(
        client: &DzClient,
        id: u64,
        cancel_token: &CancellationToken,
    ) -> ApiResult<AlbumWCover> {
        let mut backoff = EBCancellableRunner::new(
            || client.album_w_cover(id).map_err(ApiError::from),
            cancel_token,
        );
        backoff
            .call_with_err_cbk(|err| err == &ApiError::NotFound)
            .await
    }

    /// Album renamer task. This function takes care of potentially duplicate song
    /// and album name, and once the album has been properly fixed, it's then
    /// sent to the download queue.
    async fn rename_fix_task(
        rx: flume::Receiver<AlbumWCover>,
        tx: flume::Sender<WrappedAlbum>,
    ) -> ApiResult<()> {
        let mut seen_albums_names = HashMap::new();

        while let Ok(album) = rx.recv_async().await {
            let mut album = WrappedAlbum::new(album);
            let seen_count = seen_albums_names
                .entry((album.artist_id, album.album_path()))
                .and_modify(|v| *v += 1)
                .or_insert(0);
            if *seen_count > 0 {
                album.set_prefix(*seen_count);
                warn!(
                    "Conflict: album (id: {} '{}') will use a different path",
                    album.id, album.name
                );
            }

            tx.send_async(album)
                .await
                .map_err(|_| ApiError::InternalError)?;
        }
        Ok(())
    }

    /// Build playlist path.
    pub fn build_playlist_interpolated_path(&self) -> Option<String> {
        let MediaCollection::PrefetchedPlaylist(playlist) = self else {
            return None;
        };

        let schema = Config::get().playlist_name_schema.trim();
        let interpolated = schema
            .replace("$ID", &playlist.id().to_string())
            .replace("$CHECKSUM", &playlist.checksum)
            .replace("$DATE_ADD", &playlist.date_add)
            .replace("$DATE_MOD", &playlist.date_mod)
            .replace("$USERNAME", &playlist.username)
            .replace("$USER_ID", &playlist.user_id.to_string())
            .replace("$TITLE", &playlist.title);

        let sanitized = sanitize(interpolated);
        if sanitized.is_empty() {
            error!("`playlist_name_schema` is empty after sanitization. Please change it.");
        }
        Some(sanitized)
    }

    /// Process this collection of albums.
    pub async fn fetch_albums(
        self,
        tx: flume::Sender<WrappedAlbum>,
        ongoing_download: OngoingDownload,
    ) -> ApiResult<()> {
        let client = ArlService::get().get_primary_client().await?;
        match self {
            Self::PrefetchedPlaylist(mut playlist) => {
                let (tx_renamer, rx_renamer) = flume::bounded(0);
                tokio::spawn(Self::rename_fix_task(rx_renamer, tx));
                let mut runner = ExponentialBackoffCancellableMut::new(
                    &mut playlist,
                    ongoing_download.cancel_token.clone(),
                );
                let mut songs = vec![];
                loop {
                    let album_result = runner.call(niceerr_dz::pagination::Paginator::next).await;
                    match album_result {
                        ExecutionResult::Completed(Some(Ok(chunk))) => songs.extend(chunk),
                        ExecutionResult::Completed(Some(Err(e))) => {
                            warn!("Retrying: error while fetching playlist albums: {e:?}");
                            continue;
                        }
                        ExecutionResult::Completed(None) | ExecutionResult::Cancelled => break,
                        ExecutionResult::TimedOut => {
                            error!(
                                "Couldn't fetch playlist after all {} retries",
                                runner.iter_count
                            );
                            return Ok(());
                        }
                    };
                }

                let song_count = songs.len();
                // Once we have all the songs, we can group them by album and then
                // fetch each album individually.
                let mut albums: HashMap<u64, Vec<IncompleteSong>> = HashMap::new();
                songs
                    .into_iter()
                    .for_each(|s| albums.entry(s.album_id).or_default().push(s));
                info!(
                    "Collected {} unique albums and {} songs.",
                    albums.len(),
                    song_count
                );

                for (album_id, songs) in albums {
                    match Self::get_album_backoff(&client, album_id, &ongoing_download.cancel_token)
                        .await
                    {
                        Ok(mut album) => {
                            album.songs = songs;
                            // We remove potentially repeated songs in the album. Some playlists contain
                            // duplicate references to the same song.
                            album.songs.sort_unstable_by_key(|s| s.id);
                            album.songs.dedup_by_key(|s| s.id);
                            tx_renamer
                                .send_async(album)
                                .await
                                .map_err(|_| ApiError::InternalError)?;
                        }
                        Err(e) => {
                            warn!("Couldn't fetch album, id={album_id} ({e:?})");
                            continue;
                        }
                    };
                }
            }
            Self::PrefetchedAlbum(album) => {
                let factory = || album.cover_jpg(&client).map_err(ApiError::from);
                let mut runner = EBCancellableRunner::new(factory, &ongoing_download.cancel_token);
                let cover = runner
                    .call_with_err_cbk(|e| e == &ApiError::NotFound)
                    .await?;
                let album = AlbumWCover::from((*album, cover));
                tx.send_async(WrappedAlbum::new(album)).await.ok();
            }
            Self::Unfetched(discography) => {
                let (tx_renamer, rx_renamer) = flume::bounded(0);
                tokio::spawn(Self::rename_fix_task(rx_renamer, tx));
                for album_id in discography.ids() {
                    match Self::get_album_backoff(
                        &client,
                        *album_id,
                        &ongoing_download.cancel_token,
                    )
                    .await
                    {
                        Ok(album) => {
                            tx_renamer
                                .send_async(album)
                                .await
                                .map_err(|_| ApiError::InternalError)?;
                        }
                        Err(e) => {
                            warn!("Couldn't fetch album, id={album_id} ({e:?})");
                            continue;
                        }
                    };
                }
            }
        };
        Ok(())
    }
}
