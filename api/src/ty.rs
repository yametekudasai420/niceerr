use axum::{
    extract::{rejection::JsonRejection, FromRequest, Request},
    http::StatusCode,
};

use crate::error::ApiError;

pub(crate) type ApiResult<T> = Result<T, ApiError>;
pub(crate) type JsonResult<T> = ApiResult<(StatusCode, axum::Json<T>)>;
pub(crate) type BodylessResult = ApiResult<StatusCode>;

// Custom JSON exttractor. We basically use this to transform JSON extract errors
// into API errors.
#[derive(Debug, Clone, Copy, Default)]
pub struct Json<T>(pub T);

impl<S, T> FromRequest<S> for Json<T>
where
    axum::Json<T>: FromRequest<S, Rejection = JsonRejection>,
    S: Send + Sync,
{
    type Rejection = ApiError;

    async fn from_request(req: Request, state: &S) -> Result<Self, Self::Rejection> {
        let (parts, body) = req.into_parts();

        let req = Request::from_parts(parts, body);

        axum::Json::<T>::from_request(req, state)
            .await
            .map_err(ApiError::from)
            .map(|json| Json(json.0))
    }
}
