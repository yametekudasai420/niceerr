use anyhow::Context;
use axum::Router;
use log::info;
use niceerr_config::Config;
use tokio::{
    fs::{remove_file, try_exists},
    net::{TcpListener, UnixListener},
    try_join,
};

use crate::shutdown::shutdown;

pub async fn spawn_servers(app: Router) -> anyhow::Result<()> {
    let unix = spawn_tcp_listener(app.clone());
    let tcp = spawn_unix_listener(app);
    try_join!(unix, tcp)?;
    Ok(())
}

async fn spawn_tcp_listener(app: Router) -> anyhow::Result<()> {
    let Some(address) = Config::get().listen_address.as_ref() else {
        return Ok(());
    };
    info!("Spawning API server (TCP) at {address:?}");
    let listener = TcpListener::bind(address).await?;
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown())
        .await
        .context("server: TCP listener abruptly terminated")?;
    Ok(())
}

async fn spawn_unix_listener(app: Router) -> anyhow::Result<()> {
    let Some(path) = Config::get().unix_socket.as_ref() else {
        return Ok(());
    };
    let file_exists = try_exists(&path).await?;
    if file_exists {
        remove_file(&path).await?;
    }
    info!("Spawning API server (unix) at {path:?}");
    let listener = UnixListener::bind(path).context("can't bind to unix socket on path")?;
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown())
        .await
        .context("server: unix listener abruptly terminated")?;
    remove_file(&path).await?;
    Ok(())
}
