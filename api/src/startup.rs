use futures_util::StreamExt;
use log::{error, info, warn};
use niceerr_config::{Config, DEFAULT_NICEERR_USERNAME};
use niceerr_dz::DzClient;
use niceerr_entity::{
    download_session::{DownloadKind, Model as DownloadSession},
    traits::Countable,
    user::Model as UserModel,
};
use serde_json::json;

use crate::{arl_service::ArlService, error::ApiError, ty::ApiResult};

pub(crate) async fn exec_db_startup_tasks() {
    create_default_user_conditionally().await.ok();
    update_progress_to_error().await.ok();
    if let Err(e) = migration_update_missing_title().await {
        error!("Failed to update missing titles: {e:?}");
    }
}

async fn create_default_user_conditionally() -> ApiResult<()> {
    if UserModel::count().await? > 0 {
        info!("User table isn't empty, skipping default user creation");
        return Ok(());
    }

    let password = &Config::get().default_admin_password;

    // Create default superuser. serde will automatically populate all
    // the remaining fields.
    let user = json!({
        "username": DEFAULT_NICEERR_USERNAME,
        "password": password,
        "superuser": true,
    });
    let user_create = serde_json::from_value::<UserModel>(user).expect("unexpected failure");
    user_create
        .create(None)
        .await
        .inspect_err(|e| error!("Failed to create default user: {}", e))?;
    info!(
        "created default user with username '{}'",
        DEFAULT_NICEERR_USERNAME
    );
    Ok(())
}

// Sets progress with IN_PROGRESS status to ERROR. This can only happen if the server was abruptly stopped.
async fn update_progress_to_error() -> ApiResult<()> {
    let updated = DownloadSession::update_progress_to_error().await?;
    if updated > 0 {
        warn!("{} download sessions were updated to error status", updated);
    }
    Ok(())
}

async fn get_deezer_title(client: &DzClient, kind: &DownloadKind, id: u64) -> ApiResult<String> {
    match kind {
        DownloadKind::Album => Ok(client.album(id).await?.name),
        DownloadKind::Artist => Ok(client.artist_info(id).await?.artist_name),
        DownloadKind::Playlist => Ok(client.playlist_paginated(id, 1).await?.into_inner().title),
        DownloadKind::Song => Ok(client.song(id).await?.song.title),
    }
}

async fn migration_update_missing_title() -> ApiResult<()> {
    let client = ArlService::get().get_primary_client().await?;
    let mut items = DownloadSession::migration_get_missing_title_stream().await?;
    let mut updated = 0;
    while let Some(item) = items.next().await.transpose()? {
        let Ok(id_int) = item.download_id.parse::<u64>() else {
            continue;
        };
        let title = match get_deezer_title(&client, &item.download_kind, id_int).await {
            Ok(title) => title,
            Err(ApiError::NotFound) => "(deleted by deezer)".into(),
            Err(e) => {
                error!("Failed to get title for {item:?}: {e:?}");
                continue;
            }
        };

        item.update_tile(title).await?;
        updated += 1;
        if updated % 10 == 0 {
            info!("Updated {} download sessions so far", updated);
        }
    }

    if updated > 0 {
        info!(
            "{} download sessions were updated with missing title",
            updated
        );
    }
    Ok(())
}
