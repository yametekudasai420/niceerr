use axum::{body::Body, response::IntoResponse, Json};
use camino::Utf8PathBuf;
use dashmap::DashMap;
use flume::Sender;
use niceerr_dz::types::{IncompleteSong, StreamFormat};
use niceerr_entity::{
    download_session::{DownloadSessionOptions, Model as DownloadSession, SkippedContentSet},
    song::Model as Song,
    user::Model as User,
};
use reqwest::{header, StatusCode};
use serde::{Deserialize, Serialize};
use std::{
    ops::Deref,
    sync::{atomic::AtomicUsize, Arc},
};
use tokio_util::sync::CancellationToken;
use tracing::Span;

use crate::{
    dz::album::{MediaCollection, WrappedAlbum},
    limit_service::LimitServicePermit,
    tmpfile::SelfDeleteSong,
};

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InnerOngoingDownload {
    #[serde(skip)]
    pub span: Span,
    #[serde(skip_serializing)]
    pub cancel_token: CancellationToken,
    #[serde(skip_serializing)]
    pub user: Arc<User>,
    pub dl_bytes_so_far: AtomicUsize,
    pub total_bytes: AtomicUsize,
    pub irrecoverable_errors: AtomicUsize,
    pub download_session: DownloadSession,
    // We need to hold on to this permit until the whole
    // thing finishes to prevent users from downloading more
    // stuff than they should.
    #[serde(skip)]
    #[allow(dead_code)]
    pub permit: LimitServicePermit,
    #[serde(skip)]
    pub root_path: Utf8PathBuf,
    #[serde(skip)]
    pub stream_data: Option<Sender<SelfDeleteSong>>,
    #[serde(skip)]
    // Song queue. This queue is shared among all ongoing downloads.
    pub song_queue_tx: Sender<QueuedSong>,
}

/// An enum, used to represent the two different types of responses
/// when new stuff is added.
pub enum DownloadResponse<T: Serialize> {
    ServerSide(T),
    Streaming(Body),
}

impl<T> IntoResponse for DownloadResponse<T>
where
    T: Serialize,
{
    fn into_response(self) -> axum::response::Response {
        match self {
            Self::ServerSide(json) => (StatusCode::OK, Json(json)).into_response(),
            Self::Streaming(body) => {
                let headers = [
                    (header::CONTENT_TYPE, "application/x-zip"),
                    (
                        header::CONTENT_DISPOSITION,
                        &format!("attachment; filename=\"{}\"", "streamed-music.zip"),
                    ),
                ];
                (StatusCode::OK, headers, body).into_response()
            }
        }
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename = "camelCase")]
pub struct OngoingDownload {
    #[serde(flatten)]
    pub inner: Arc<InnerOngoingDownload>,
}

impl Deref for OngoingDownload {
    type Target = InnerOngoingDownload;
    fn deref(&self) -> &InnerOngoingDownload {
        &self.inner
    }
}

// Download guard.
// This guard only serves to clean up the download list
// once we're done with the download request.
pub struct OngoingRequestGuard {
    pub(crate) request: OngoingDownload,
}

impl OngoingRequestGuard {
    pub fn new(request: &OngoingDownload) -> Self {
        Self {
            request: request.clone(),
        }
    }
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
/// Only used to serialize the system's download state.
pub struct SerializedState {
    pub user: User,
    pub ongoing_downloads: Vec<OngoingDownload>,
}

#[derive(Debug, Clone)]
pub struct DownloadService {
    pub(crate) ongoing_downloads: DashMap<Arc<User>, Vec<OngoingDownload>>,
    // Request queue: this queue processes incoming download requests, whether they are
    // single , songs, or discographies.
    pub(crate) request_tx: Sender<QueuedDownloadRequest>,
    // Update download session queue: receives, updates and saves to db the results of
    // the incoming requests.
    pub(crate) update_dl_session_tx: Sender<OngoingDownload>,
    // Song to db queue: sends the result of the downloaded songs to db.
    pub(crate) song_to_db_tx: Sender<(Song, OngoingDownload)>,
}

// This type is sent to the song download queue. It's quite chunky, but it works well.
pub(crate) type QueuedSong = (
    Arc<OngoingRequestGuard>,
    IncompleteSong,
    Arc<WrappedAlbum>,
    OngoingDownload,
);

pub type QueuedDownloadRequest = (OngoingDownload, MediaCollection);

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct DownloadOptions {
    #[serde(default)]
    pub stream_format: StreamFormat,
    #[serde(default)]
    pub include_credited: bool,
    #[serde(default)]
    pub allow_fallback: bool,
    #[serde(default)]
    pub stream: bool,
    #[serde(default)]
    pub skipped_content: SkippedContentSet,
}

impl DownloadOptions {
    pub fn as_db_object(self) -> DownloadSessionOptions {
        DownloadSessionOptions {
            include_credited: self.include_credited,
            allow_fallback: self.allow_fallback,
            skipped_content: self.skipped_content,
            is_stream: self.stream,
        }
    }
}

// The type of queue to be used with this
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub(crate) enum DownloadQueue {
    ServerSide,
    Stream,
}

/// Guard that literally increases and decreases a shared atomic
/// counter whenever it's created or dropped. Used to keep track
/// of the number of spawned workers.
#[derive(Debug)]
pub(crate) struct WorkerCount {}
