use axum::body::Body;
use camino::Utf8PathBuf;
use chrono::Utc;
use flume::Sender;
use futures_util::{future::join, task::AtomicWaker, TryFutureExt};
use log::{error, info, warn};
use niceerr_config::Config;
use niceerr_dz::types::StreamFormat;
use niceerr_entity::{
    assigned_permission::Permission,
    download_session::{DownloadKind, DownloadQuality, Model as DownloadSession, OutcomeKind},
    song::Model as Song,
    user::Model as User,
};
use std::{
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, LazyLock, OnceLock,
    },
    time::Duration,
};
use tokio::{select, time::interval};
use tokio_util::sync::CancellationToken;
use tracing::{info_span, Instrument};
use uuid::Uuid;

use crate::{
    arl_service::ArlService,
    cancellation::CancellableRunner,
    clean_tmp::TmpFolder,
    download::ty::{
        DownloadService, OngoingDownload, OngoingRequestGuard, QueuedDownloadRequest, QueuedSong,
        SerializedState,
    },
    dz::{album::MediaCollection, song::SongDownloader},
    error::ApiError,
    limit_service::{LimitService, WrappedUser},
    scheduled_tracker::{ScheduledTaskName, ScheduledTaskTracker},
    slow_stream::SlowConsumerStreamWrapper,
    storage_zip::{ZipDownload, ZipStreamer},
    tmpfile::SelfDeleteSong,
    ty::ApiResult,
    webhook_service::WebhookService,
};

use super::ty::{
    DownloadOptions, DownloadQueue, DownloadResponse, InnerOngoingDownload, WorkerCount,
};

// Static download service.
static SERVICE: OnceLock<DownloadService> = OnceLock::new();
static ONGOING_WAKER: LazyLock<Arc<AtomicWaker>> = LazyLock::new(|| Arc::new(AtomicWaker::new()));
static WORKER_COUNT: OnceLock<AtomicUsize> = OnceLock::new();

impl DownloadService {
    // initialize this service
    pub fn init() {
        if SERVICE.get().is_some() {
            warn!("downloads service is already initialized");
            return;
        }
        let (request_tx, request_rx) = flume::unbounded();
        let (update_dl_session_tx, update_dl_session_rx) = flume::unbounded();
        let (song_to_db_tx, song_to_db_rx) = flume::unbounded();

        let this = Self {
            ongoing_downloads: Default::default(),
            request_tx,
            update_dl_session_tx,
            song_to_db_tx,
        };
        WORKER_COUNT
            .set(AtomicUsize::new(0))
            .expect("Cannot initialize downloads service (worker count singleton)");
        SERVICE
            .set(this)
            .expect("Cannot initialize downloads service");
        tokio::spawn(Self::request_queue_task(request_rx));
        tokio::spawn(Self::update_download_session_task(update_dl_session_rx));
        tokio::spawn(Self::song_to_db_task(song_to_db_rx));
        tokio::spawn(Self::run_scheduled_task());
        info!(
            "Download service started, workers={}",
            Config::get().server_side_download_workers
        );
    }

    /// Get a waker that is triggered whenever a new request is made.
    ///
    /// If there are any clients using the SSE endpoint for ongoing downloads,
    /// the stream will be IMMEDIATELY woken up.
    pub fn ongoing_waker() -> Arc<AtomicWaker> {
        ONGOING_WAKER.clone()
    }

    pub fn active_worker_count() -> usize {
        WORKER_COUNT
            .get()
            .expect("Download service not initialized")
            .load(Ordering::Relaxed)
    }

    fn spawn_song_queue_workers(queue: DownloadQueue) -> Sender<QueuedSong> {
        let (tx, rx) = flume::bounded(1);
        let workers = match queue {
            DownloadQueue::ServerSide => Config::get().server_side_download_workers,
            DownloadQueue::Stream => Config::get().streaming_download_workers,
        };
        info!("Spawning {workers} download workers (queue: {queue:?})");
        for _ in 0..workers {
            tokio::spawn(Self::song_queue_task(rx.clone()));
        }
        tx
    }

    /// Get or initialize the on-demand download queue for both streaming and
    /// server-side downloads. Depending on the passed queue, this function
    /// will try to find an existing queue (if there's already an ongoing
    /// download of the same type) or initialize the download worker pool.
    fn get_or_init_song_queue(queue: DownloadQueue) -> Sender<QueuedSong> {
        let this = SERVICE.get().expect("Downloads service not initialized");
        let find_stream = queue == DownloadQueue::Stream;
        this.ongoing_downloads
            .iter()
            .find_map(|it| {
                it.value().iter().find_map(|ongoing| {
                    (ongoing.download_session.is_stream == find_stream).then(|| {
                        info!("Reusing existing queue: {queue:?}");
                        ongoing.song_queue_tx.clone()
                    })
                })
            })
            .unwrap_or_else(|| Self::spawn_song_queue_workers(queue))
    }

    pub fn shrink_queues(&self) {
        self.ongoing_downloads.shrink_to_fit();
        self.request_tx.shrink_to_fit();
        self.update_dl_session_tx.shrink_to_fit();
        self.song_to_db_tx.shrink_to_fit();
    }

    pub async fn scheduled_task() -> ApiResult<()> {
        let _tracker = ScheduledTaskTracker::try_guard(ScheduledTaskName::Housekeeping)?;
        LimitService::get().shrink_to_fit();
        let download_svc = Self::get_service();
        download_svc.shrink_queues();
        // Make sure we don't run a full cleanup if there are ongoing downloads.
        if download_svc.ongoing_downloads.is_empty() {
            TmpFolder::run_full_cleanup().await
        } else {
            TmpFolder::run_partial_cleanup().await;
        }
        Ok(())
    }

    async fn run_scheduled_task() {
        let mut sleeper = {
            let config = Config::get();
            interval(Duration::from_secs(config.housekeeping_interval_sec.into()))
        };
        loop {
            sleeper.tick().await;
            Self::scheduled_task()
                .await
                .inspect_err(|e| error!("Housekeeping task failed: {e}"))
                .ok();
        }
    }

    /// Get the number of active downloads.
    pub fn active_downloads_count() -> usize {
        let service = DownloadService::get_service();
        service.ongoing_downloads.len()
    }

    /// Returns the ongoing downloas for a given user.
    ///
    /// This method will hide downloads from other users if the caller doesn't have
    /// a sufficient level to see them.
    pub fn get_serializable_instance(caller: &User) -> Vec<SerializedState> {
        let service = DownloadService::get_service();
        let mut state = Vec::new();
        for item in &service.ongoing_downloads {
            let (user, ongoing_downloads) = (item.key(), item.value());
            if !caller.has_authority_over(user, &Permission::ReadOngoingDownload) {
                continue;
            }

            let item = SerializedState {
                user: (**user).clone(),
                ongoing_downloads: ongoing_downloads.clone(),
            };
            state.push(item)
        }
        state
    }

    pub async fn download_artist(
        user: WrappedUser,
        id: u64,
        options: DownloadOptions,
    ) -> ApiResult<DownloadResponse<OngoingDownload>> {
        let client = ArlService::get().get_primary_client().await?;
        let albums_f = client.full_discography_ids(id, options.include_credited);
        let artist_f = client.artist_info(id);
        let (albums, artist) = join(albums_f, artist_f).await;
        let (albums, artist) = (albums?, artist?);

        let title = &artist.artist_name;
        let session = DownloadSession::new_ongoing_artist(
            title,
            &user,
            options.stream_format.into_db_type(),
            id.to_string(),
            options.as_db_object(),
        )
        .await?;
        Self::get_service()
            .process_download(user, MediaCollection::Unfetched(albums), session)
            .await
    }

    pub async fn download_playlist(
        user: WrappedUser,
        id: u64,
        mut options: DownloadOptions,
    ) -> ApiResult<DownloadResponse<OngoingDownload>> {
        // It doesn't make sense to include credited albums because this is just 1 album.
        options.include_credited = false;
        let client = ArlService::get().get_primary_client().await?;
        let playlist = client.playlist_paginated(id, 3000).await?;
        let title = playlist.title.to_owned();
        let queued = MediaCollection::PrefetchedPlaylist(playlist);
        let session = DownloadSession::new_ongoing_playlist(
            &title,
            &user,
            options.stream_format.into_db_type(),
            id.to_string(),
            options.as_db_object(),
        )
        .await?;
        Self::get_service()
            .process_download(user, queued, session)
            .await
    }

    pub async fn download_album(
        user: WrappedUser,
        id: u64,
        mut options: DownloadOptions,
    ) -> ApiResult<DownloadResponse<OngoingDownload>> {
        // It doesn't make sense to include credited albums because this is just 1 album.
        options.include_credited = false;
        let album = ArlService::get()
            .get_primary_client()
            .await?
            .album(id)
            .await?;
        let title = album.name.to_owned();
        let album_streamer = MediaCollection::PrefetchedAlbum(Box::new(album));
        let session = DownloadSession::new_ongoing_album(
            &title,
            &user,
            options.stream_format.into_db_type(),
            id.to_string(),
            options.as_db_object(),
        )
        .await?;
        Self::get_service()
            .process_download(user, album_streamer, session)
            .await
    }

    pub async fn download_song(
        user: WrappedUser,
        id: u64,
        mut options: DownloadOptions,
    ) -> ApiResult<DownloadResponse<OngoingDownload>> {
        // It doesn't make sense to include credited albums because this is just a song.
        options.include_credited = false;
        let (album, song) = {
            let client = ArlService::get().get_primary_client().await?;
            let song = client.song(id).await?;
            let mut album = Box::new(client.album(song.album_id).await?);
            album.songs.retain(|s| s.id == song.id);
            (album, song)
        };
        let title = &song.title;
        let session = DownloadSession::new_ongoing_song(
            title,
            &user,
            options.stream_format.into_db_type(),
            id.to_string(),
            options.as_db_object(),
        )
        .await?;

        Self::get_service()
            .process_download(user, MediaCollection::PrefetchedAlbum(album), session)
            .await
    }

    fn inner_stop_all(&self) {
        self.ongoing_downloads
            .iter()
            .for_each(|download| download.iter().for_each(|request| request.stop()));
    }

    /// Stop all ongoing download requests.
    pub fn stop_all() {
        Self::get_service().inner_stop_all();
    }

    /// Get the number of ongoing download requests.
    pub fn ongoing_download_count() -> usize {
        let service = DownloadService::get_service();
        service
            .ongoing_downloads
            .iter()
            .fold(0, |acc, x| acc + x.value().len())
    }

    // Add a request to this user's queue.
    fn add_request_to_service(&self, request: OngoingDownload) {
        if let Some(mut d) = self.ongoing_downloads.get_mut(&request.user) {
            d.push(request)
        } else {
            let mut v = Vec::new();
            let user = request.user.clone();
            v.push(request);
            self.ongoing_downloads.insert(user, v);
        };
        // Wake the ongoing download waker to notify all clients
        ONGOING_WAKER.wake();
    }

    // Remove a request from this user's queue.
    fn remove_request_from_service(&self, user: &User, id: &Uuid) {
        let mut cleanup_user = false;
        if let Some(mut user_downloads) = self.ongoing_downloads.get_mut(user) {
            user_downloads.retain(|v| v.id() != id);
            cleanup_user = user_downloads.is_empty();
        }
        // Clean up this user's download queue if it's empty
        // Note: we need to do this in a separate step to avoid deadlocks
        if cleanup_user {
            self.ongoing_downloads.remove(user);
        }
        self.ongoing_downloads.shrink_to_fit();
    }

    // Get a download request from the in-memory state
    pub fn find_ongoing(user: &User, id: &Uuid) -> ApiResult<OngoingDownload> {
        let service = SERVICE.get().expect("download service isn't initialized");
        if let Some(user_downloads) = service.ongoing_downloads.get(user) {
            if let Some(request) = user_downloads.iter().find(|v| v.id() == id) {
                return Ok(request.clone());
            }
        }
        Err(ApiError::NotFound)
    }

    /// Get a static reference to the service.
    pub fn get_service() -> &'static DownloadService {
        SERVICE.get().expect("download service isn't initialized")
    }

    /// Process an unprepared download request, regardless of its kind.
    async fn process_download(
        &self,
        user: WrappedUser,
        collection: MediaCollection,
        session: DownloadSession,
    ) -> ApiResult<DownloadResponse<OngoingDownload>> {
        let (request, zip_stream) = OngoingDownload::new(user, session, &collection);
        self.add_request_to_service(request.clone());

        self.request_tx
            .send_async((request.clone(), collection))
            .await
            .map_err(|e| {
                warn!("failed to send download request: {}", e);
                ApiError::MaxResourceLimitExceeded
            })?;
        if let Some(stream) = zip_stream {
            return Ok(DownloadResponse::Streaming(Body::from_stream(stream)));
        }
        Ok(DownloadResponse::ServerSide(request))
    }

    async fn song_to_db_task(
        receiver: flume::Receiver<(niceerr_entity::song::Model, OngoingDownload)>,
    ) {
        while let Ok((song, ongoing_download)) = receiver.recv_async().await {
            match song.save().await {
                Ok(saved) => {
                    // Don't trigger webhook event for streams. Streams don't save any data
                    // to disk, so it doesn't make sense to trigger it.
                    if ongoing_download.stream_data.is_none() {
                        WebhookService::send(saved);
                    }
                }
                Err(e) => {
                    error!("Couldn't save song result to database: {e:?}");
                }
            };
        }
    }

    // Song downloader task
    async fn song_queue_task(receiver: flume::Receiver<QueuedSong>) {
        let _count = WorkerCount::new();
        while let Ok(queued_song) = receiver.recv_async().await {
            let (_guard, song, album, ongoing_download) = queued_song;
            let span =
                info_span!(parent: &ongoing_download.span, "song", id=%song.id, title=song.title);

            let mut model = Song::new_unsaved(ongoing_download.id(), song.id as i64);
            let f = || async {
                let song = SongDownloader::new(album, &ongoing_download, &mut model).await;
                if let Ok(mut song) = song {
                    song.download().await;
                }
            };
            f().instrument(span).await;

            let song = SelfDeleteSong::new(model.clone(), &ongoing_download);
            // if this is a stream request, block this downloader worker until
            // the song's contents get flushed to the client.
            if let Some(streaming) = &ongoing_download.stream_data {
                streaming.send_async(song).await.ok();
            }

            Self::get_service()
                .song_to_db_tx
                .try_send((model, ongoing_download))
                .inspect_err(|e| error!("Couldn't send song to db: {e:?}"))
                .ok();
            // When the last guard for this download request is dropped,
            // the request itself will be removed from the list of ongoing
            // downloads.
        }
        info!("song queue task: receiver is down, exiting");
    }

    // processes full blown download requests of any type, whether it's an album, a song,
    // whatever.
    async fn request_queue_task(receiver: flume::Receiver<QueuedDownloadRequest>) {
        loop {
            let (request, kind) = receiver
                .recv_async()
                .await
                .expect("System request channel is closed. This is fatal.");
            if let Err(e) = request
                .process_albums(kind)
                .instrument(request.span.clone())
                .await
            {
                error!("Failed to process download request: {e:?}");
            } else {
                info!("Successfully finished processing download request");
            }
        }
    }

    // updates the db "outcome" of this download session. Whether it failed, or it succeeded,
    // we update its outcome.
    async fn update_download_session_task(receiver: flume::Receiver<OngoingDownload>) {
        while let Ok(download) = receiver.recv_async().await {
            let total_bytes = download.total_bytes.load(Ordering::Relaxed);
            let downloaded_bytes = download.dl_bytes_so_far.load(Ordering::Relaxed);
            let completed = total_bytes > 0 && total_bytes == downloaded_bytes;
            let was_stopped = download.is_stopped();
            let has_irrecoverable_errors =
                download.irrecoverable_errors.load(Ordering::Relaxed) > 0 || downloaded_bytes == 0;
            let outcome = match (completed, was_stopped, has_irrecoverable_errors) {
                (true, false, false) => OutcomeKind::Success,
                (false, false, false) => OutcomeKind::PartiallyDownloaded,
                (false, true, _) => OutcomeKind::Stopped,
                (_, _, _) => OutcomeKind::Error,
            };
            download
                .download_session
                .update_status(outcome)
                .await
                .inspect_err(|e| error!("couldn't update download session: {e:?}"))
                .ok();
        }
        warn!("download service: update task is down");
    }
}

impl OngoingDownload {
    /// Create a new ongoing download. This method returns a tuple, where the second
    /// element is Some if this download is stremable.
    fn new(
        user: WrappedUser,
        download_session: DownloadSession,
        collection: &MediaCollection,
    ) -> (
        OngoingDownload,
        Option<SlowConsumerStreamWrapper<ZipStreamer>>,
    ) {
        let (user, permit) = user.into_inner();
        let span =
            info_span!("download", id = %download_session.id, kind=%download_session.download_kind);

        let root_path = Self::build_root_prefix(&user, &download_session, Some(collection));
        let mut stream = None;
        let mut download_queue = DownloadQueue::ServerSide;
        let (tx, rx) = flume::bounded(Config::get().streaming_download_queue_size as usize);
        if download_session.is_stream {
            download_queue = DownloadQueue::Stream;
            stream = Some(tx);
        }
        let inner = InnerOngoingDownload {
            total_bytes: AtomicUsize::new(0),
            cancel_token: CancellationToken::new(),
            dl_bytes_so_far: AtomicUsize::new(0),
            irrecoverable_errors: AtomicUsize::new(0),
            user: Arc::new(user),
            // options,
            span,
            permit,
            download_session,
            root_path,
            stream_data: stream,
            song_queue_tx: DownloadService::get_or_init_song_queue(download_queue),
        };
        let ongoing = OngoingDownload {
            inner: Arc::new(inner),
        };
        let zip_stream = ongoing
            .stream_data
            .as_ref()
            .map(|_| ZipDownload::new_stream_live(&ongoing, rx));
        (ongoing, zip_stream)
    }

    pub fn id(&self) -> &Uuid {
        &self.inner.download_session.id
    }

    /// Create the root folder for this download.
    pub(crate) fn build_root_prefix(
        user: &User,
        download_session: &DownloadSession,
        collection: Option<&MediaCollection>,
    ) -> Utf8PathBuf {
        let mut master_folder = Utf8PathBuf::new();
        if download_session.is_stream {
            let tmp_path = Utf8PathBuf::from(&Config::get().temp_folder)
                .join("niceerr")
                .join(Utc::now().timestamp_millis().to_string());
            master_folder.push(&tmp_path);
        } else {
            master_folder.push(&Config::get().downloads_folder);
        }
        if download_session.download_kind == DownloadKind::Playlist {
            if let Some(folder) = &Config::get().playlist_folder {
                master_folder.push(folder);
            }
        }
        let mut path = match &user.download_path {
            Some(path) => master_folder.join(&**path),
            _ => master_folder,
        };

        if let Some(playlist_path) = collection.and_then(|c| c.build_playlist_interpolated_path()) {
            path.push(playlist_path);
        }

        path
    }

    pub fn is_stopped(&self) -> bool {
        self.cancel_token.is_cancelled()
    }

    /// Begin processing the passed albums.
    async fn process_albums(&self, streamer: MediaCollection) -> ApiResult<()> {
        let (tx, rx) = flume::bounded(Config::get().album_fetcher_queue_size as usize);
        info!("Saving to: {}", self.root_path);
        let this_session = self.clone();
        tokio::spawn(async { streamer.fetch_albums(tx, this_session).await.ok() });
        let guard = Arc::new(OngoingRequestGuard::new(self));

        while !self.is_stopped() {
            // process albums asynchronously
            let album_fn = || rx.recv_async();
            let Some(Ok(mut album)) = CancellableRunner::new(album_fn, &self.cancel_token)
                .call()
                .await
            else {
                return Ok(());
            };

            // Skip stuff, if enabled
            if self.download_session.skip_live() && album.is_live()
                || self.download_session.skip_single() && album.is_single()
                || self.download_session.skip_compilation() && album.is_maybe_compilation()
                || self.download_session.skip_ep() && album.is_ep()
                || self.download_session.skip_album() && album.is_album()
            {
                info!(
                    "Skipping album: '{}' ({}), t={:?}",
                    album.name, album.id, album.record_type
                );
                continue;
            }

            let album_size = album.byte_size(&self.stream_format());
            if self.user.would_exceed_download_quota(album_size).await? {
                self.stop();
                warn!(
                    "User would be over quota: {} ('{}') (album={}, quota={})",
                    self.user.id,
                    self.user.username,
                    album_size,
                    self.user.max_bandwidth_bytes_per_day
                );
                return Ok(());
            }
            self.total_bytes.fetch_add(album_size, Ordering::Relaxed);
            let songs = std::mem::take(&mut album.songs);
            let album = Arc::new(album);

            for song in songs {
                let fut = self
                    .song_queue_tx
                    .send_async((guard.clone(), song, album.clone(), self.clone()))
                    .map_err(|e| {
                        error!("fatal error: song queue receiver is down: {e:?}");
                        ApiError::InternalError
                    });
                select! {
                    res = fut => res?,
                    _ = self.cancel_token.cancelled() => return Ok(()),
                };
            }
        }
        Ok(())
    }

    pub fn stream_format(&self) -> StreamFormat {
        match self.download_session.download_quality {
            DownloadQuality::Flac => StreamFormat::Flac,
            DownloadQuality::Mp3_320 => StreamFormat::Mp3_320,
            DownloadQuality::Mp3_128 => StreamFormat::Mp3_128,
        }
    }

    /// Signals this request to stop processing.
    pub fn stop(&self) {
        self.cancel_token.cancel();
    }

    pub fn increase_error_count(&self) {
        self.irrecoverable_errors.fetch_add(1, Ordering::Relaxed);
    }
}

impl Drop for OngoingRequestGuard {
    fn drop(&mut self) {
        let service = DownloadService::get_service();
        let _enter = self.request.span.enter();
        // remove this request from the "live" queue
        service.remove_request_from_service(&self.request.user, self.request.id());
        // ...and update this request's status
        service
            .update_dl_session_tx
            .send(self.request.clone())
            .inspect_err(|e| error!("drop: couldn't update this result: {e:#?}"))
            .ok();
        info!("Download request {}: FINISHED", self.request.id());
    }
}

// Trait to convert a database dl quality enum into a dz download quality enum.
trait IntoDzType {
    fn into_db_type(self) -> DownloadQuality;
}

impl IntoDzType for StreamFormat {
    fn into_db_type(self) -> DownloadQuality {
        match self {
            StreamFormat::Flac => DownloadQuality::Flac,
            StreamFormat::Mp3_320 => DownloadQuality::Mp3_320,
            StreamFormat::Mp3_128 => DownloadQuality::Mp3_128,
        }
    }
}

impl WorkerCount {
    fn new() -> WorkerCount {
        WORKER_COUNT
            .get()
            .expect("Download service not initialized")
            .fetch_add(1, Ordering::Relaxed);
        WorkerCount {}
    }
}

impl Drop for WorkerCount {
    fn drop(&mut self) {
        WORKER_COUNT
            .get()
            .expect("Download service not initialized")
            .fetch_sub(1, Ordering::Relaxed);
    }
}
