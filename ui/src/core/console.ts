import { useApi } from 'src/boot/axios';

interface ArlUpdate {
  updated: number;
  untouched: number;
}

async function fetchAvailableCountries(): Promise<string[]> {
  const url = '/console/arl/available-countries';
  return useApi()
    .get(url)
    .then((response) => response.data);
}

async function resetPriorities(): Promise<ArlUpdate> {
  const url = '/console/arl/reset-priorities';
  return useApi()
    .post(url)
    .then((resp) => resp.data);
}

async function reprioritizeUsingCountries(
  countries: string[]
): Promise<ArlUpdate> {
  const url = '/console/arl/reprioritize-by-country';
  return useApi()
    .post(url, { countries: countries })
    .then((resp) => resp.data);
}

export { fetchAvailableCountries, reprioritizeUsingCountries, resetPriorities };
