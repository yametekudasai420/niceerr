import { useApi } from 'src/boot/axios';
import { PaginatedResponse, Pagination } from './pagination';

interface Arl {
  id: string;
  token: string;
  alias: string;
  createdAt: string;
  expiresAt: string;
  createdBy: string;
  priority: number;
  country: string;
}

interface ArlUpdate {
  token?: string;
  alias?: string;
}

async function fetchArls(
  pagination: Pagination
): Promise<PaginatedResponse<Arl>> {
  const url = `/arl${pagination.asQueryParams()}`;
  return new PaginatedResponse(await useApi().get(url));
}

async function deleteArl(id: string): Promise<void> {
  await useApi().delete(`/arl/${id}`);
}

async function createArl(arl: ArlUpdate): Promise<void> {
  await useApi().post('/arl', arl);
}

async function updateArl(id: string, arl: ArlUpdate): Promise<void> {
  await useApi().put(`/arl/${id}`, arl);
}

export type { Arl, ArlUpdate };
export { fetchArls, deleteArl, createArl, updateArl };
