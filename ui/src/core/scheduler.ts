import { z } from 'zod';
import { useApi } from 'src/boot/axios';

const ScheduledTaskNameSchema = z.enum([
  'userDownloadsPrune',
  'housekeeping',
  'pruneExpiredArl',
  'primaryArlRefresh',
  'relayTokenRefresh',
]);

const TaskStateSchema = z.enum(['NEVER_EXECUTED', 'RUNNING', 'COMPLETED']);

const ScheduledTaskSchema = z.object({
  state: TaskStateSchema,
  lastRun: z.string().nullable().optional(),
});

const SchedulerStateSchema = z.record(
  ScheduledTaskNameSchema,
  ScheduledTaskSchema
);

// Type inference from Zod schemas
type ScheduledTaskName = z.infer<typeof ScheduledTaskNameSchema>;
type TaskState = z.infer<typeof TaskStateSchema>;
type ScheduledTask = z.infer<typeof ScheduledTaskSchema>;
type SchedulerState = z.infer<typeof SchedulerStateSchema>;

async function getSchedulerState(): Promise<SchedulerState> {
  const url = '/console/schedule/state';
  return useApi()
    .get(url)
    .then((resp) => SchedulerStateSchema.parse(resp.data));
}

async function runScheduledTask(
  task: ScheduledTaskName
): Promise<SchedulerState> {
  const url = `/console/schedule/${task}`;
  return useApi()
    .post(url)
    .then((resp) => SchedulerStateSchema.parse(resp.data));
}

export { getSchedulerState, runScheduledTask };
export type { ScheduledTaskName, TaskState, ScheduledTask, SchedulerState };
