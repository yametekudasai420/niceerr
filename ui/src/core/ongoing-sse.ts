import { useApi } from 'src/boot/axios';
import { OngoingDownload, OngoingUserDlEntry } from './download';
import {
  SSE_EXTRA_TIMEOUT_MSEC,
  SSE_RESET_CONNECTION_TIMEOUT_MSEC,
} from './consts';
import QNotification from './notifications';

type CallbackFunction = (data: OngoingDownload[]) => void;
const INITIAL_RECONNECT_DELAY_MS = 1000;

interface EventReaderPayload {
  done: boolean;
  value?: string;
}

class OngoingDownloadEventReader {
  private sseResetConnectionTimeout: number = SSE_RESET_CONNECTION_TIMEOUT_MSEC;
  private isFullyStopped = false;
  private lastPingReceived: number | null = null;
  private timeoutId: ReturnType<typeof setTimeout> | null = null;
  private reader: ReadableStreamDefaultReader<string> | null = null;
  private callback: CallbackFunction;
  private reconnectAttempts = 0;
  private maxReconnectAttempts = 10;

  constructor(callback: CallbackFunction) {
    this.callback = callback;
  }

  private getSseConnectionTimeout(): number {
    return this.sseResetConnectionTimeout + SSE_EXTRA_TIMEOUT_MSEC;
  }

  private resetTimeout() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    const sseTimeout = this.getSseConnectionTimeout();
    this.timeoutId = setTimeout(() => {
      console.warn(`SSE: didn't receive msgs for ${sseTimeout} ms`);
      this.handleDisconnection();
    }, sseTimeout);
  }

  public timeSinceLastPing(): number | null {
    return this.lastPingReceived
      ? new Date().getTime() - this.lastPingReceived
      : null;
  }

  private async processText({
    done,
    value,
  }: EventReaderPayload): Promise<void> {
    if (done) {
      if (!this.isFullyStopped) {
        this.lastPingReceived = null;
        console.warn('SSE: Server closed the connection unexpectedly');
        this.handleDisconnection();
      }
      return;
    }
    if (!value) return;

    this.resetTimeout();
    this.lastPingReceived = new Date().getTime();
    this.reconnectAttempts = 0; // Reset reconnect attempts on successful message

    if (value.startsWith('data')) {
      const json = value.split('data:')[1];
      const obj = JSON.parse(json) as Array<OngoingUserDlEntry>;
      this.callback(OngoingDownloadEventReader.prepareForQuasarTable(obj));
    }

    this.reader?.read().then(this.processText.bind(this));
  }

  private async handleDisconnection() {
    if (this.isFullyStopped) return;

    this.cleanup();

    if (this.reconnectAttempts < this.maxReconnectAttempts) {
      this.reconnectAttempts++;
      console.warn(
        `SSE: Attempting to reconnect (attempt ${this.reconnectAttempts}/${this.maxReconnectAttempts})`
      );

      const delay = Math.min(
        INITIAL_RECONNECT_DELAY_MS * this.reconnectAttempts,
        SSE_RESET_CONNECTION_TIMEOUT_MSEC
      );

      setTimeout(() => {
        this.getOngoing();
      }, delay);
    } else {
      console.error('SSE: Max reconnection attempts reached. Stopping.');
      new QNotification(
        'Connection Error',
        'Failed to maintain connection to the server. Please refresh the page.'
      ).finishNegative();
    }
  }

  public async getOngoing(): Promise<void> {
    if (this.isFullyStopped) return;

    try {
      const response = await useApi().get('/download/ongoingsse', {
        responseType: 'stream',
        adapter: 'fetch',
        fetchOptions: { priority: 'high' },
      });

      this.sseResetConnectionTimeout =
        (response.headers['x-keepalive-interval-sec'] as number) * 1000;
      console.info(
        `SSE: successfully initialized, keep-alive interval: ${
          this.sseResetConnectionTimeout / 1000
        }s.`
      );

      this.resetTimeout();

      const stream: ReadableStream = response.data;
      this.reader = stream.pipeThrough(new TextDecoderStream()).getReader();
      this.reader.read().then(this.processText.bind(this));
    } catch (e) {
      console.error('SSE: error establishing connection:', e);
      this.handleDisconnection();
    }
  }

  private static prepareForQuasarTable(
    entries: Array<OngoingUserDlEntry>
  ): Array<OngoingDownload> {
    return entries.flatMap(({ user, ongoingDownloads }) =>
      ongoingDownloads.map(
        ({
          dlBytesSoFar,
          totalBytes,
          irrecoverableErrors,
          downloadSession,
        }) => ({
          username: user.username,
          userId: user.id,
          dlBytesSoFar,
          totalBytes,
          irrecoverableErrors,
          downloadSession,
        })
      )
    );
  }

  public stop(): void {
    console.info('SSE: stopped.');
    this.isFullyStopped = true;
    this.cleanup();
  }

  private cleanup(): void {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
      this.timeoutId = null;
    }
    if (this.reader) {
      this.reader.cancel();
      this.reader = null;
    }
  }
}

export type { CallbackFunction };
export default OngoingDownloadEventReader;
