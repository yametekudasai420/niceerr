import { useApi } from 'src/boot/axios';
import { PaginatedResponse, Pagination } from './pagination';
import { SkippedContent } from 'src/stores/optionsStore';

interface DownloadHistoryEntry {
  id: string;
  title: string;
  downloadId: string;
  downloadQuality: string;
  downloadKind: string;
  downloadedBy: string;
  outcome: string;
  createdAt: string;
  totalBytesSum: number;
  downloadedBytesSum: number;
  songCount: number;
  errorCount: number;
  username: string;
  isPruned: boolean;
  isStream: boolean;
  includeCredited: boolean;
  allowFallback: boolean;
  skippedContent: SkippedContent[];
}

async function fetchDownloadHistory(
  pagination: Pagination
): Promise<PaginatedResponse<DownloadHistoryEntry>> {
  const url = `/download/history${pagination.asQueryParams()}`;
  return new PaginatedResponse(await useApi().get(url));
}
interface DownloadHistoryEntry {
  downloadedBy: string;
  id: string;
}

class LocalDownloader {
  private entry: DownloadHistoryEntry;
  private ignoreErrors: boolean;
  private callback: (progress: number) => void;
  private controller: AbortController | null = null;

  constructor(
    entry: DownloadHistoryEntry,
    ignoreErrors: boolean,
    callback: (progress: number) => void
  ) {
    this.entry = entry;
    this.ignoreErrors = ignoreErrors;
    this.callback = callback;
  }

  async start(): Promise<void> {
    const userId = this.entry.downloadedBy;
    const downloadId = this.entry.id;
    let url = `/download/zip/${userId}/${downloadId}`;
    if (this.ignoreErrors) {
      url += '?ignoreErrors=true';
    }

    this.controller = new AbortController();

    const response = await useApi().get(url, {
      responseType: 'blob',
      signal: this.controller.signal,
      onDownloadProgress: (pEvent) => this.callback(pEvent.loaded),
      adapter: 'fetch',
      fetchOptions: { priority: 'low' },
    });

    if (response.status !== 200) {
      throw new Error(`Error downloading zip: ${response.status}`);
    }

    const blob = new Blob([response.data], { type: 'application/zip' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = `${downloadId}.zip`;
    link.click();
    URL.revokeObjectURL(link.href);

    this.controller = null;
  }

  cancelDownload(): void {
    if (this.controller) {
      this.controller.abort();
      this.controller = null;
    }
  }
}

export { fetchDownloadHistory, LocalDownloader };
export type { DownloadHistoryEntry };
