import { useApi } from 'src/boot/axios';
import router from 'src/router';
import useCredsStore from 'src/stores/userCreds';

export const useRefreshService = () => {
  const credStore = useCredsStore();

  const refreshToken = async (): Promise<boolean> => {
    const credentials = credStore.asJsObject;
    if (!credentials) {
      console.error('No credentials found (cache error?)');
      return false;
    }

    try {
      const refreshedCredentials = await credentials.refresh();
      credStore.setCredentials(refreshedCredentials);
      return true;
    } catch (e) {
      console.error(`Refresh error: ${e}`);
      return false;
    }
  };

  const refreshTokenConditionally = async (): Promise<boolean> => {
    const credentials = credStore.asJsObject;
    if (!credentials) {
      throw new Error('No credentials found (cache error?)');
    }

    const remainingSec = credStore.tokenRemainingSeconds(Date.now());
    if (remainingSec === 0) {
      throw new Error("Token is expired and wasn't renewed. Logging off...");
    }

    if (!credentials.needsRefresh()) {
      return false;
    }

    return refreshToken();
  };

  const refreshTokenTask = async (): Promise<void> => {
    try {
      await refreshTokenConditionally();
    } catch (e) {
      console.info(`Couldn't refresh token: '${e}'. Purging credential cache.`);
      credStore.erase();
      await router.push({ path: '/login' });
    }
  };

  const triggerArlRefresh = async (): Promise<void> => {
    await useApi().post('/arl/refresh');
  };

  return { refreshToken, refreshTokenTask, triggerArlRefresh };
};
