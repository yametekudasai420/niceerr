import { AxiosError } from 'axios';
import { MAX_STRING_LENGTH_OVERFLOW } from './consts';

function formatByteCount(byteCount: number): string {
  const units = ['bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB'];
  let count = parseInt(byteCount.toString());
  let unitIndex = 0;

  while (count >= 1024 && unitIndex < units.length - 1) {
    count /= 1024;
    unitIndex++;
  }
  return `${count.toFixed(2)} ${units[unitIndex]}`;
}

function calculateProgressFormatted(soFar: number, total: number): string {
  // check whether soFar is greater than or equal than progress
  if (soFar >= total) {
    return '100';
  }
  const progress = (soFar / total || 1) * 100;
  return progress.toFixed(2);
}

function formatPersistenceMinuteCount(minutes: number): string {
  return formatSecondInterval(minutes * 60);
}

function parseJwt(token: string): number {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  const jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split('')
      .map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );
  const parsed = JSON.parse(jsonPayload);
  return parseInt(parsed.exp);
}

function formatSecondInterval(secondCount: number): string {
  const seconds = Math.floor(secondCount);
  const minutes = Math.floor(seconds / 60);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);
  const months = Math.floor(days / 30);
  const years = Math.floor(days / 365);
  const decades = Math.floor(years / 10);

  if (seconds < 60) {
    return `${seconds} second${seconds === 1 ? '' : 's'}`;
  } else if (minutes < 60) {
    return `${minutes} minute${minutes === 1 ? '' : 's'}`;
  } else if (hours < 24) {
    return `${hours} hour${hours === 1 ? '' : 's'} ${
      minutes % 60
        ? `${minutes % 60} minute${minutes % 60 === 1 ? '' : 's'} `
        : ''
    }`;
  } else if (days < 30) {
    return `${days} day${days === 1 ? '' : 's'} ${
      hours % 24 ? `${hours % 24} hour${hours % 24 === 1 ? '' : 's'} ` : ''
    }`;
  } else if (months < 12) {
    return `${months} month${months === 1 ? '' : 's'} ${
      days % 30 ? `${days % 30} day${days % 30 === 1 ? '' : 's'} ` : ''
    }`;
  } else if (years < 10) {
    return `${years} year${years === 1 ? '' : 's'} ${
      months % 12 ? `${months % 12} month${months % 12 === 1 ? '' : 's'} ` : ''
    }`;
  } else {
    if (decades > 10) {
      return `4-ever (${decades} decades)`;
    }
    return `${decades} decade${decades === 1 ? '' : 's'} ${
      years % 10 ? `${years % 10} year${years % 10 === 1 ? '' : 's'} ` : ''
    }`;
  }
}

function formatRelativeDate(utcDateString: string): string {
  const date = new Date(utcDateString);
  const currentDate = new Date(Date.now()).toUTCString(); // Get current date in UTC
  const currentDateUTC = new Date(currentDate);
  const timeDifference = Math.abs(currentDateUTC.getTime() - date.getTime());

  return formatSecondInterval(timeDifference / 1000);
}

function formatNullRelativeDate(
  utcDateString: string | null | undefined,
  suffix: string | undefined = undefined
): string {
  if (utcDateString === null || utcDateString === undefined) {
    return 'Never';
  }
  return formatRelativeDate(utcDateString) + (suffix ?? '');
}

interface NiceerrErrorResponse {
  error: string;
  status: number;
}

async function axiosErrorPrettyMessage(e: AxiosError): Promise<string> {
  if (e.response) {
    const contentType = e.response.headers['content-type'];

    // Check if the response is of blob type
    if (contentType && contentType.includes('application/json')) {
      try {
        // Attempt to parse the response as JSON
        const response = JSON.parse(
          await (e.response.data as Blob).text()
        ) as NiceerrErrorResponse;

        if (response.status && response.error) {
          return `${response.status}: ${response.error}`;
        }
      } catch (parseError) {
        try {
          const response = e.response.data as NiceerrErrorResponse;
          return `${response.status}: ${response.error}`;
        } catch (e) {
          console.error('Failed to parse JSON response:', parseError);
        }
      }
    } else {
      // If the response is not a blob or JSON
      const response = e.response.data as NiceerrErrorResponse;
      if (response.status && response.error) {
        return `${response.status}: ${response.error}`;
      }
    }
  }

  return e.message;
}

class DownloadQueue {
  private ids: Set<string>;

  constructor() {
    this.ids = new Set<string>();
  }

  /**
   * Adds an ID to the queue.
   * @param id - The ID to add.
   * @returns {boolean} - Returns true if the ID was added, false if it already existed.
   */
  add(id: string): boolean {
    if (this.ids.has(id)) {
      return false;
    } else {
      this.ids.add(id);
      return true;
    }
  }

  /**
   * Removes an ID from the queue.
   * @param id - The ID to remove.
   */
  remove(id: string): void {
    this.ids.delete(id);
  }
}

function shortenLongString(s: string): string {
  if (s.length > MAX_STRING_LENGTH_OVERFLOW) {
    return s.substring(0, MAX_STRING_LENGTH_OVERFLOW) + '...';
  }
  return s;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyObject = { [key: string]: any };

// Merge two objects into `target`
function mergeObjects(target: AnyObject, source: AnyObject): AnyObject {
  const merged: AnyObject = {};

  for (const key in source) {
    if (source.hasOwnProperty(key)) {
      if (
        source[key] !== null &&
        typeof source[key] === 'object' &&
        !Array.isArray(source[key])
      ) {
        merged[key] = mergeObjects(target[key] || {}, source[key]);
      } else if (source[key] !== null || target[key] === undefined) {
        merged[key] = source[key];
      } else {
        merged[key] = target[key];
      }
    }
  }

  for (const key in target) {
    if (!merged.hasOwnProperty(key)) {
      merged[key] = target[key];
    }
  }

  return merged;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function sortObjectsByKey<T extends Record<string, any>>(
  objects: T[],
  key: keyof T,
  referenceList: string[]
): T[] {
  const orderMap = new Map(referenceList.map((value, index) => [value, index]));
  return objects.sort(
    (a, b) =>
      (orderMap.get(a[key]) ?? Infinity) - (orderMap.get(b[key]) ?? Infinity)
  );
}

export {
  formatByteCount,
  parseJwt,
  formatRelativeDate,
  formatNullRelativeDate,
  formatPersistenceMinuteCount as formatMinuteCount,
  axiosErrorPrettyMessage,
  formatSecondInterval,
  calculateProgressFormatted,
  shortenLongString,
  mergeObjects,
  sortObjectsByKey,
  DownloadQueue,
};
