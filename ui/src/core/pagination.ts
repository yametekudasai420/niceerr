import { AxiosResponse } from 'axios';

interface Pagination {
  page: number;
  pageSize: number;
  search?: string;
  sortBy?: string;
  desc?: boolean;
}

function stringAddQueryParam(base: string, k: string, v: string): string {
  if (!base.startsWith('?')) {
    return `${base}?${k}=${v}&`;
  }
  if (base.endsWith('&')) {
    return `${base}${k}=${v}&`;
  }
  return `${base}&${k}=${v}&`;
}

class Pagination implements Pagination {
  constructor(
    page: number,
    pageSize: number,
    search?: string,
    sortBy?: string,
    desc?: boolean
  ) {
    this.page = page;
    this.pageSize = pageSize;
    this.desc = desc;
    this.sortBy = sortBy;
    this.search = search;
  }

  asQueryParams(): string {
    let params = '';
    if (this.page) {
      params = stringAddQueryParam(params, 'page', this.page.toString());
    }
    if (this.pageSize) {
      params = stringAddQueryParam(
        params,
        'pageSize',
        this.pageSize.toString()
      );
    }
    if (this.sortBy) {
      params = stringAddQueryParam(params, 'sortBy', this.sortBy);
    }
    if (this.search && this.search.trim() != '') {
      params = stringAddQueryParam(params, 'search', this.search);
    }
    if (this.desc) {
      params = stringAddQueryParam(params, 'desc', this.desc.toString());
    }
    return params;
  }
}

interface PaginatedResponse<T> {
  totalItems: number;
  totalPages: number;
  data: T[];
}

class PaginatedResponse<T> implements PaginatedResponse<T> {
  constructor(response: AxiosResponse) {
    this.data = response.data;
    this.totalItems = parseInt(response.headers['x-total-items']);
    this.totalPages = parseInt(response.headers['x-total-pages']);
  }
}

export { Pagination, PaginatedResponse };
