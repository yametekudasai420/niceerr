import { defineStore } from 'pinia';
import { Credentials, Permission, User } from 'src/core/user';

const useCredsStore = defineStore('userCreds', {
  state: () => ({
    credentials: null as Credentials | null,
  }),

  getters: {
    canChangeStreamSettings(): boolean {
      return !this.canServerSideOnly && !this.canStreamOnly;
    },

    canDownload(): boolean {
      const user = this.getUser;
      if (!user) {
        return false;
      }

      return (
        user.hasPermission(Permission.PerformStreamDownload) ||
        user.hasPermission(Permission.PerformServerSideDownload)
      );
    },

    canServerSideOnly(): boolean {
      const user = this.getUser;
      if (!user) {
        return false;
      }

      if (user.superuser) {
        return false;
      }

      return (
        user.hasPermission(Permission.PerformServerSideDownload) &&
        !user.hasPermission(Permission.PerformStreamDownload)
      );
    },

    canStreamOnly(): boolean {
      const user = this.getUser;
      if (!user) {
        return false;
      }

      if (user.superuser) {
        return false;
      }

      return (
        !user.hasPermission(Permission.PerformServerSideDownload) &&
        user.hasPermission(Permission.PerformStreamDownload)
      );
    },

    getUser(state): User | null {
      const storeUser = state.credentials?.user ?? null;
      if (!storeUser) {
        return null;
      }

      return new User(storeUser);
    },

    getJwt(state): string | null {
      return state.credentials?.jwt ?? null;
    },

    tokenRemainingSeconds() {
      // Offload calculation here.
      const calculate = (date: number): number => {
        const { credentials } = this;
        if (!credentials) return 0;
        const now = date / 1000;
        return Math.max(0, Math.floor(credentials.expiresAt - now));
      };
      return calculate;
    },

    isStoredUserValid(state): boolean {
      const cbk = this.tokenRemainingSeconds;
      return state.credentials !== null && cbk(Date.now()) > 0;
    },

    asJsObject(state): Credentials | null {
      if (state.credentials === null) return null;
      const ret = new Credentials(
        state.credentials.jwt,
        state.credentials.user,
        state.credentials.lastUpdatedAt
      );
      return ret;
    },
  },

  actions: {
    setCredentials(creds: Credentials) {
      this.credentials = creds;
    },

    erase() {
      this.credentials = null;
    },
  },
  persist: true,
});

export default useCredsStore;
