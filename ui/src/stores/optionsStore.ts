import { defineStore } from 'pinia';
import { StreamFormat } from 'src/core/download';
import useCredsStore from './userCreds';
import { mergeObjects } from 'src/core/util';

type SkippedContent = 'ALBUM' | 'COMPILATION' | 'SINGLE' | 'LIVE' | 'EP';

type PreferredInterface = 'desktop' | 'mobile' | 'auto';

const DEFAULT_DOWNLOAD_HISTORY_COLPREFS: ColumnPref[] = [
  {
    name: 'title',
    desc: 'Kind/Title',
    enabled: true,
  },
  {
    name: 'downloadQuality',
    desc: 'Quality',
    enabled: true,
  },
  {
    name: 'outcome',
    desc: 'Outcome',
    enabled: true,
  },
  {
    name: 'details',
    desc: 'Details',
    enabled: true,
  },
  {
    name: 'downloadStats',
    desc: 'Stats',
    enabled: true,
  },
  {
    name: 'createdAt',
    desc: 'Created',
    enabled: true,
  },
  {
    name: 'downloadedBy',
    desc: 'Downloaded by',
    enabled: true,
  },
  {
    name: 'id',
    desc: 'ID',
    enabled: false,
  },
  {
    name: 'actions',
    desc: 'Actions',
    enabled: true,
  },
];

const DEFAULT_USER_MANAGE_COLPREFS = [
  {
    name: 'username',
    desc: 'Username',
    enabled: true,
  },
  {
    name: 'createdAt',
    desc: 'Created',
    enabled: true,
  },
  {
    name: 'level',
    desc: 'User level',
    enabled: true,
  },
  {
    name: 'enabled',
    desc: 'Enabled',
    enabled: true,
  },
  {
    name: 'permissions',
    desc: 'Permissions',
    enabled: true,
  },
  {
    name: 'downloadPath',
    desc: 'Server-side download path',
    enabled: true,
  },
  {
    name: 'downloadSessionPersistMinutes',
    desc: 'Keep downloads for...',
    enabled: true,
  },
  {
    name: 'maxBandwidthBytesPerDay',
    desc: 'Daily bandwidth quota',
    enabled: true,
  },
  {
    name: 'downloadedBytes',
    desc: 'Bytes downloaded',
    enabled: true,
  },
  {
    name: 'id',
    desc: 'User ID (click to change password)',
    enabled: true,
  },
  {
    name: 'actions',
    desc: 'Actions',
    enabled: true,
  },
];

interface ColumnPref {
  name: string;
  desc: string;
  enabled: boolean;
}

interface ColumnPrefs {
  downloadHistory: ColumnPref[];
  userManage: ColumnPref[];
}

interface Store {
  options: Options;
}

interface Options {
  v: number;
  enableWildcardSearch: boolean;
  useIsoDates: boolean;
  // Download options
  allowFallback: boolean;
  includeCredited: boolean;
  ignoreErrors: boolean;
  streamDefault: boolean;
  streamFormat: StreamFormat;
  // Show verbose user ids and download IDs
  showVerboseIds: boolean;
  hideUnsupportedPlatformWarnings: boolean;
  interface: PreferredInterface;
  interfaceBlur: boolean;
  interfaceFullBlack: boolean;
  columnPrefs: ColumnPrefs;
  skippedContent: SkippedContent[];
}

const DEFAULTS: Options = {
  v: 6,
  enableWildcardSearch: true,
  useIsoDates: false,
  allowFallback: true,
  includeCredited: false,
  ignoreErrors: false,
  streamDefault: false,
  streamFormat: 'Flac' as StreamFormat,
  showVerboseIds: false,
  hideUnsupportedPlatformWarnings: false,
  interface: 'auto' as PreferredInterface,
  interfaceBlur: true,
  interfaceFullBlack: false,
  columnPrefs: {
    downloadHistory: DEFAULT_DOWNLOAD_HISTORY_COLPREFS,
    userManage: DEFAULT_USER_MANAGE_COLPREFS,
  } as ColumnPrefs,
  skippedContent: [] as SkippedContent[],
};

/// Avoid mutating the default defaults dict
function cloneDefaults(): Options {
  return JSON.parse(JSON.stringify(DEFAULTS)) as Options;
}

const useOptionsStore = defineStore('globalOptions', {
  state: () => ({
    options: cloneDefaults(),
  }),

  getters: {
    getStore(state: Store): Options {
      return state.options;
    },

    /// Get the default background style
    defaultBg(state: Store): string {
      if (state.options.interfaceBlur === false) {
        return 'bg-black';
      }
      return 'blur-background';
    },

    downloadHistoryCols(state: Store): string[] {
      return state.options.columnPrefs.downloadHistory
        .filter((c) => c.enabled)
        .map((c) => c.name);
    },

    userManageCols(state: Store): string[] {
      return state.options.columnPrefs.userManage
        .filter((c) => c.enabled)
        .map((c) => c.name);
    },

    fullBlack(state: Store): string {
      if (state.options?.interfaceFullBlack === true) {
        return 'bg-black';
      }
      return '';
    },
  },

  actions: {
    set(v: Options) {
      const credStore = useCredsStore();
      // Adjust the options based on the user's credentials
      if (credStore.canStreamOnly) {
        v.streamDefault = true;
      } else if (credStore.canServerSideOnly) {
        v.streamDefault = false;
      }

      // Merge the new options into the existing state to preserve reactivity

      if (this.options) {
        Object.assign(this.options, v);
      } else {
        this.options = { ...v };
      }
    },

    resetDefaults() {
      this.set(cloneDefaults());
    },

    getOrSetDefault(): Options {
      const current = this.$state.options;
      // Merge any missing options in the user's outdated prefs
      if (current.v !== DEFAULTS.v) {
        console.info(
          `UI upgrade: triggering merge (curr=${current.v}, new=${DEFAULTS.v})`
        );
        const mergedState = mergeObjects(
          JSON.parse(JSON.stringify(this.$state)),
          DEFAULTS
        ) as Options;
        mergedState.v = DEFAULTS.v;
        this.set(mergedState as Options);
      }

      const credStore = useCredsStore();
      if (credStore.canStreamOnly) {
        current.streamDefault = true;
      } else if (credStore.canServerSideOnly) {
        current.streamDefault = false;
      }
      return current;
    },
  },

  persist: true,
});

export default useOptionsStore;
export type { Options, ColumnPref, SkippedContent };
