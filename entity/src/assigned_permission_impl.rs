use log::info;
use niceerr_db::DBConfig;
use sea_orm::{ColumnTrait, EntityTrait, QueryFilter};

use crate::{
    assigned_permission::{self, Permission, PermissionScope},
    error::EntityResult,
    user,
};

impl assigned_permission::Model {
    pub fn new_without_id(perm: Permission, scope: PermissionScope) -> Self {
        Self {
            user_id: Default::default(),
            name: perm,
            scope,
        }
    }

    pub async fn delete_all_permissions_for(user: &user::Model) -> EntityResult<u64> {
        info!("deleting perms for {}: {}", user.id, user.username);
        Ok(assigned_permission::Entity::delete_many()
            .filter(assigned_permission::Column::UserId.eq(user.id))
            .exec(DBConfig::get_connection())
            .await?
            .rows_affected)
    }

    pub async fn assign_multiple_perms(
        perms: Vec<assigned_permission::ActiveModel>,
    ) -> EntityResult<()> {
        if perms.is_empty() {
            return Ok(());
        }
        assigned_permission::Entity::insert_many(perms)
            .exec(DBConfig::get_connection())
            .await?;
        Ok(())
    }
}
