use chrono::Utc;
use futures::{Stream, TryStreamExt};
use sea_orm::{
    sea_query::{Alias, Expr},
    ActiveModelTrait, ColumnTrait, EntityTrait, IntoActiveModel, QueryFilter, QuerySelect,
    QueryTrait, Set,
};
use uuid::Uuid;

use crate::{
    assigned_permission::Permission,
    download_session::{
        Column as DownloadSessionColumn, DownloadKind, DownloadQuality, DownloadSessionOptions,
        DownloadSessionWSongInfo, Entity as DownloadSessionEntity, Model as DownloadSession,
        OutcomeKind, SearchFilterDlSession, SkippedContent,
    },
    error::{EntityError, EntityResult},
    pagination::PaginationParams,
    song::{Column as SongColumn, Entity as SongEntity, Model as Song},
    traits::{selector_into_paginated, SortFilterParams, SortableFilterable},
    user::{Column as UserColumn, Entity as UserEntity, Model as User},
};

impl DownloadSession {
    /// Find all download sessions that haven't been pruned and
    /// that weren't streamed (those don't save data to disk).
    pub async fn find_on_storage(id: Uuid) -> EntityResult<Option<Self>> {
        let db = niceerr_db::DBConfig::get_connection();
        Ok(DownloadSessionEntity::find()
            .filter(DownloadSessionColumn::Id.eq(id))
            .filter(DownloadSessionColumn::IsPruned.eq(false))
            .filter(DownloadSessionColumn::IsStream.eq(false))
            .one(db)
            .await?)
    }

    pub fn skip_live(&self) -> bool {
        self.skipped_content.contains(&SkippedContent::Live)
    }

    pub fn skip_ep(&self) -> bool {
        self.skipped_content.contains(&SkippedContent::Ep)
    }

    pub fn skip_single(&self) -> bool {
        self.skipped_content.contains(&SkippedContent::Single)
    }

    pub fn skip_album(&self) -> bool {
        self.skipped_content.contains(&SkippedContent::Album)
    }

    pub fn skip_compilation(&self) -> bool {
        self.skipped_content.contains(&SkippedContent::Compilation)
    }

    pub async fn new_ongoing<S: AsRef<str>>(
        title: &str,
        caller: &User,
        download_quality: DownloadQuality,
        download_kind: DownloadKind,
        download_id: S,
        options: DownloadSessionOptions,
    ) -> EntityResult<Self> {
        let model = DownloadSession {
            title: title.to_string(),
            download_quality,
            download_kind,
            download_id: download_id.as_ref().to_string(),
            id: Uuid::new_v4(),
            downloaded_by: caller.id,
            outcome: Default::default(),
            created_at: Utc::now(),
            is_pruned: false,
            include_credited: options.include_credited,
            allow_fallback: options.allow_fallback,
            is_stream: options.is_stream,
            skipped_content: options.skipped_content.into(),
        };
        let db = niceerr_db::DBConfig::get_connection();
        Ok(model.into_active_model().insert(db).await?)
    }

    pub async fn new_ongoing_playlist<S: AsRef<str>>(
        title: &str,
        caller: &User,
        download_quality: DownloadQuality,
        download_id: S,
        options: DownloadSessionOptions,
    ) -> EntityResult<Self> {
        Self::new_ongoing(
            title,
            caller,
            download_quality,
            DownloadKind::Playlist,
            download_id,
            options,
        )
        .await
    }

    pub async fn new_ongoing_artist<S: AsRef<str>>(
        title: &str,
        caller: &User,
        download_quality: DownloadQuality,
        download_id: S,
        options: DownloadSessionOptions,
    ) -> EntityResult<Self> {
        Self::new_ongoing(
            title,
            caller,
            download_quality,
            DownloadKind::Artist,
            download_id,
            options,
        )
        .await
    }

    pub async fn new_ongoing_album<S: AsRef<str>>(
        title: &str,
        caller: &User,
        download_quality: DownloadQuality,
        download_id: S,
        options: DownloadSessionOptions,
    ) -> EntityResult<Self> {
        Self::new_ongoing(
            title,
            caller,
            download_quality,
            DownloadKind::Album,
            download_id,
            options,
        )
        .await
    }

    pub async fn new_ongoing_song<S: AsRef<str>>(
        title: &str,
        caller: &User,
        download_quality: DownloadQuality,
        download_id: S,
        options: DownloadSessionOptions,
    ) -> EntityResult<Self> {
        Self::new_ongoing(
            title,
            caller,
            download_quality,
            DownloadKind::Song,
            download_id,
            options,
        )
        .await
    }

    /// Songs associated to this download session as a stream.
    pub async fn songs_as_stream(&self) -> EntityResult<impl Stream<Item = EntityResult<Song>>> {
        let db = niceerr_db::DBConfig::get_connection();

        let ret = SongEntity::find()
            .filter(SongColumn::TotalBytes.gt(0))
            .filter(SongColumn::SessionId.eq(self.id))
            .stream(db)
            .await?
            .map_err(EntityError::from);

        Ok(ret)
    }

    pub async fn update_status(&self, outcome: OutcomeKind) -> EntityResult<()> {
        let mut sself = self.clone().into_active_model();
        sself.outcome = Set(outcome);
        sself.update(niceerr_db::DBConfig::get_connection()).await?;
        Ok(())
    }

    pub async fn update_progress_to_error() -> EntityResult<u64> {
        let db = niceerr_db::DBConfig::get_connection();
        let updated = DownloadSessionEntity::update_many()
            .col_expr(
                DownloadSessionColumn::Outcome,
                Expr::value(OutcomeKind::Error),
            )
            .filter(DownloadSessionColumn::Outcome.eq(OutcomeKind::InProgress))
            .exec(db)
            .await?;
        Ok(updated.rows_affected)
    }

    pub async fn mark_pruned(&self) -> EntityResult<()> {
        let mut sself = self.clone().into_active_model();
        sself.is_pruned = Set(true);
        sself.update(niceerr_db::DBConfig::get_connection()).await?;
        Ok(())
    }

    pub async fn list_paginated(
        caller: &User,
        params: &PaginationParams,
        filter: SearchFilterDlSession,
    ) -> EntityResult<(Vec<DownloadSessionWSongInfo>, u64, u64)> {
        let mut query = filter.query();
        if !caller.is_god() {
            let visible_users = caller
                .visible_users_select(&Permission::ReadDownloadSession)?
                .select_only()
                .column(UserColumn::Id)
                .into_query();
            query = query.filter(DownloadSessionColumn::DownloadedBy.in_subquery(visible_users));
        }
        // This will inject quite a bunch of important data such as total bytes, downloaded bytes, etc.

        query = query.column_as(
            Expr::expr(Expr::col(SongColumn::TotalBytes).sum()).if_null(0),
            "total_bytes_sum",
        );
        query = query.column_as(
            Expr::expr(Expr::col(SongColumn::DownloadedBytes).sum()).if_null(0),
            "downloaded_bytes_sum",
        );
        query = query.column_as(
            Expr::expr(Expr::col(SongColumn::Id.as_column_ref()).count()).if_null(0),
            "song_count",
        );
        query = query.column_as(SongColumn::ErrorMessage.count(), "error_count");
        let (alias, username_col) = (Alias::new("user_alias"), Expr::cust("user_alias.username"));
        query = query.column_as(username_col.clone(), "username");
        query = query.join_rev(
            sea_orm::JoinType::LeftJoin,
            SongEntity::belongs_to(DownloadSessionEntity)
                .from(SongColumn::SessionId)
                .to(DownloadSessionColumn::Id)
                .into(),
        );
        query = query.join_as(
            sea_orm::JoinType::LeftJoin,
            DownloadSessionEntity::belongs_to(UserEntity)
                .from(DownloadSessionColumn::DownloadedBy)
                .to(UserColumn::Id)
                .into(),
            alias,
        );
        query = query
            .group_by(DownloadSessionColumn::Id)
            .group_by(username_col);
        let selector = query.into_model::<DownloadSessionWSongInfo>();
        selector_into_paginated(selector, params).await
    }

    pub async fn update_tile(self, title: String) -> EntityResult<()> {
        let mut sself = self.into_active_model();
        sself.title = Set(title);
        sself.update(niceerr_db::DBConfig::get_connection()).await?;
        Ok(())
    }

    pub async fn migration_get_missing_title_stream(
    ) -> EntityResult<impl Stream<Item = EntityResult<Self>>> {
        let db = niceerr_db::DBConfig::get_connection();
        let items = DownloadSessionEntity::find()
            .filter(DownloadSessionColumn::Title.eq(""))
            .stream(db)
            .await?
            .map_err(EntityError::from);
        Ok(items)
    }
}

impl SortFilterParams for SearchFilterDlSession {
    type Entity = DownloadSessionEntity;
    type Column = DownloadSessionColumn;

    fn sortable_columns() -> &'static [Self::Column] {
        &[
            DownloadSessionColumn::CreatedAt,
            DownloadSessionColumn::Title,
            DownloadSessionColumn::DownloadedBy,
            DownloadSessionColumn::DownloadId,
            DownloadSessionColumn::Outcome,
            DownloadSessionColumn::DownloadKind,
            DownloadSessionColumn::DownloadQuality,
        ]
    }

    fn filterable_columns() -> &'static [Self::Column] {
        // It makes sense to allow filtering for the same set
        // of sortable columns
        Self::sortable_columns()
    }

    fn get_search_value(self) -> Vec<impl sea_orm::sea_query::IntoLikeExpr + Clone> {
        self.search
    }

    fn get_sort_value(&self) -> Option<&String> {
        self.sort_by.as_ref()
    }

    fn default_sort_column() -> Self::Column {
        DownloadSessionColumn::CreatedAt
    }

    fn sort_by_desc(&self) -> bool {
        self.desc.unwrap_or(false)
    }
}
