use better_debug::BetterDebug;
use chrono::{DateTime, Utc};
use sea_orm::entity::prelude::*;
use sea_orm::DeriveEntityModel;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::ty::NonBlankString;

#[derive(Debug, Clone, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct UpdatableArl {
    pub alias: Option<NonBlankString>,
    pub priority: Option<u32>,
    pub token: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct SearchFilterArl {
    #[serde(default)]
    pub search: Vec<String>,
    pub sort_by: Option<String>,
    pub desc: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ArlPayload {
    pub alias: NonBlankString,
    pub token: String,
}

#[derive(BetterDebug, Clone, Serialize, Deserialize, PartialEq, Eq, DeriveEntityModel)]
#[sea_orm(table_name = "deeznuts_arl")]
#[serde(rename_all = "camelCase")]
pub struct Model {
    // Don't allow the user to change ID
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing, default = "Uuid::new_v4")]
    pub id: Uuid,
    pub alias: NonBlankString,
    #[serde(skip_deserializing, default = "Utc::now")]
    pub created_at: DateTime<Utc>,
    #[serde(skip_deserializing)]
    pub expires_at: DateTime<Utc>,
    #[serde(skip_deserializing)]
    pub created_by: Uuid,
    #[serde(skip_serializing)]
    #[better_debug(ignore)]
    pub token: String,
    pub priority: i32,
    pub country: String,
}

impl ActiveModelBehavior for ActiveModel {}

#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Relation {
    User,
}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        match self {
            Self::User => Entity::belongs_to(super::user::Entity)
                .from(Column::CreatedBy)
                .to(super::user::Column::Id)
                .into(),
        }
    }
}

impl Related<super::user::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::User.def()
    }
}
