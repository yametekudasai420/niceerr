/// Nice wrappers for string types and consistent updates.
///
/// These types ensure that the string is not empty/blank/malformed (for paths).
use std::ops::Deref;

use niceerr_config::sanitize;
use sea_orm::{sea_query::Nullable, DeriveValueType};
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
#[serde(tag = "type", content = "value", rename = "camelCase")]
pub enum FieldUpdate<T> {
    #[default]
    Unchanged,
    Set(T),
    Unset,
}

impl<T> FieldUpdate<T> {
    pub fn is_change(&self) -> bool {
        matches!(self, FieldUpdate::Set(_))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, DeriveValueType)]
pub struct NonBlankString(String);

#[derive(Clone, Debug, PartialEq, Eq, DeriveValueType)]
pub struct NonEmptyString(String);

#[derive(Clone, Debug, PartialEq, Eq, DeriveValueType)]
pub struct PathSanitizedString(String);

impl Deref for NonBlankString {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Deref for NonEmptyString {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Deref for PathSanitizedString {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl NonEmptyString {
    pub fn new_unchecked(s: String) -> Self {
        Self(s)
    }
}

impl PathSanitizedString {
    pub fn is_empty(&self) -> bool {
        self.0.trim().is_empty()
    }
}

impl std::fmt::Display for NonBlankString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl std::fmt::Display for NonEmptyString {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl Serialize for NonBlankString {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0)
    }
}

impl Serialize for NonEmptyString {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0)
    }
}

impl Serialize for PathSanitizedString {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0)
    }
}

impl<'de> Deserialize<'de> for NonBlankString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        if s.is_empty() || s.trim().is_empty() {
            return Err(de::Error::custom("string can't be blank"));
        }
        Ok(NonBlankString(s))
    }
}

impl<'de> Deserialize<'de> for NonEmptyString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        if s.is_empty() {
            return Err(de::Error::custom("string can't be empty"));
        }
        Ok(NonEmptyString(s))
    }
}

impl<'de> Deserialize<'de> for PathSanitizedString {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = Deserialize::deserialize(deserializer)?;
        let trimmed = s.trim();
        if trimmed.is_empty() {
            return Err(de::Error::custom("string can't be empty"));
        }
        if trimmed.starts_with(".") || trimmed.starts_with("..") {
            return Err(de::Error::custom("string can't start with dots"));
        }
        if s.len() > 255 {
            return Err(de::Error::custom(
                "string can't be longer than 255 characters",
            ));
        }
        if sanitize(&s) != s {
            return Err(de::Error::custom("string contains invalid characters"));
        }

        Ok(PathSanitizedString(s))
    }
}

impl Nullable for PathSanitizedString {
    fn null() -> sea_orm::Value {
        sea_orm::Value::String(None)
    }
}
