use std::collections::HashSet;
use std::ops::Deref;

use chrono::{DateTime, Utc};
use sea_orm::{entity::prelude::*, FromQueryResult};
use sea_orm::{DeriveEntityModel, FromJsonQueryResult};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub struct DownloadSessionOptions {
    pub include_credited: bool,
    pub allow_fallback: bool,
    pub is_stream: bool,
    pub skipped_content: SkippedContentSet,
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct SearchFilterDlSession {
    #[serde(default)]
    pub search: Vec<String>,
    pub sort_by: Option<String>,
    pub desc: Option<bool>,
}

#[derive(
    EnumIter,
    DeriveActiveEnum,
    Eq,
    PartialEq,
    Deserialize,
    Serialize,
    Debug,
    Clone,
    DeriveDisplay,
    PartialOrd,
    Ord,
    Hash,
)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
#[serde(rename_all = "UPPERCASE")]
pub enum SkippedContent {
    #[sea_orm(string_value = "ALBUM")]
    Album,
    #[sea_orm(string_value = "COMPILATION")]
    Compilation,
    #[sea_orm(string_value = "SINGLE")]
    Single,
    #[sea_orm(string_value = "EP")]
    Ep,
    #[sea_orm(string_value = "LIVE")]
    Live,
}

pub type SkippedContentSet = HashSet<SkippedContent>;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, FromJsonQueryResult, Default)]
pub struct SkippedContentJson(pub SkippedContentSet);

impl Into<SkippedContentJson> for SkippedContentSet {
    fn into(self) -> SkippedContentJson {
        SkippedContentJson(self)
    }
}

impl Deref for SkippedContentJson {
    type Target = HashSet<SkippedContent>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(
    EnumIter, DeriveActiveEnum, Eq, PartialEq, Deserialize, Serialize, Debug, Clone, DeriveDisplay,
)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
pub enum DownloadKind {
    #[sea_orm(string_value = "PLAYLIST")]
    Playlist,
    #[sea_orm(string_value = "ARTIST")]
    Artist,
    #[sea_orm(string_value = "ALBUM")]
    Album,
    #[sea_orm(string_value = "SONG")]
    Song,
}

#[derive(EnumIter, DeriveActiveEnum, Eq, PartialEq, Deserialize, Serialize, Debug, Clone, Copy)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
pub enum DownloadQuality {
    #[sea_orm(string_value = "FLAC")]
    Flac,
    #[sea_orm(string_value = "MP3_320")]
    Mp3_320,
    #[sea_orm(string_value = "MP3_128")]
    Mp3_128,
}

#[derive(EnumIter, DeriveActiveEnum, Eq, PartialEq, Deserialize, Serialize, Debug, Clone, Copy)]
#[sea_orm(rs_type = "String", db_type = "String(StringLen::None)")]
pub enum OutcomeKind {
    #[sea_orm(string_value = "SUCCESS")]
    Success,
    #[sea_orm(string_value = "PARTIALLY_DOWNLOADED")]
    PartiallyDownloaded,
    #[sea_orm(string_value = "STOPPED")]
    Stopped,
    #[sea_orm(string_value = "IN_PROGRESS")]
    InProgress,
    #[sea_orm(string_value = "ERROR")]
    Error,
}

impl Default for OutcomeKind {
    fn default() -> Self {
        Self::InProgress
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, DeriveEntityModel)]
#[sea_orm(table_name = "download_session")]
#[serde(rename_all = "camelCase")]
pub struct Model {
    // Don't allow the user to change ID
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing, default = "Uuid::new_v4")]
    pub id: Uuid,
    pub title: String,
    pub download_id: String,
    pub download_quality: DownloadQuality,
    pub download_kind: DownloadKind,
    pub downloaded_by: Uuid,
    pub outcome: OutcomeKind,
    pub created_at: DateTime<Utc>,
    pub is_pruned: bool,
    pub is_stream: bool,
    pub include_credited: bool,
    pub allow_fallback: bool,
    pub skipped_content: SkippedContentJson,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, FromQueryResult)]
#[serde(rename_all = "camelCase")]
pub struct DownloadSessionWSongInfo {
    // Don't allow the user to change ID
    #[serde(skip_deserializing, default = "Uuid::new_v4")]
    pub id: Uuid,
    pub title: String,
    pub download_id: String,
    pub download_quality: DownloadQuality,
    pub download_kind: DownloadKind,
    pub downloaded_by: Uuid,
    pub outcome: OutcomeKind,
    pub created_at: DateTime<Utc>,
    #[serde(default = "default_i64")]
    pub total_bytes_sum: Option<i64>,
    #[serde(default = "default_i64")]
    pub downloaded_bytes_sum: Option<i64>,
    #[serde(default = "default_i64")]
    pub song_count: Option<i64>,
    #[serde(default = "default_i64")]
    pub error_count: Option<i64>,
    pub username: String,
    pub is_pruned: bool,
    pub is_stream: bool,
    pub include_credited: bool,
    pub allow_fallback: bool,
    pub skipped_content: SkippedContentJson,
}

fn default_i64() -> Option<i64> {
    Some(0)
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {}

impl ActiveModelBehavior for ActiveModel {}
