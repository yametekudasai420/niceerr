use sea_orm::entity::prelude::*;
use sea_orm::DeriveEntityModel;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct SearchFilterSong {
    #[serde(default)]
    pub search: Vec<String>,
    pub sort_by: Option<String>,
    pub desc: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, DeriveEntityModel, Hash)]
#[sea_orm(table_name = "downloaded_song")]
#[serde(rename_all = "camelCase")]
pub struct Model {
    // Don't allow the user to change ID
    #[sea_orm(primary_key)]
    #[serde(skip_deserializing, default = "Uuid::new_v4")]
    pub id: Uuid,
    pub session_id: Uuid,
    pub item_id: i64,
    pub total_bytes: i32,
    pub downloaded_bytes: i32,
    pub download_path: String,
    pub error_message: Option<String>,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {}

impl ActiveModelBehavior for ActiveModel {}
