use chrono::{DateTime, Utc};
use futures_util::{Stream, TryStreamExt};
use log::warn;
use sea_orm::ActiveValue::NotSet;
use sea_orm::{
    ActiveModelTrait, ColumnTrait, ConnectionTrait, EntityTrait, IntoActiveModel, ModelTrait,
    QueryFilter, QueryOrder, QuerySelect, QueryTrait, Set, Statement,
};
use uuid::Uuid;

use crate::arl::{
    ActiveModel as ActiveArl, ArlPayload, Column as ArlColumn, Entity as ArlEntity, Model as Arl,
    SearchFilterArl, UpdatableArl,
};
use crate::assigned_permission::Permission;
use crate::error::{EntityError, EntityResult};
use crate::pagination::PaginationParams;
use crate::traits::{SelectIntoPaginated, SortFilterParams, SortableFilterable};
use crate::user::{Column as UserColumn, Model as User};

impl UpdatableArl {
    pub fn new_priority_change(prio: i32) -> Self {
        UpdatableArl {
            priority: Some(prio as u32),
            ..Default::default()
        }
    }
}

impl Arl {
    /// Reset the priority autoincrement counter. This prevents the database's counter
    /// from going up uncontrollably.
    #[inline]
    pub async fn reset_priority_index() -> EntityResult<()> {
        let db = niceerr_db::DBConfig::get_connection();
        // TODO: Find out if the ORM has the means to nicely pass this as a prepared statement
        let (table, column) = ArlColumn::Priority.as_column_ref();
        let (table, column) = (table.to_string(), column.to_string());
        let sql = format!(
            r#"
SELECT setval(
    pg_get_serial_sequence('{table}', '{column}'), COALESCE(MAX({column}), 1), true
) FROM {table}"#
        );
        let stmt = Statement::from_string(sea_orm::DatabaseBackend::Postgres, sql);
        db.execute(stmt).await?;
        Ok(())
    }

    pub async fn sorted_by_countries(
        countries: Vec<String>,
    ) -> EntityResult<impl Stream<Item = EntityResult<Arl>>> {
        if countries.is_empty() {
            warn!("Can't use this function with no countries");
            return Err(EntityError::InvalidData);
        }
        let db = niceerr_db::DBConfig::get_connection();
        let countries = countries.into_iter().map(Into::into).collect::<Vec<_>>();
        Ok(ArlEntity::find()
            .filter(ArlColumn::Country.is_in(countries.clone()))
            .order_by(
                ArlColumn::Country,
                sea_orm::Order::Field(sea_orm::Values(countries)),
            )
            .order_by_asc(ArlColumn::Priority)
            .order_by_asc(ArlColumn::Id)
            .stream(db)
            .await?
            .map_err(EntityError::from))
    }

    pub async fn lowest_priority() -> EntityResult<Option<Self>> {
        let db = niceerr_db::DBConfig::get_connection();
        Ok(ArlEntity::find()
            // Note that 0 is a higher priority, and anything beyond that is lower
            .order_by_desc(ArlColumn::Priority)
            .one(db)
            .await?)
    }

    pub async fn unique_countries() -> EntityResult<Vec<String>> {
        let db = niceerr_db::DBConfig::get_connection();
        let countries = ArlEntity::find()
            .distinct_on([ArlColumn::Country])
            .order_by_asc(ArlColumn::Country)
            .all(db)
            .await?;
        Ok(countries.into_iter().map(|it| it.country).collect())
    }

    pub async fn new(
        caller: &User,
        arl_payload: ArlPayload,
        expires_at: &DateTime<Utc>,
        country: String,
    ) -> EntityResult<Arl> {
        let model = ActiveArl {
            id: Set(Uuid::new_v4()),
            alias: Set(arl_payload.alias),
            token: Set(arl_payload.token),
            created_at: Set(Utc::now()),
            expires_at: Set(*expires_at),
            created_by: Set(caller.id),
            priority: NotSet,
            country: Set(country),
        }
        .into_active_model();
        let db = niceerr_db::DBConfig::get_connection();
        Ok(model.insert(db).await?)
    }

    pub async fn update_info(
        self,
        expires_at: &DateTime<Utc>,
        country: &str,
    ) -> EntityResult<Self> {
        let mut model = self.into_active_model();
        model.expires_at = Set(*expires_at);
        model.country = Set(country.to_string());
        let db = niceerr_db::DBConfig::get_connection();
        Ok(model.update(db).await?)
    }

    pub async fn update(
        self,
        other: UpdatableArl,
        expires_at: Option<DateTime<Utc>>,
        country: Option<String>,
    ) -> EntityResult<Self> {
        let mut model = self.into_active_model();
        model.alias = other.alias.map_or(NotSet, Set);
        model.token = other.token.map_or(NotSet, Set);
        model.priority = other.priority.map_or(NotSet, |v| Set(v as i32));
        model.expires_at = expires_at.map_or(NotSet, Set);
        model.country = country.map_or(NotSet, Set);
        let db = niceerr_db::DBConfig::get_connection();
        Ok(model.update(db).await?)
    }

    pub async fn list_paginated(
        caller: &User,
        params: &PaginationParams,
        filter: SearchFilterArl,
    ) -> EntityResult<(Vec<Self>, u64, u64)> {
        let mut query = filter.query();
        if caller.level > 0 {
            // sub-superusers can only see either their own tokens, or subordinate sub-user's
            // tokens.
            let visible_users = caller
                .visible_users_select(&Permission::ReadArl)?
                .select_only()
                .column(UserColumn::Id)
                .as_query()
                .to_owned();
            query = query.filter(ArlColumn::CreatedBy.in_subquery(visible_users));
        }
        query.into_paginated(params).await
    }

    pub async fn get_country_unexpired_tokens_stream<I>(
        values: I,
    ) -> EntityResult<impl Stream<Item = EntityResult<Arl>>>
    where
        I: IntoIterator<Item = String>,
    {
        let db = niceerr_db::DBConfig::get_connection();
        let arls = ArlEntity::find()
            .order_by_asc(ArlColumn::Priority)
            .filter(ArlColumn::ExpiresAt.gt(Utc::now()))
            .filter(ArlColumn::Country.is_in(values))
            .stream(db)
            .await?
            .map_err(EntityError::from);
        Ok(arls)
    }

    pub async fn get_all_stream() -> EntityResult<impl Stream<Item = EntityResult<Arl>>> {
        let db = niceerr_db::DBConfig::get_connection();
        let arls = ArlEntity::find()
            .order_by_asc(ArlColumn::Priority)
            .stream(db)
            .await?
            .map_err(EntityError::from);
        Ok(arls)
    }

    pub async fn get_unexpired_tokens_stream() -> EntityResult<impl Stream<Item = EntityResult<Arl>>>
    {
        let db = niceerr_db::DBConfig::get_connection();
        let arls = ArlEntity::find()
            .order_by_asc(ArlColumn::Priority)
            .filter(ArlColumn::ExpiresAt.gt(Utc::now()))
            .stream(db)
            .await?
            .map_err(EntityError::from);
        Ok(arls)
    }

    pub async fn find_by_priority(priority: i32) -> EntityResult<Option<Self>> {
        let db = niceerr_db::DBConfig::get_connection();
        Ok(ArlEntity::find()
            .filter(ArlColumn::Priority.eq(priority))
            .one(db)
            .await?)
    }

    /// Pull this ARL and its parent user in one go
    pub async fn find_with_user(id: &Uuid) -> EntityResult<(Self, User)> {
        let db = niceerr_db::DBConfig::get_connection();
        ArlEntity::find()
            .filter(ArlColumn::Id.eq(*id))
            .find_also_related(super::user::Entity)
            .one(db)
            .await?
            .and_then(|(a, u)| u.map(|u| (a, u)))
            .ok_or(EntityError::NotFound)
    }

    pub async fn find_by_id(id: &Uuid) -> EntityResult<Self> {
        let db = niceerr_db::DBConfig::get_connection();
        ArlEntity::find()
            .filter(ArlColumn::Id.eq(*id))
            .one(db)
            .await?
            .ok_or(EntityError::NotFound)
    }

    /// Delete proxy
    pub async fn delete_p(self) -> EntityResult<()> {
        let db = niceerr_db::DBConfig::get_connection();
        self.delete(db).await?;
        Self::reset_priority_index().await?;
        Ok(())
    }
}

impl SortFilterParams for SearchFilterArl {
    type Entity = ArlEntity;
    type Column = ArlColumn;

    fn sortable_columns() -> &'static [Self::Column] {
        &[
            ArlColumn::Id,
            ArlColumn::Alias,
            ArlColumn::CreatedBy,
            ArlColumn::Priority,
            ArlColumn::CreatedAt,
            ArlColumn::ExpiresAt,
            ArlColumn::Country,
        ]
    }

    fn filterable_columns() -> &'static [Self::Column] {
        // It makes sense to filter in the same columns
        // we use for sorting
        Self::sortable_columns()
    }

    fn get_search_value(self) -> Vec<impl sea_orm::sea_query::IntoLikeExpr + Clone> {
        self.search
    }

    fn get_sort_value(&self) -> Option<&String> {
        self.sort_by.as_ref()
    }

    fn default_sort_column() -> Self::Column {
        ArlColumn::Priority
    }

    fn sort_by_desc(&self) -> bool {
        self.desc.unwrap_or(false)
    }
}
