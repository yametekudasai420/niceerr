use std::ops::Deref;

use crate::{client::DzClient, error::DzResult};

/// You don't want to use this directly, unless you're crazy.
#[allow(async_fn_in_trait)]
pub trait Paginatable {
    type Item;
    type Initialized;
    type InitArgs;

    async fn fetch_page(
        this: &Self::Initialized,
        client: &DzClient,
        start: usize,
        items_per_page: usize,
    ) -> DzResult<Vec<Self::Item>>;

    async fn init(client: &DzClient, this: Self::InitArgs) -> DzResult<(usize, Self::Initialized)>;
}

/// Generic paginator interface to Deezer's paginatable types.
///
/// Note that the underlying generic type can also provide
/// additional methods, as is the case for `Playlist` and
/// `Discography`. `Deref` is implemented for any generic `T`, so you
/// can call the underlying methods directly even if you have
/// a `Paginator` object.
#[derive(Debug)]
pub struct Paginator<T: Paginatable> {
    client: DzClient,
    items_per_page: usize,
    current_page: usize,
    total_pages: usize,
    inner: T::Initialized,
}

impl<T: Paginatable> Deref for Paginator<T> {
    type Target = T::Initialized;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T: Paginatable> Paginator<T> {
    /// Fetch the next page in this paginator.
    ///
    /// The paginator will advance IF, and ONLY IF the return value
    /// is `Some(Ok())`. This allows callers to retry failed calls.
    pub async fn next(&mut self) -> Option<DzResult<Vec<T::Item>>> {
        if self.current_page == self.total_pages {
            return None;
        }
        let start = self.current_page * self.items_per_page;
        match T::fetch_page(&self.inner, &self.client, start, self.items_per_page).await {
            Ok(items) => {
                self.current_page += 1;
                Some(Ok(items))
            }
            Err(e) => Some(Err(e)),
        }
    }

    /// Get the inner initialized object of this paginator.
    pub fn into_inner(self) -> T::Initialized {
        self.inner
    }

    /// Set the current page to zero. This will basically reset
    /// the paginator.
    pub fn reset_page(&mut self) {
        self.current_page = 0;
    }

    /// Set this paginator's page.
    ///
    /// Panics if `page` is greater than or equal to the number of total pages.
    pub fn set_page(&mut self, page: usize) {
        assert!(page < self.total_pages);
        self.current_page = page;
    }

    /// Get the current page we're at.
    pub fn current_page(&self) -> usize {
        self.current_page
    }

    /// Total page count.
    pub fn total_pages(&self) -> usize {
        self.total_pages
    }

    pub(crate) async fn new(
        client: DzClient,
        items_per_page: usize,
        init_args: T::InitArgs,
    ) -> DzResult<Paginator<T>> {
        assert!(items_per_page > 0, "items_per_page can't be zero!");
        let (total_items, inner) = T::init(&client, init_args).await?;
        let total_pages = (total_items as f64 / items_per_page as f64).ceil() as usize;
        Ok(Paginator {
            client,
            items_per_page,
            current_page: 0,
            total_pages,
            inner,
        })
    }
}
