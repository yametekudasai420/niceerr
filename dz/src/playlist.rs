use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

use crate::{
    client::DzClient,
    error::{DzError, DzResult},
    pagination::Paginatable,
    types::IncompleteSong,
    util::{de_u64, try_extract},
};

/// A playlist with some incomplete data. You can use the client
/// to retrieve the full playlist, along with its contained songs.
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct IncompletePlaylist {
    #[serde(rename(deserialize = "PLAYLIST_ID"), deserialize_with = "de_u64")]
    pub playlist_id: u64,
    #[serde(rename(deserialize = "NB_SONG"))]
    pub song_count: usize,
    #[serde(rename(deserialize = "PARENT_USERNAME"))]
    pub username: String,
    #[serde(rename(deserialize = "PARENT_USER_ID"), deserialize_with = "de_u64")]
    pub user_id: u64,
    #[serde(rename(deserialize = "TITLE"))]
    pub title: String,
    #[serde(rename(deserialize = "PLAYLIST_PICTURE"))]
    pub picture: String,
}

/// Complete playlist representation.
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Playlist {
    #[serde(rename(deserialize = "PLAYLIST_ID"), deserialize_with = "de_u64")]
    playlist_id: u64,
    #[serde(default, rename(deserialize = "CURATOR"))]
    pub curator: bool,
    #[serde(rename(deserialize = "CHECKSUM"))]
    pub checksum: String,
    #[serde(rename(deserialize = "DATE_ADD"))]
    pub date_add: String,
    #[serde(rename(deserialize = "DATE_MOD"))]
    pub date_mod: String,
    #[serde(rename(deserialize = "DESCRIPTION"))]
    pub description: String,
    #[serde(rename(deserialize = "DURATION"))]
    pub duration: u64,
    #[serde(rename(deserialize = "IS_FAVORITE"))]
    pub is_favorite: Option<bool>,
    #[serde(rename(deserialize = "IS_SPONSORED"))]
    pub is_sponsored: bool,
    #[serde(rename(deserialize = "NB_FAN"))]
    pub fan_count: u64,
    #[serde(rename(deserialize = "NB_SONG"))]
    pub song_count: usize,
    #[serde(rename(deserialize = "PLAYLIST_PICTURE"))]
    pub picture: String,
    #[serde(rename(deserialize = "PARENT_USERNAME"))]
    pub username: String,
    #[serde(rename(deserialize = "PARENT_USER_ID"), deserialize_with = "de_u64")]
    pub user_id: u64,
    #[serde(rename(deserialize = "TITLE"))]
    pub title: String,
}

#[derive(Debug)]
pub struct PlaylistArgs {
    pub(crate) playlist_id: u64,
}

impl Paginatable for Playlist {
    type Item = IncompleteSong;
    type InitArgs = PlaylistArgs;
    type Initialized = Playlist;

    async fn init(client: &DzClient, args: Self::InitArgs) -> DzResult<(usize, Self::Initialized)> {
        let payload = HashMap::from([
            ("playlist_id".into(), args.playlist_id.into()),
            ("nb".into(), Value::from(0)),
        ]);
        let response = client
            .call_method_proxy("deezer.pagePlaylist", payload)
            .await?;
        let curator = response["results"]["CURATOR"].as_bool().unwrap_or(false);
        let mut playlist: Playlist = try_extract(&response, "/results/DATA")?;
        playlist.curator = curator;
        let total_items =
            response["results"]["SONGS"]["total"]
                .as_u64()
                .ok_or(DzError::UpstreamError(
                    "Couldn't deserialize playlist length".into(),
                ))? as usize;
        Ok((total_items, playlist))
    }

    async fn fetch_page(
        this: &Self::Initialized,
        client: &DzClient,
        start: usize,
        items_per_page: usize,
    ) -> DzResult<Vec<Self::Item>> {
        let payload = HashMap::from([
            ("playlist_id".into(), this.playlist_id.into()),
            ("nb".into(), Value::from(items_per_page)),
            ("start".into(), Value::from(start)),
        ]);
        let response = client
            .call_method_proxy("deezer.pagePlaylist", payload)
            .await?;
        IncompleteSong::try_from_array(&response["results"]["SONGS"]["data"])
    }
}

impl Playlist {
    // This playlist ID.
    pub fn id(&self) -> u64 {
        self.playlist_id
    }

    /// Get the total number of songs in this playlist.
    pub fn song_count(&self) -> usize {
        self.song_count
    }

    /// This playlist's cover URL.
    ///
    /// Some playlist's don't have a cover and their website
    /// cover is merely a deezer-branded picture (waves and vibrant
    /// colors) with the playlist's name overlaid on it.
    ///
    /// This method will return `None` for such playlists.
    pub fn cover_url(&self) -> Option<String> {
        if self.picture.is_empty() {
            return None;
        }
        Some(format!(
            "https://cdn-images.dzcdn.net/images/playlist/{}/3000x0-000000-80-0-0.jpg",
            self.picture
        ))
    }

    async fn cover_jpg_inner(&self, client: &DzClient) -> DzResult<Vec<u8>> {
        Ok(client
            .client
            .get(self.cover_url().expect("cover url is None"))
            .send()
            .await?
            .bytes()
            .await?
            .to_vec())
    }

    /// This playlist's cover image.
    ///
    /// See [`Playlist::cover_url`] for more information about this method.
    pub async fn cover_jpg(&self, client: &DzClient) -> Option<DzResult<Vec<u8>>> {
        self.cover_url()?;
        Some(self.cover_jpg_inner(client).await)
    }
}
