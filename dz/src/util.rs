use std::any::type_name;

use log::info;
use serde::{de::DeserializeOwned, Deserialize, Deserializer};
use serde_json::Value;

use crate::error::{DzError, DzResult, FallibleAPIResponse};

/// Try to extract a given T from a JSON value using a path pointer.
pub(crate) fn try_extract<T>(value: &Value, pointer: &str) -> DzResult<T>
where
    T: DeserializeOwned,
{
    let extracted = value
        .pointer(pointer)
        .ok_or_else(|| DzError::UpstreamError(format!("couldn't find {pointer} in value")))?;
    serde_json::from_value(extracted.to_owned()).map_err(|e| {
        DzError::UpstreamError(format!("couldn't deserialize as {}: {e}", type_name::<T>()))
    })
}

pub(crate) fn parse_media_gw_error(value: Vec<&Value>) -> DzResult<()> {
    #[derive(Deserialize, Debug)]
    #[allow(dead_code)]
    struct DzNestedError {
        code: i32,
        message: String,
    }
    for error in value {
        let deserialized = serde_json::from_value::<Vec<DzNestedError>>(error.to_owned());
        if let Ok(errors) = deserialized {
            info!("media url errors: {errors:?}");
            if errors.iter().any(|it| it.code == 2002) {
                return Err(DzError::Unavailable(
                    crate::error::UnavailableReason::Unpublished,
                ));
            }
        }
    }
    Ok(())
}

/// Whether this dz response is a fallback.
pub(crate) fn to_either(value: Value) -> FallibleAPIResponse {
    if value["payload"]["FALLBACK"].is_object() || value["results"]["DATA"]["FALLBACK"].is_object()
    {
        return FallibleAPIResponse::Fallback(value);
    }
    FallibleAPIResponse::Normal(value)
}

/// Whether this dz response has errors or not.
pub(crate) fn is_response_error(value: &Value) -> DzResult<()> {
    // get media url returns errors in a different format
    if let Some(err) = value
        .get("data")
        .and_then(|v| v.as_array())
        .map(|v| v.iter().map(|v| v.get("errors")).collect::<Vec<_>>())
    {
        if err.is_empty() {
            return Ok(());
        }
        let err = err.into_iter().flatten().collect::<Vec<_>>();
        parse_media_gw_error(err)?;
    }
    if let Some(err) = value.get("error").and_then(|it| it.as_array()) {
        if err.is_empty() {
            return Ok(());
        }
        return Err(DzError::UpstreamError(format!("{err:?}")));
    }
    if let Some(err) = value.get("error").and_then(|it| it.as_object()) {
        if err.is_empty() {
            return Ok(());
        }
        if err.get("REQUEST_ERROR").is_some() || err.get("DATA_ERROR").is_some() {
            return Err(DzError::NotFound);
        }
        return Err(DzError::UpstreamError(format!("{err:?}")));
    }
    Ok(())
}

pub(crate) fn de_u64<'de, D: Deserializer<'de>>(deserializer: D) -> Result<u64, D::Error> {
    Ok(match Value::deserialize(deserializer)? {
        Value::String(s) => s.parse().map_err(serde::de::Error::custom)?,
        Value::Number(num) => num
            .as_u64()
            .ok_or_else(|| serde::de::Error::custom("Invalid number"))?,
        _ => return Err(serde::de::Error::custom("wrong type")),
    })
}

pub(crate) fn de_usize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<usize, D::Error> {
    Ok(match Value::deserialize(deserializer)? {
        Value::String(s) => s.parse().map_err(serde::de::Error::custom)?,
        Value::Number(num) => num
            .as_u64()
            .ok_or_else(|| serde::de::Error::custom("Invalid number"))
            .map(|u| u as usize)?,
        _ => return Err(serde::de::Error::custom("wrong type")),
    })
}
