use std::{collections::HashMap, sync::Arc};

use log::{debug, info};
use reqwest::{cookie::Jar, Client, ClientBuilder, Url};
use serde::Deserialize;
use serde_json::Value;

use crate::{
    discography::{Discography, DiscographyArgs},
    error::{DzError, DzResult, FallibleAPIResponse},
    pagination::Paginator,
    playlist::{Playlist, PlaylistArgs},
    types::{
        Album, AlbumId, AlbumWCover, ArtistInfo, DiscographyIds, IncompleteSong, Song, StreamFormat,
    },
    util::{is_response_error, to_either, try_extract},
};

#[derive(Debug, Default)]
pub(crate) struct InnerClient {
    country: String,
    api_token: String,
    pub(crate) license_token: String,
    expiration: i64,
    lossless: bool,
}

/// A nice, cheaply cloneable deezer client.
///
/// This is the entrypoint to all of this library's methods
/// and functionality. Note that, in order to build a client,
/// you'll need a premium ARL that is capable of lossless
/// streaming.
///
/// Additional logic to login with non-premium accounts isn't
/// implemented.
#[derive(Debug, Clone)]
pub struct DzClient {
    pub(crate) client: Client,
    pub(crate) creds: Arc<InnerClient>,
}

impl DzClient {
    /// This account's origin country
    pub fn country(&self) -> &str {
        &self.creds.country
    }

    pub fn is_lossless(&self) -> bool {
        self.creds.lossless
    }

    /// Expiration timestamp.
    pub fn expiration(&self) -> i64 {
        self.creds.expiration
    }

    /// Creates a new client from an ARL and a reqwest builder. This method will fail
    /// if: the account isn't lossless, if the ARL is invalid or if the passed `ClientBuilder`
    /// cannot be built.
    ///
    /// This method requires a `ClientBuilder` in order to inject the ARL and
    /// cookie store configuration.
    ///
    /// Steps to get your own ARL:
    ///
    /// - Log into your deezer account using chrome
    /// - Open developer tools (F12)
    /// - Go to Application -> Cookies
    /// - Search 'arl'
    /// - Copy the long spaghetti string and use it with this method
    pub async fn new<S: AsRef<str>>(arl: S, builder: ClientBuilder) -> DzResult<Self> {
        Self::new_inner(arl, builder, true).await
    }

    /// Create a new client from an arl and a request builder. This method behaves just as
    /// [`DzClient::new`], except it'll accept a free ARL.
    pub async fn new_non_lossless<S: AsRef<str>>(arl: S, builder: ClientBuilder) -> DzResult<Self> {
        Self::new_inner(arl, builder, false).await
    }

    async fn new_inner<S: AsRef<str>>(
        arl: S,
        builder: ClientBuilder,
        hires_only: bool,
    ) -> DzResult<Self> {
        let jar = Jar::default();
        jar.add_cookie_str(
            &format!("arl={}", arl.as_ref()),
            &"https://www.deezer.com"
                .parse::<Url>()
                .expect("Invalid URL"),
        );
        let client = builder.cookie_provider(Arc::new(jar)).build()?;
        let creds = Arc::new(Self::get_dz_tokens(&client, hires_only).await?);

        let dz = DzClient { client, creds };
        Ok(dz)
    }

    /// Retrieves dz tokens. We need those to be able to access internal APIs and CDNs.
    /// Note that the ARL token must be already set in the passed credentials.
    async fn get_dz_tokens(client: &Client, hires_only: bool) -> DzResult<InnerClient> {
        let creds = InnerClient {
            ..Default::default()
        };
        let user_data =
            Self::call_dz_method(client, "deezer.getUserData", &creds, Default::default()).await?;
        let offer_name = try_extract::<String>(&user_data, "/results/OFFER_NAME")?.to_lowercase();
        if hires_only && offer_name.contains("deezer free") {
            return Err(DzError::SubscriptionRequired);
        }
        let license_token: String = try_extract(&user_data, "/results/USER/OPTIONS/license_token")?;
        let country: String = try_extract(&user_data, "/results/COUNTRY")?;
        let expiration: i64 =
            try_extract(&user_data, "/results/USER/OPTIONS/expiration_timestamp")?;
        let api_token: String = try_extract(&user_data, "/results/checkForm")?;
        let lossless: bool = try_extract(&user_data, "/results/USER/OPTIONS/web_lossless")?;

        if hires_only && !lossless {
            info!("account can't stream hq/flac");
            return Err(DzError::SubscriptionRequired);
        }
        Ok(InnerClient {
            country,
            api_token,
            license_token,
            expiration,
            lossless,
        })
    }

    /// Calls a deezer method, using this struct's internal credentials.
    /// # Arguments
    ///
    /// * `method` - Deeznuts method to call.
    /// * `payload` - Payload to send with the function args.
    ///
    /// This method returns arbitrary serde values. You probably don't want to
    /// use this function, unless you know what you're doing.
    pub async fn call_method_proxy(
        &self,
        method: &str,
        payload: HashMap<String, Value>,
    ) -> DzResult<FallibleAPIResponse> {
        let creds = &self.creds;
        Self::call_dz_method(&self.client, method, creds, payload).await
    }

    /// Calls a deeznuts method.
    async fn call_dz_method<S: AsRef<str>>(
        client: &Client,
        deezer_method: S,
        creds: &InnerClient,
        payload: HashMap<String, Value>,
    ) -> DzResult<FallibleAPIResponse> {
        // api_token must be empty when we log in first
        let api_token = match deezer_method.as_ref() {
            "deezer.getUserData" => "",
            _ => &creds.api_token,
        };

        let mut params: HashMap<String, Value> = HashMap::from([
            ("method".into(), deezer_method.as_ref().into()),
            ("input".into(), "3".into()),
            ("api_version".into(), "1.0".into()),
            ("api_token".into(), api_token.into()),
        ]);
        params.extend(payload);

        let response = client
            .post("https://www.deezer.com/ajax/gw-light.php")
            .json(&params)
            .send()
            .await?
            .error_for_status()?
            .json::<Value>()
            .await?;

        debug!("dz: {}(): {:#?}", deezer_method.as_ref(), response);
        debug!("dz: extra info: params: {:#?} creds: {:#?}", params, creds);

        let out = to_either(response);
        if let FallibleAPIResponse::Fallback(_) = &out {
            return Ok(out);
        }
        is_response_error(&out).inspect_err(|_| {
            info!("got invalid upstream response: {out:?}");
        })?;

        Ok(out)
    }

    /// Get information about an artist.
    ///
    /// This information includes, but is not limited to:
    ///
    /// - Their "picture" or cover image
    /// - Number of fans
    /// - Highlighted songs
    /// - Top songs
    /// - Related artists
    /// - Biography
    /// - ...and other related stuff.
    ///
    /// This method is useless if you want to get a list of their albums
    /// or songs.
    pub async fn artist_info(&self, artist_id: u64) -> DzResult<ArtistInfo> {
        ArtistInfo::get(artist_id, self).await
    }

    /// Get a paginator to an artist's entire discography. This paginator yields a stream
    /// of pages, where each page is an array of albums. This call is as efficient as it
    /// gets because each album also contains its songs, so you don't need to call
    /// anything else to fetch either the album or the songs themselves.
    ///
    /// Arguments:
    ///
    /// - artist_id: Deezer artist id
    /// - items_per_page: Number of items to pull per page.
    ///
    /// **Panics if `items_per_page` is zero.**
    ///
    /// [`DzError`] is returned if there's a network error or if the artist doesn't exist.
    ///
    ///
    /// <h2> BIG BOY NOTE </h2>
    ///
    /// Due to deezer's API being weird and inconsistent, albums fetched with this method
    /// will have the following properties set to `None`:
    ///
    /// - `label`
    pub async fn discography_paginated(
        &self,
        artist_id: u64,
        items_per_page: usize,
        include_credited: bool,
    ) -> DzResult<Paginator<Discography>> {
        let init_args = DiscographyArgs {
            artist_id,
            include_credited,
        };
        Paginator::new(self.clone(), items_per_page, init_args).await
    }

    /// Get a paginator to a playlist. This paginator yields a stream
    /// of pages, where each page is an array of [`Song`]s. **You can also use this method
    /// to only fetch the playlist information, if you want to.**
    ///
    /// Arguments:
    ///
    /// - playlist_id: Deezer playlist id
    /// - items_per_page: Number of items to pull per page.
    ///
    /// **Panics if `items_per_page` is zero.**
    ///
    /// [`DzError`] is returned if there's a network error or if the playlist doesn't exist.
    ///
    ///
    /// <h2> BIG BOY NOTE </h2>
    ///
    /// Due to deezer's API being weird and inconsistent, albums fetched with this method
    /// will have the following properties set to `None`:
    ///
    /// - `label`
    pub async fn playlist_paginated(
        &self,
        playlist_id: u64,
        items_per_page: usize,
    ) -> DzResult<Paginator<Playlist>> {
        let init_args = PlaylistArgs { playlist_id };
        Paginator::new(self.clone(), items_per_page, init_args).await
    }

    /// Get an artist's entire discography as album IDs.
    ///
    /// This is the dirt and cheap version of `discography_paginated` as the returned
    /// "discography" only contains an array of album IDs. This pulls all albums IDs at once in just
    /// a single call.
    ///
    /// Note that you'll still need to fetch each album individually to get the actual contents.
    pub async fn full_discography_ids(
        &self,
        id: u64,
        include_credited: bool,
    ) -> DzResult<DiscographyIds> {
        let filter_role_id = include_credited.then_some(vec![0, 5]).unwrap_or(vec![0]);
        let payload: HashMap<_, Value> = HashMap::from([
            ("art_id".into(), id.into()),
            ("start".into(), 0.into()),
            ("nb".into(), Value::from(-1)),
            ("filter_role_id".into(), filter_role_id.into()),
            ("nb_songs".into(), 0.into()), // Don't pull song data
            ("discography_mode".into(), "all".into()), //
            ("array_default".into(), vec!["ALB_ID"].into()),
        ]);
        let response = self
            .call_method_proxy("album.getDiscography", payload)
            .await?;
        let albums: Vec<AlbumId> = try_extract(&response, "/results/data")?;
        let album_ids = albums
            .into_iter()
            .map(|a| a.id.parse::<u64>())
            .collect::<Result<Vec<u64>, _>>()
            .map_err(|e| DzError::UpstreamError(e.to_string()))?;
        if album_ids.is_empty() {
            info!("artist {id} does not have any albums");
            return Err(DzError::NotFound);
        }
        Ok(DiscographyIds {
            album_ids,
            include_credited,
        })
    }

    /// Convenience function that returns the album object with its cover image as a tuple.
    pub async fn album_w_cover(&self, id: u64) -> DzResult<AlbumWCover> {
        let album = self.album(id).await?;
        let cover = album.cover_jpg(self).await?;
        Ok(AlbumWCover { album, cover })
    }

    /// Get an album given an ID.
    pub async fn album(&self, id: u64) -> DzResult<Album> {
        let mut payload: HashMap<_, Value> = HashMap::from([
            ("alb_id".into(), id.into()),
            ("lang".into(), "us".into()),
            ("nb".into(), 0.into()),
        ]);
        let mut response = self
            .call_method_proxy("deezer.pageAlbum", payload.clone())
            .await?;
        if let FallibleAPIResponse::Fallback(f) = response {
            // Try to cope with deezer's ill-designed API with mixed up types
            let first = try_extract::<u64>(&f, "/payload/FALLBACK/ALB_ID");
            let second = try_extract::<String>(&f, "/results/DATA/FALLBACK/ALB_ID").and_then(|v| {
                v.parse::<u64>()
                    .map_err(|e| DzError::UpstreamError(e.to_string()))
            });
            let id = first.or(second)?;

            payload.insert("alb_id".into(), id.into());
            response = self.call_method_proxy("deezer.pageAlbum", payload).await?;
        }

        let songs = IncompleteSong::try_from_array(&response["results"]["SONGS"]["data"])?;
        let mut album = Album::from_value(&response)?;
        album.songs = songs;
        self.inject_metadata(&mut album).await?;
        Ok(album)
    }

    /// Get a song given an ID.
    ///
    /// This method will always return a [`Song`] instance with all its fields populated.
    ///
    /// **ALL OTHER METHODS will always return incomplete songs!**
    pub async fn song(&self, id: u64) -> DzResult<Song> {
        let payload: HashMap<_, Value> =
            HashMap::from([("sng_id".into(), id.into()), ("lang".into(), "us".into())]);
        let response = self.call_method_proxy("deezer.pageTrack", payload).await?;
        Song::try_from_response(&response)
    }

    /// Whether this client has the rights to stream the specified song. This method
    /// also checks whether this client's country can stream the song.
    ///
    /// Returns Ok(()) on success, or [`DzError::Unavailable`] on failure.
    pub fn can_stream(&self, song: &Song) -> DzResult<()> {
        if let Some(rights) = &song.rights.stream_sub_available {
            if !rights {
                return Err(DzError::Unavailable(
                    crate::error::UnavailableReason::Unpublished,
                ));
            }
        }
        if song.is_unavailable() {
            return Err(DzError::Unavailable(
                crate::error::UnavailableReason::Unpublished,
            ));
        }

        if !song.country_can_stream(self.country()) {
            return Err(DzError::Unavailable(
                crate::error::UnavailableReason::Geoblocked,
            ));
        }
        Ok(())
    }

    /// Does exactly the same thing as [`DzClient::has_rights`] and more: this method
    /// also checks if this client can stream the song at the specified format. Returns
    /// Ok(()) on success.
    pub fn can_stream_at_format(&self, song: &Song, format: &StreamFormat) -> DzResult<()> {
        self.can_stream(song)?;
        if !song.is_stream_format_available(format) && song.fallback_format(format).is_some() {
            return Err(DzError::Unavailable(
                crate::error::UnavailableReason::InvalidStreamQuality,
            ));
        }
        Ok(())
    }

    async fn inject_metadata(&self, album: &mut Album) -> DzResult<()> {
        let metadata = self
            .client
            .get(format!("https://api.deezer.com/album/{}", album.id))
            .send()
            .await?
            .json::<Value>()
            .await?;
        #[derive(Debug, Deserialize)]
        struct GenreContainer {
            name: String,
        }
        let genres: Vec<GenreContainer> = try_extract(&metadata, "/genres/data")?;
        album.genres = genres.into_iter().map(|g| g.name).collect();
        album.record_type = try_extract(&metadata, "/record_type")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use reqwest::Client;
    use tokio::io::AsyncWriteExt;

    use crate::{
        error::DzError,
        types::{ExtraTagInfo, StreamFormat},
    };

    use super::DzClient;

    async fn get_arl_from_env() -> String {
        std::env::var("ARL").expect("ARL env var not set")
    }

    async fn build_client() -> DzClient {
        let arl = get_arl_from_env().await;
        let builder = Client::builder();
        DzClient::new(&arl, builder)
            .await
            .expect("Cannot create client")
    }

    #[tokio::test]
    async fn test_init_client() {
        let client = build_client().await;
        assert!(client.expiration() > 0);
    }

    #[tokio::test]
    async fn test_geoblocked_song() {
        let song_id = 82735708;
        let client = build_client().await;
        let mut song = client.song(song_id).await.expect("Couldn't fetch song");
        song.available_countries.stream_ads = vec!["XX".into(), "XY".into(), "XZ".into()];
        assert_eq!(
            client.can_stream(&song),
            Err(DzError::Unavailable(
                crate::error::UnavailableReason::Geoblocked
            ))
        );
        let err = song
            .stream(&client, &StreamFormat::Flac)
            .await
            .err()
            .expect("expected an error");
        assert_eq!(
            err,
            DzError::Unavailable(crate::error::UnavailableReason::Geoblocked)
        );
    }

    #[tokio::test]
    async fn test_unavailable_song() {
        // this one was completely unpublished
        let song_id = 82735714;
        let client = build_client().await;
        let song = client.song(song_id).await.expect("Couldn't fetch song");
        assert!(
            song.available_countries.stream_ads.is_empty(),
            "song should be unavailable"
        );
        assert_eq!(
            client.can_stream(&song),
            Err(DzError::Unavailable(
                crate::error::UnavailableReason::Unpublished
            ))
        );
        let err = song
            .stream(&client, &StreamFormat::Flac)
            .await
            .err()
            .expect("expected an error");
        assert_eq!(
            err,
            DzError::Unavailable(crate::error::UnavailableReason::Unpublished)
        );
    }

    #[tokio::test]
    async fn test_playlist_chunked() {
        // Deezer's official soft rock playlist
        let playlist_id = 6121968764;
        let client = build_client().await;
        let mut playlist = client
            .playlist_paginated(playlist_id, 20)
            .await
            .expect("Couldn't fetch playlist");

        assert!(playlist.song_count() > 10);
        assert_eq!(playlist.id(), playlist_id);
        assert!(playlist.total_pages() > 2);
        assert_eq!(playlist.current_page(), 0);

        let mut total_fetched = 0;
        let mut loops = 0;
        while let Some(Ok(songs)) = playlist.next().await {
            assert!(songs.len() <= 20);
            total_fetched += songs.len();
            loops += 1
        }
        assert_eq!(loops, playlist.total_pages());
        // Has 50 songs as of today (28/May/2024)
        assert!(total_fetched > 40);
    }

    #[tokio::test]
    async fn test_discography_ids() {
        // Lifelover no longer releases albums
        let artist_id = 171937;
        let client = build_client().await;
        let discography = client
            .full_discography_ids(artist_id, false)
            .await
            .expect("discography error");
        assert!(
            !discography.album_ids.is_empty(),
            "Discography shouldn't be empty"
        );
        assert!(
            !discography.include_credited(),
            "Include credited is set to true"
        );
        // Lifelover has 4 albums + 1 EP
        assert_eq!(discography.ids().len(), 5);
    }

    #[tokio::test]
    async fn test_discography_stream() {
        // Lifelover has 5 albums
        let artist_id = 171937;
        let client = build_client().await;
        let mut discography = client
            .discography_paginated(artist_id, 10, false)
            .await
            .expect("Couldn't fetch discography");

        assert_eq!(discography.album_count(), 5);
        assert_eq!(discography.current_page(), 0);
        assert_eq!(discography.total_pages(), 1);

        let albums = discography
            .next()
            .await
            .expect("Expected albums")
            .expect("Couldn't fetch albums (network error?)");

        assert_eq!(albums.len(), 5);
        for album in albums.iter() {
            assert!(!album.songs.is_empty(), "album is empty");
            assert_eq!(album.is_dz_compilation(), false);
            assert_eq!(album.is_live(), false);
            let byte_size: usize = album
                .songs
                .iter()
                .map(|s| s.byte_size(&StreamFormat::Flac))
                .sum();
            assert_eq!(byte_size, album.byte_size(&StreamFormat::Flac));
        }
        // Paginator no longer has albums
        let next = discography.next().await;
        assert!(next.is_none());
        assert_eq!(discography.current_page(), 1);
    }

    #[tokio::test]
    async fn test_discography_stream_chunked() {
        // Lifelover has 5 albums
        let artist_id = 171937;
        let client = build_client().await;
        let mut discography = client
            .discography_paginated(artist_id, 2, false)
            .await
            .expect("Couldn't fetch discography");

        assert_eq!(discography.album_count(), 5);
        assert_eq!(discography.current_page(), 0);
        assert_eq!(discography.total_pages(), 3); // 5 albums / 2 items per page = 3 pages

        let mut total_fetched = 0;
        while let Some(Ok(albums)) = discography.next().await {
            assert!(albums.len() <= 2);
            total_fetched += albums.len();
            for album in albums.iter() {
                assert!(!album.songs.is_empty(), "album is empty");
            }
        }
        // Reset page manually
        discography.set_page(0);
        assert_eq!(discography.current_page(), 0);
        assert!(discography.next().await.is_some());
        assert_eq!(discography.current_page(), 1);
        discography.reset_page();
        assert_eq!(discography.current_page(), 0);

        assert_eq!(total_fetched, 5);
    }

    #[tokio::test]
    async fn test_album() {
        // Lifelover's Konkurs
        let client = build_client().await;
        let album = client.album(368544997).await.expect("Cannot fetch album");
        assert_eq!(album.release_year(), Some(2008));
        assert_eq!(album.is_live(), false);
        assert_eq!(album.is_dz_compilation(), false);
        assert_eq!(album.disk_count(), 1);
    }

    #[tokio::test]
    async fn test_not_found() {
        let invalid_id = u64::MAX;
        let client = build_client().await;
        let artist = client.full_discography_ids(invalid_id, true).await;
        let err = artist.err().expect("Expected an error");
        assert_eq!(err, DzError::NotFound);
        let album = client.album(invalid_id).await;
        let err = album.err().expect("Expected an error");
        assert_eq!(err, DzError::NotFound);
        let song = client.song(invalid_id).await;
        let err = song.err().expect("Expected an error");
        assert_eq!(err, DzError::NotFound);
    }

    #[tokio::test]
    async fn test_stream_song() {
        // 1-minute song
        let song_id = 1977411587;
        let client = build_client().await;
        let song = client.song(song_id).await.expect("Expected song");
        assert!(song.stream_key().len() > 0);
        assert_eq!(song.id, song_id);
        assert_eq!(client.can_stream(&song), Ok(()));
        assert_eq!(song.byte_size(&StreamFormat::Mp3_128), 976_351);
        assert_eq!(song.byte_size(&StreamFormat::Mp3_320), 2_440_880);
        assert_eq!(song.byte_size(&StreamFormat::Flac), 4_306_071);
        // Song has no lyrics.
        assert_eq!(song.lyrics_available(), false);
        let lyrics = song
            .fetch_lyrics(&client)
            .await
            .err()
            .expect("Expected error");
        assert_eq!(lyrics, DzError::NotFound);

        let bytes = song
            .bytes(&client, &crate::types::StreamFormat::Mp3_128)
            .await
            .expect("Couldn't get byte stream");
        assert_eq!(bytes.len(), 976_351);

        // Test fallback formats
        assert_eq!(
            song.fallback_format(&StreamFormat::Flac),
            Some(StreamFormat::Mp3_320)
        );
        assert_eq!(
            song.fallback_format(&StreamFormat::Mp3_320),
            Some(StreamFormat::Mp3_128)
        );

        assert_eq!(song.fallback_format(&StreamFormat::Mp3_128), None);
    }

    #[tokio::test]
    async fn test_write_tags() {
        let song_id = 1977411587;
        let client = build_client().await;
        let song = client.song(song_id).await.expect("Expected song");
        let album = client.album(song.album_id).await.expect("Cannot get album");
        let bytes = song
            .bytes(&client, &StreamFormat::Mp3_128)
            .await
            .expect("Couldn't get byte stream");
        let temp_dir = std::env::temp_dir().join("test.mp3");
        let mut file = tokio::fs::OpenOptions::new()
            .truncate(true)
            .read(true)
            .create(true)
            .write(true)
            .open(&temp_dir)
            .await
            .expect("Cannot create temp file");
        file.write_all(&bytes).await.expect("Couldn't write bytes");
        file.flush().await.ok();
        let picture = song.cover_jpg(&client).await.expect("Cannot fetch cover");
        let handle = file.into_std().await;
        song.file_apply_tags(handle, album.clone(), picture, ExtraTagInfo::default())
            .await
            .expect("Couldn't apply tags");
    }

    #[tokio::test]
    async fn test_lyrics() {
        // Lifelover / Konkurs / Mental central dialog
        let song_id = 1977411467;
        let client = build_client().await;
        let song = client.song(song_id).await.expect("Expected song");
        let lyrics = song.fetch_lyrics(&client).await.expect("Expected error");
        assert!(lyrics.text.len() > 0);
    }

    #[tokio::test]
    async fn test_unavailable_stream() {
        // Nocte Obducta - Stille: this entire album is only available in MP3
        let song_id = 15450390;
        let client = build_client().await;
        let song = client.song(song_id).await.expect("Expected song");
        assert!(song.stream_key().len() > 0);
        assert_eq!(song.id, song_id);
        assert_eq!(client.can_stream(&song), Ok(()));
        assert_eq!(song.byte_size(&StreamFormat::Mp3_128), 4_095_615);
        assert_eq!(song.byte_size(&StreamFormat::Mp3_320), 10_238_954);
        assert_eq!(song.byte_size(&StreamFormat::Flac), 0);

        let stream = song
            .stream(&client, &StreamFormat::Flac)
            .await
            .err()
            .expect("Expected error");
        assert_eq!(
            stream,
            DzError::Unavailable(crate::error::UnavailableReason::InvalidStreamQuality)
        );
    }

    #[test]
    fn test_stream_format_methods() {
        assert_eq!(StreamFormat::Mp3_128.extension(), "mp3");
        assert_eq!(StreamFormat::Mp3_320.extension(), "mp3");
        assert_eq!(StreamFormat::Flac.extension(), "flac");
        assert_eq!(StreamFormat::Mp3_128.to_string(), "MP3_128");
        assert_eq!(StreamFormat::Mp3_320.to_string(), "MP3_320");
        assert_eq!(StreamFormat::Flac.to_string(), "FLAC");
    }

    #[tokio::test]
    async fn test_get_artist_info_underground() {
        // Based and redpilled Dimhymn
        let artist_id = 63414462;
        let client = build_client().await;
        let info = client
            .artist_info(artist_id)
            .await
            .expect("Couldn't get artist info");
        // Dimhymn is underground and doesn't have a bio
        // or related artists data.
        assert_eq!(info.biography, None);
        assert!(info.related_artists.is_empty());
        assert!(info.related_playlists.is_empty());
        assert!(!info.highlights.is_empty());

        let pic_url = info.cover_url();
        assert!(pic_url.len() > 0);
        let picture = info.cover_jpg(&client).await.expect("Cannot fetch cover");
        assert!(picture.len() > 0);
    }

    #[tokio::test]
    async fn test_get_artist_info_normie() {
        // Korn is as normie as it gets. But they're cool.
        let artist_id = 1327;
        let client = build_client().await;
        let info = client
            .artist_info(artist_id)
            .await
            .expect("Couldn't get artist info");
        // Dimhymn is underground and doesn't have a bio
        // or related artists data.
        assert!(info.biography.is_some());
        assert!(!info.related_artists.is_empty());
        assert!(!info.related_playlists.is_empty());
        assert!(!info.highlights.is_empty());
        assert!(!info.top_songs.is_empty());
        let pic_url = info.cover_url();
        assert!(pic_url.len() > 0);
        let picture = info.cover_jpg(&client).await.expect("Cannot fetch cover");
        assert!(picture.len() > 0);
    }

    #[tokio::test]
    async fn test_get_playlist_no_cover() {
        let playlist_id = 198436;
        let client = build_client().await;
        let playlist = client
            .playlist_paginated(playlist_id, 1)
            .await
            .expect("Couldn't get playlist");
        assert_eq!(playlist.cover_url(), None);
        assert_eq!(playlist.cover_jpg(&client).await, None);
    }

    #[tokio::test]
    async fn test_get_playlist_with_cover() {
        let playlist_id = 4717254048;
        let client = build_client().await;
        let playlist = client
            .playlist_paginated(playlist_id, 1)
            .await
            .expect("Couldn't get playlist");
        assert!(playlist.cover_url().is_some());
        assert!(
            playlist
                .cover_jpg(&client)
                .await
                .expect("Cannot fetch cover")
                .expect("Cannot fetch cover")
                .len()
                > 0
        );
    }

    #[tokio::test]
    async fn fallback_album() {
        let album_id = 76127552;
        let client = build_client().await;
        let album = client.album(album_id).await.expect("Cannot get album");
        assert_ne!(album.id, album_id);
    }

    #[tokio::test]
    async fn fallback_song() {
        let album_id = 92658012;
        let client = build_client().await;
        let album = client.song(album_id).await.expect("Cannot get album");
        assert_ne!(album.id, album_id);
    }
}
