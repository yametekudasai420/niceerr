use serde_json::Value;
use std::collections::HashMap;

use crate::{
    client::DzClient,
    error::{DzError, DzResult},
    pagination::Paginatable,
    types::{Album, IncompleteSong},
};

#[derive(Debug)]
pub struct Discography {
    artist_id: u64,
    include_credited: bool,
    album_count: usize,
}

#[derive(Debug)]
pub struct DiscographyArgs {
    pub(crate) artist_id: u64,
    pub(crate) include_credited: bool,
}

impl Paginatable for Discography {
    type Item = Album;
    type InitArgs = DiscographyArgs;
    type Initialized = Discography;

    async fn init(client: &DzClient, args: Self::InitArgs) -> DzResult<(usize, Self::Initialized)> {
        let albums = client
            .full_discography_ids(args.artist_id, args.include_credited)
            .await?;
        let total_items = albums.ids().len();
        Ok((
            total_items,
            Discography {
                artist_id: args.artist_id,
                include_credited: args.include_credited,
                album_count: total_items,
            },
        ))
    }

    async fn fetch_page(
        this: &Self::Initialized,
        client: &DzClient,
        start: usize,
        items_per_page: usize,
    ) -> DzResult<Vec<Self::Item>> {
        let filter_role_id = this
            .include_credited
            .then_some(vec![0, 5])
            .unwrap_or(vec![0]);
        let payload: HashMap<String, Value> = HashMap::from([
            ("art_id".into(), this.artist_id.into()),
            ("start".into(), start.into()),
            ("nb".into(), Value::from(items_per_page)),
            ("filter_role_id".into(), filter_role_id.into()),
            ("nb_songs".into(), u32::MAX.into()),
            ("discography_mode".into(), "all".into()),
        ]);

        let response = client
            .call_method_proxy("album.getDiscography", payload)
            .await?;
        let albums = response["results"]["data"]
            .as_array()
            .ok_or(DzError::UpstreamError(
                "Couldn't deserialize albums as array".into(),
            ))?;

        let mut ret = vec![];
        for album in albums {
            let songs = IncompleteSong::try_from_array(&album["SONGS"]["data"])?;
            let mut album: Album = serde_json::from_value(album.to_owned())
                .map_err(|e| DzError::UpstreamError(e.to_string()))?;
            album.songs = songs;
            ret.push(album);
        }
        Ok(ret)
    }
}

impl Discography {
    /// Get the total number of albums in this discography.
    pub fn album_count(&self) -> usize {
        self.album_count
    }

    /// Whether this discography includes credited albums or not.
    pub fn include_credited(&self) -> bool {
        self.include_credited
    }
}
