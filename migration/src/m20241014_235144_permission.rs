use niceerr_entity::{assigned_permission, user};
use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(AssignedPermission::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(AssignedPermission::UserId)
                            .uuid()
                            .not_null()
                            .comment("User ID this permission is assigned to"),
                    )
                    .col(
                        ColumnDef::new(AssignedPermission::Name)
                            .string()
                            .not_null()
                            .comment("Name of this permission, hardcoded in the app"),
                    )
                    .col(
                        ColumnDef::new(AssignedPermission::Scope)
                            .string()
                            .not_null()
                            .comment("Scope of this permission (whether it only applies to the caller, or globally"),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            // create foreign key from this record...
                            .from(
                                assigned_permission::Entity,
                                assigned_permission::Column::UserId,
                            )
                            // ..and point it to upload_session
                            .to(user::Entity, user::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .primary_key(
                        Index::create()
                            .col(AssignedPermission::Name)
                            .col(AssignedPermission::UserId),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .if_exists()
                    .table(AssignedPermission::Table)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum AssignedPermission {
    Table,
    UserId,
    Name,
    Scope,
}
