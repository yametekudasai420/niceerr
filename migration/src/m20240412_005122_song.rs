use niceerr_entity::{download_session, song};
use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(DownloadedSong::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(DownloadedSong::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(DownloadedSong::SessionId).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            // create foreign key from this record...
                            .from(song::Entity, song::Column::SessionId)
                            // ..and point it to upload_session
                            .to(download_session::Entity, download_session::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(DownloadedSong::ItemId)
                            .big_integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DownloadedSong::TotalBytes)
                            .integer()
                            .not_null()
                            .default(0),
                    )
                    .col(
                        ColumnDef::new(DownloadedSong::DownloadedBytes)
                            .integer()
                            .not_null()
                            .default(0),
                    )
                    .col(
                        ColumnDef::new(DownloadedSong::DownloadPath)
                            .string()
                            .not_null(),
                    )
                    .col(ColumnDef::new(DownloadedSong::ErrorMessage).string().null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .if_exists()
                    .table(DownloadedSong::Table)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
pub enum DownloadedSong {
    Table,
    Id,
    SessionId,
    ItemId,
    TotalBytes,
    DownloadedBytes,
    DownloadPath,
    ErrorMessage,
}
