use sea_orm_migration::prelude::*;

use crate::m20240405_221720_dz_arl::DeeznutsArl;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts

        manager
            .alter_table(
                Table::alter()
                    .table(DeeznutsArl::Table)
                    .add_column_if_not_exists(
                        ColumnDef::new(DeeznutsArl::Country)
                            .string()
                            .not_null()
                            .default("US"),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts
        manager
            .alter_table(
                Table::alter()
                    .table(DeeznutsArl::Table)
                    .drop_column(DeeznutsArl::Country)
                    .to_owned(),
            )
            .await
    }
}
