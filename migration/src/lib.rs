pub use sea_orm_migration::prelude::*;

mod m20220101_000001_create_user;
mod m20240405_221720_dz_arl;
mod m20240405_224015_session;
mod m20240412_005122_song;
mod m20240626_165334_arl_add_country_field;
mod m20241014_235144_arl_token_add_unique;
mod m20241014_235144_permission;
mod m20241014_235144_remove_user_cap_bool;
mod m20250126_191924_session_add_title;
mod m20250224_225409_session_add_skipped;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_user::Migration),
            Box::new(m20240405_221720_dz_arl::Migration),
            Box::new(m20240405_224015_session::Migration),
            Box::new(m20240412_005122_song::Migration),
            Box::new(m20240626_165334_arl_add_country_field::Migration),
            Box::new(m20241014_235144_arl_token_add_unique::Migration),
            Box::new(m20241014_235144_permission::Migration),
            Box::new(m20241014_235144_remove_user_cap_bool::Migration),
            Box::new(m20250126_191924_session_add_title::Migration),
            Box::new(m20250224_225409_session_add_skipped::Migration),
        ]
    }
}
