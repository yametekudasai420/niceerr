use sea_orm_migration::prelude::*;

use crate::m20240405_224015_session::DownloadSession;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts

        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .add_column_if_not_exists(
                        ColumnDef::new(DownloadSession::Title)
                            .string()
                            .not_null()
                            .default(""),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts
        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .drop_column(DownloadSession::Title)
                    .to_owned(),
            )
            .await
    }
}
