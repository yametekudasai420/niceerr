use niceerr_entity::{arl, user};
use sea_orm_migration::prelude::*;
#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(DeeznutsArl::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(DeeznutsArl::Id)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(DeeznutsArl::Alias)
                            .string()
                            .unique_key()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(DeeznutsArl::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(
                        ColumnDef::new(DeeznutsArl::ExpiresAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(ColumnDef::new(DeeznutsArl::CreatedBy).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            // create foreign key from this record...
                            .from(arl::Entity, arl::Column::CreatedBy)
                            // ..and point it to upload_session
                            .to(user::Entity, user::Column::Id)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(DeeznutsArl::Token).string().not_null())
                    .col(
                        ColumnDef::new(DeeznutsArl::Priority)
                            .unique_key()
                            .integer()
                            .auto_increment()
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .if_exists()
                    .table(DeeznutsArl::Table)
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
pub enum DeeznutsArl {
    Table,
    Id,
    Alias,
    CreatedAt,
    ExpiresAt,
    CreatedBy,
    Token,
    Country,
    Priority,
}
