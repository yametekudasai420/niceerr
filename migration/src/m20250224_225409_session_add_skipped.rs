use sea_orm_migration::{prelude::*, sea_orm::JsonValue};

use crate::m20240405_224015_session::DownloadSession;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .add_column_if_not_exists(
                        ColumnDef::new(DownloadSession::SkippedContent)
                            .json_binary()
                            .default(JsonValue::Array(vec![]))
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await?;
        // Map all "skip live" booleans to ["LIVE"]
        let table = DownloadSession::Table.to_string();
        let target_col = DownloadSession::SkippedContent.to_string();
        let source_col = DownloadSession::SkipLive.to_string();
        let stmt = format!(
            r#"
UPDATE {table}
SET {target_col} = '["LIVE"]'::jsonb
WHERE {source_col} = true;
    "#
        );
        let db = manager.get_connection();
        db.execute_unprepared(&stmt).await?;

        // Drop the useless column now

        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .drop_column(DownloadSession::SkipLive)
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Re-add the SkipLive column as a boolean, defaulting to false
        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .add_column_if_not_exists(
                        ColumnDef::new(DownloadSession::SkipLive)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await?;

        // Restore the "skip live" boolean based on the contents of SkippedContent
        let table = DownloadSession::Table.to_string();
        let target_col = DownloadSession::SkipLive.to_string();
        let source_col = DownloadSession::SkippedContent.to_string();
        let stmt = format!(
            r#"
UPDATE {table}
SET {target_col} = true
WHERE {source_col} = '["LIVE"]'::jsonb;
    "#
        );
        let db = manager.get_connection();
        db.execute_unprepared(&stmt).await?;

        // Drop the SkippedContent column
        manager
            .alter_table(
                Table::alter()
                    .table(DownloadSession::Table)
                    .drop_column(DownloadSession::SkippedContent)
                    .to_owned(),
            )
            .await?;

        Ok(())
    }
}
