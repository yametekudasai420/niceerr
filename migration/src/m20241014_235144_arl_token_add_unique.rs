use sea_orm_migration::prelude::*;

use crate::m20240405_221720_dz_arl::DeeznutsArl;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // This will delete potentially duplicate ARLs because the original migration
        // didn't have the code that added the unique key attribute on the "token" column.
        //
        // That code has been added but we need a brand new migration anyway for existing
        // Niceerr instances.
        let table = DeeznutsArl::Table.to_string();
        let id_col = DeeznutsArl::Id.to_string();
        let token_col = DeeznutsArl::Token.to_string();
        let stmt = format!(
            r#"WITH DuplicateTokens AS (
SELECT 
    "{id_col}", 
    "{token_col}", 
    ROW_NUMBER() OVER (PARTITION BY "{token_col}" ORDER BY "{id_col}") AS rn
FROM 
    {table}
)
DELETE FROM {table}
WHERE "{id_col}" IN (
    SELECT "{id_col}" FROM DuplicateTokens WHERE rn > 1
);"#
        );
        let db = manager.get_connection();
        db.execute_unprepared(&stmt).await?;

        manager
            .alter_table(
                Table::alter()
                    .table(DeeznutsArl::Table)
                    .modify_column(ColumnDef::new(DeeznutsArl::Token).unique_key())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
